import os, sys, subprocess, argparse, tempfile, shutil
from functions import u, error, parse_input_fasta, check_binaries

#Processing command line arguments
usage = "Usage: %(prog)s [options] -i INPUT_FILE -o OUTPUT_DIR"
parser = argparse.ArgumentParser(usage = usage)
parser.add_argument('-i', dest = 'input_file', required = True,
                    help = 'Input proteome FASTA file')
parser.add_argument('-f', dest = 'output_fasta', required = True,
                    help = 'Output FASTA file')
parser.add_argument('-o', dest = 'output_hhm', required = True,
                    help = 'Output HHM file')
parser.add_argument('-s', dest = 'species', default = '', help =\
'If multiple headers, select only those with the species')
##
parser.add_argument('-n', dest = 'ignore', action = "store_true",
                    default = False, help = 'Ignore headers w/o the species')
if __name__ == "__main__":
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    opt = parser.parse_args()
    opt.input_file = os.path.abspath(opt.input_file)
    if not os.path.isfile(opt.input_file):
        error('Input file does not exist')
    opt.output_fasta = os.path.abspath(opt.output_fasta)
    opt.output_hhm = os.path.abspath(opt.output_hhm)
                    
#---Main section---
check_binaries(['hhmake'])
home = os.path.expanduser('~') + '/'
temp_dir = tempfile.mkdtemp(suffix = '_{}'.format(os.getpid())) + '/'
temp_fasta = tempfile.NamedTemporaryFile('w+')
temp_hhm = tempfile.NamedTemporaryFile('w+')
fasta_breaker = home + 'work/pyscripts/fasta_breaker.py'
with open(os.devnull, 'wb') as devnull:
    check = subprocess.Popen(['python3', fasta_breaker, '-i', opt.input_file,\
    '-o', temp_dir], stdout = devnull, stderr = subprocess.PIPE)
    ##
stdout, stderr = check.communicate()
if stderr:
    error(u(stderr), ' in fasta_breaker.py')
headers = set()
with open(opt.output_fasta, 'w') as ofile_fasta, open(opt.output_hhm, 'w') as\
ofile_hhm:
##
    for fname in sorted(os.listdir(temp_dir)):
        fpath = temp_dir + fname
        header, seq = parse_input_fasta(fpath)[0]
        if opt.species:
            header_ = ''
            for subheader in header.split('>'):
                if '[' + opt.species in subheader:
                    header_ = '>' + subheader.strip()
                    break
            if not header_:
                if opt.ignore:
                    continue
                else:
                    error("The species '{}' is not found in the header of '{}'".\
                    format(opt.species, fname))
                    ##
            if header_ in headers:
                continue
        else:
            header_ = header
        headers.add(header_)
        content = '{}\n{}\n'.format(header_, seq)
        ofile_fasta.write(content)
        with open(temp_fasta.name, 'w') as ofile:
            ofile.write(content)
        with open(os.devnull, 'wb+') as devnull:
            check = subprocess.Popen(['hhmake', '-i', temp_fasta.name, '-o',\
            temp_hhm.name, '-M', 'first', '-name', header_[1: ]], stdout =\
            devnull, stderr = subprocess.PIPE)
            ##
        stdout, stderr = check.communicate()
        if stderr and ('ERROR' in u(stderr)):
            error(u(stderr), ' in hhmake')
        with open(temp_hhm.name, 'r') as ifile:
            ofile_hhm.write(ifile.read())
shutil.rmtree(temp_dir)