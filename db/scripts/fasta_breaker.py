import os, sys, re
from optparse import OptionParser

#Global variables
cheader = re.compile('^>')
cseq = re.compile('^[ACDEFGHIKLMNPQRSTUVWYXUBZ\*]+$')
cblank = re.compile('^[\ \t]*$')

#Processing command line arguments
usage = "Usage: %prog [options] -i input.fasta -o output_dir"
parser = OptionParser(usage = usage)
parser.add_option('-i', metavar = "INPUT", dest = 'input', help =\
'Input multiple FASTA file')
##
parser.add_option('-o', metavar = 'OUTPUT_DIR', dest = 'output', help =\
'Output directory with single-record FASTA files')
##
if __name__ == "__main__":
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()
    (opt, args) = parser.parse_args()
    opt.input = os.path.abspath(opt.input)
    if not os.path.isfile(opt.input):
        sys.stderr.write('Error: Input file does not exist\n')
        sys.exit(1)
    opt.output = os.path.abspath(opt.output)
    opt.output = os.path.abspath(opt.output) + '/'
    if not os.path.isdir(opt.output):
        os.makedirs(opt.output)
    elif os.listdir(opt.output):
        sys.stderr.write('Error: Output directory is not empty\n')
        sys.exit(1)

#---Defining functions---
def parse_input_fasta(input_file):
    """Function for parsing and placing into list an input FASTA file"""
    records = []
    first = True
    expect_seq = False
    error = False
    header = ''
    seq = ''
    with open(input_file, 'r') as file:
        for line_idx, line in enumerate(file):
            if cheader.search(line):
                if expect_seq:
                    first = True
                if first:
                    first = False
                else:
                    records.append([header, seq])
                header = line[: -1]
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                seq += line.strip()
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                error = True
                break
        records.append([header, seq])
        if error:
            sys.stderr.write(\
            "Error while parsing line {} in '{}': file is not FASTA\n".\
            format(line_idx + 1, input_file))
            ##
            sys.exit(1)
    return records

#---Main section---
records = parse_input_fasta(opt.input)
digits = len(str(len(records) - 1))
count = 0
for record in records:
    filename = opt.output + '{0:0{1}d}.fasta'.format(count, digits)
    with open(filename, 'w') as ofile:
        ofile.write(record[0] + '\n' + record[1])
    count += 1
