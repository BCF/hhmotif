import os, sys, shutil

db_dir = 'hhm_precalculated/'
db_file = db_dir + 'index.tsv'
dir_ = sys.argv[1]

count = 0
seq_set = set()
with open(db_file, 'w') as ofile:
    for el in os.listdir(dir_):
        if not el.endswith('_cache'):
            continue
        dirpath = dir_ + el + '/'
        for fname in os.listdir(dirpath):
            if not fname.endswith('.hhm'):
                continue
            fpath = dirpath + fname
            seq = ''
            with open(fpath, 'r') as ifile:
                seq_pointer = 0
                for line in ifile:
                    if line.startswith('>'):
                        seq_pointer += 1
                        continue
                    if seq_pointer < 2:
                        continue
                    if seq_pointer == 3:
                        break
                    if (seq_pointer == 2) and line.startswith('#'):
                        break
                    seq += line.strip().replace('-', '').upper()
            if seq in seq_set:
                continue
            seq_set.add(seq)
            file_id = '{:0>8x}'.format(count)
            ofile.write('{}\t{}\n'.format(seq, file_id))
            shutil.copy(fpath, db_dir + file_id + '.dat')
            count += 1