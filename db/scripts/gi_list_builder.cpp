#include <iostream> //std::cout, std::endl   
#include <fstream> //std::ifstream, std::ofstream
#include <string> //std::string
#include <vector> //std::vector
#include <map> //std::map
#include <algorithm> //std::count, std::find, std::sort, std::set_intersection
#include <utility> //std::pair
#include <iterator> //std::back_inserter
#include <functions.hpp> //functions::error, functions::vector_to_string, functions::string_format
#include <argument_parser_1.hpp> //args::ArgParser

typedef std::map<int, std::vector<int> > MapType;

MapType read_input_map(std::string& ifname) {
    MapType gi_lists;
    std::ifstream input(ifname);
    if (!input)
        functions::error("Cannot open the input map file");
    int gi,taxid;
    while (input>>gi>>taxid) {
        if (gi_lists.count(taxid)==0)
            gi_lists[taxid]=std::vector<int>();
        gi_lists.at(taxid).push_back(gi);
    }
    input.close();
    return gi_lists;
}

std::vector<int> read_input_refseq(std::string& ifname) {
    std::vector<int> gi_list=std::vector<int>();
    std::ifstream input(ifname);
    if (!input)
        functions::error("Cannot open the refseq GI list file");
    int gi;
    while (input>>gi)
        gi_list.push_back(gi);
    std::sort(gi_list.begin(),gi_list.end());
    return gi_list;
}

std::vector<int> trim_to_refseq(std::vector<int>& gi_list, std::vector<int>& gi_list_refseq) {
    std::vector<int> gi_list_trimmed=std::vector<int>();
    std::set_intersection(gi_list.begin(),gi_list.end(),gi_list_refseq.begin(),gi_list_refseq.end(),std::back_inserter(gi_list_trimmed));
    return gi_list_trimmed;
}

int main(int argc, const char** argv) {
    using namespace std;
    
    //Parsing arguments
    args::ArgParser parser;
    parser.add_string_option({"-m"}, true); //GI to taxid map
    parser.add_string_option({"-r"}, true); //Refseq GI list
    parser.parse(argc, argv);
    string ifname_map=parser.get_string_value("-m");
    string ifname_refseq=parser.get_string_value("-r");
    
    //Reading in input files
    MapType gi_lists(read_input_map(ifname_map));
    vector<int> gi_list_refseq(read_input_refseq(ifname_refseq));
    
    //Forming GI lists
    int count=0;
    unsigned long gi_n=0;
    #pragma omp parallel
    #pragma omp single
    {
        for (auto it=gi_lists.begin(); it!=gi_lists.end(); it++)
            #pragma omp task firstprivate(it)
            {
                auto refseq_values=trim_to_refseq(it->second, gi_list_refseq);
                if (refseq_values.size()>0) {
                    ofstream ofile(functions::string_format("gi_%d.txt", it->first));
                    ofile<<functions::vector_to_string(refseq_values, "\n", false);
                    ofile.close();
                    count++;
                }
                gi_n+=it->second.size();
            }
        #pragma omp taskwait
    }
    
    //Finishing
    cout<<functions::string_format("Successfully written %d GI lists with total %d GIs", count, gi_n)<<endl;
}
