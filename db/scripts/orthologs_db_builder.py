import os, sys, shutil
mod_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(\
__file__))), 'pyscripts')
##
sys.path.append(mod_dir)
from functions import get_input_fasta_first_record

db_dir = 'orthologs_precalculated/'
db_file = db_dir + 'index.tsv'
dir_ = sys.argv[1]

count = 0
seq_set = set()
with open(db_file, 'w') as ofile:
    for el in os.listdir(dir_):
        if not el.endswith('_orthologs'):
            continue
        dirpath = dir_ + el + '/'
        for fname in os.listdir(dirpath):
            if not fname.endswith('.fasta'):
                continue
            fpath = dirpath + fname
            seq = get_input_fasta_first_record(fpath)[1]
            if seq in seq_set:
                continue
            seq_set.add(seq)
            file_id = '{:0>8x}'.format(count)
            ofile.write('{}\t{}\n'.format(seq, file_id))
            shutil.copy(fpath, db_dir + file_id + '.dat')
            count += 1