# Mail exchange with Roman about the databases
Edlira Nano on 18/12/2017
>>>
Hello Roman,  
I need to know exactly which databases you processed/changed in HHMotif and how did you process them, so that we are able to do the same if needed in the future. I do not necessarily need the scripts if you do not have them, you can tell us with words  (I put Bianca in CC she will understand things better than me). So please for each of them can you tell me:
  1 what the dabase is used for and where does it originally come from, where did you take it from
  2 whether you changed it or not
  3 if yes what did you change, why and how precisely

Here is the list of the databases I see in HHMotif's installation on my computer :
From folder db:
- hhm_precalculated
- netsurfp_precalculated
- orthologs_precalculated
- proteomes
- taxonomy

From folder blast-db (these I remember taking them from the net as it is so just tell me wh
- nr00 to nr78
- refseq_protein00 to 37

Also folder hhmotif_tmp contains some databases (or maybe it is just for testing).

Have I forgotten any other database?
>>>
Roman Prytuliak same day :
>>>
the precalculated databases are optional. They serve for two purposes:
	1. to speed-up the calculations if the same sequence is submitted
	multiple again (currently, this does not work, as the
	corresponding code regions are commented out for debug purposes)
	2. to ensure stable output of HH-MOTiF, as BLAST is not guaranteed 
	to output hits in the same order upon multiple runs (namely, 
	the order of hits with the same e-value is random due to incomplete thread-safety)

I am attach the corresponding scripts to populate the database for the
ELM proteins. The NetSurfP script is missing, but it is analogous: it
takes *.netsurfp.dat files  (which are Gzip archives) instead of
*.hhm.  These scripts accept as input the folder from the archive
/export2/prytuliak/work/scratch/elm_database_2016.zip.  Ideally, one
can integrate these scripts into HH-MOTiF, so that it updates its
databases on the fly, i.e. appending a line to the file index.tsv and
putting the corresponding HHM/FASTA/DAT file into the folder.

The proteome FASTA files were originally extracted from the NCBI nr
database and then cleaned and converted to HHM with the corresponding
script prepare_proteome.py (attached) which requires fasta_breaker.py (also attached).

The taxonomy file node_short.bin is generated with the script
ncbi_convert_nodes_dmp.py (attached). 
The files gi_{species_group}.txt are downloaded manually form the web
interface of NCBI after listing all the proteins and applying taxonomy
filters. The files gi_{number}.txt are generated with the attached C++ program.

It is all probably still fuzzy, so do not hesitate asking further questions.

>>>
