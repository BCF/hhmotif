# Command: comprehensive_motif_enrichment.py -i /home/eda/hhmotif_code/inputs/003a1c25beaf6c2dc9b458fe895767437e1297bd/ortho -o /home/eda/hhmotif_code/test_outpouts/outputs.txt -c /home/eda/hhmotif_tmp -dis -l results
0000: 0000.fasta
0001: 0001.fasta
0002: 0002.fasta
0003: 0003.fasta
0004: 0004.fasta
0005: 0005.fasta
0000:	32 TASKG 36 (10.49), 134 DD 135 (15.85), 137 SK 138 (16.77), 141 P 141 (16.36), 178 HI 179 (14.91)
0001:	221 NLSE 224 (13.88), 397 GSSGPLPQ 404 (10.37), 663 GRGKTNQNTG 672 (11.87)
0002:	10 RGRDRGFGAPRFGGSRAGPLS 30 (14.46), 40 KLVKKKWNLDELPK 53 (11.92), 379 ERDWVLNE 386 (12.60), 479 GSGRS 483 (13.59)
0003:	99 EGAAGTPRPAGEPA 112 (12.53), 138 KQ 139 (13.24), 304 P 304 (9.77)
0004:	125 SE 126 (12.85), 131 E 131 (11.77), 134 FY 135 (13.10), 452 KSQPERDW 459 (13.41), 559 GGGGGRSR 566 (18.58), 577 NLMYQ 581 (14.47), 683 GRSG 686 (13.83)
0005:	12 MGLSSQNGQLRGPVKPTGGPGGGGTQTQQQ 41 (15.52), 78 L 78 (14.88), 81 K 81 (14.44), 84 RI 85 (14.50), 465 DKSLY 469 (10.32)
******************************
ReciproQ=0.178
******************************
0000:
       32 TASKG 36 (10.49)
Regex: [EPT][ARSV][PST][ADK][GT] (0.982)
0001  231 EVSDT 235 (11.83) nonreciprocal
0003  105 PRPAG 109 (9.99)
0005   91 TSTKG 95 (9.64) nonreciprocal
----------
      134 DD 135 (15.85)
Regex: [DM][DEG] (1.000)
0005   70 DD 71 (22.78) nonreciprocal
0005  114 MG 115 (16.48) nonreciprocal
0002   49 DE 50 (15.65) nonreciprocal
0003  219 DD 220 (11.23) nonreciprocal
----------
      137 SK 138 (16.77)
Regex: .[GK] (1.000)
0005   73 KK 74 (22.78) nonreciprocal
0005  117 EK 118 (16.48) nonreciprocal
0002   52 PK 53 (15.65) nonreciprocal
0001  399 SG 400 (11.88) nonreciprocal
0005   458 K 458 (10.41) nonreciprocal
----------
      141 P 141 (16.36)
Regex: [P] (1.000)
0005   79 P 79 (22.78) nonreciprocal
0005  121 P 121 (16.48) nonreciprocal
0003  182 P 182 (14.42) nonreciprocal
0001  403 P 403 (11.88) nonreciprocal
0005  461 P 461 (10.41) nonreciprocal
----------
      178 HI 179 (14.91)
Regex: .. (1.000)
0002   92 PV 93 (18.12) nonreciprocal
0001  230 SE 231 (14.57) nonreciprocal
0003  565 HI 566 (12.03) nonreciprocal
******************************
0001:
      221 NLSE 224 (13.88)
Regex: [NRST].[LST][DEPQR] (0.999)
0004  143 RLTP 146 (15.61) nonreciprocal
0003  467 SLSQ 470 (14.19) nonreciprocal
0000   22 NSSD 25 (11.83) nonreciprocal
0003  169 NLLR 172 (11.06) nonreciprocal
0003  818 TLSP 821 (10.92) nonreciprocal
----------
      397 GSSGPLPQ 404 (10.37)
Regex: ..[ST][GKQ][PQT].[PQ]. (0.508)
0000    137 SKPLP 141 (11.88) nonreciprocal
0004  683 GRSGQQPQ 690 (10.35)
0005   34 GGTQTQQQ 41 (9.82)
----------
      663 GRGKTNQNTG 672 (11.87)
Regex: [GLS][DRST]........ (1.000)
0004  667 STGRSSQSSS 676 (14.36) nonreciprocal
0000  586 GRSKSSRFSG 595 (11.85) nonreciprocal
0005  133 GR 134 (10.64) nonreciprocal
0004   29 GDSASERES 37 (10.43) nonreciprocal
0005   14 LSSQNGQLRG 23 (9.08)
******************************
0002:
      10 RGRDRGFGAPRFGGSRAGPLS 30 (14.46)
Regex: ..........[DGKR].[GI][G][FST]...... (0.201)
0000  629 GSSRGFGGGGYGGFYNSDGY 648 (17.97) nonreciprocal
0000  95 RFDDRGRSDYDGIGSRGDRSG 115 (15.65) nonreciprocal
0005  22 RGPVKPTGGPGGGGT 36 (11.35)
0001           394 KLGGSSGPLPQ 404 (10.09)
----------
       40 KLVKKKWNLDELPK 53 (11.92)
Regex: .............. (1.000)
0000  125 SRWCDKSDEDDWSK 138 (15.65)
0003    962 RRRKWGNPE 970 (11.24) nonreciprocal
0001               750 K 750 (9.84) nonreciprocal
0001     845 RPHPKPKQFSS 855 (9.84) nonreciprocal
0005   71 DWKK 74 (9.53) nonreciprocal
0003  292 RHEKKKGAEEEKPK 305 (9.09) nonreciprocal
----------
      379 ERDWVLNE 386 (12.60)
Regex: [DEKQ][KMR][DEKN][EQW][ADLV][KL][HNS]. (0.978)
0000  479 DREEALHQ 486 (17.07) nonreciprocal
0004  119 KKKWDLSE 126 (12.83) nonreciprocal
0005   41 QMNQLKN 47 (8.90) nonreciprocal
----------
      479 GSGRS 483 (13.59)
Regex: .[QST]..[GS] (1.000)
0000    86 SDRG 89 (20.53) nonreciprocal
0000  629 GSSRG 633 (16.20) nonreciprocal
0005   19 GQLRG 23 (11.00)
0003  956 GTGVS 960 (10.10) nonreciprocal
******************************
0003:
       99 EGAAGTPRPAGEPA 112 (12.53)
Regex: .............. (1.000)
0005   19 GQLRGPVKPTGGPG 32 (13.95)
0004  432 DDLTRRMRRDGWPA 445 (13.18) nonreciprocal
0002             379 ERD 381 (12.36) nonreciprocal
0004              128 PK 129 (12.16) nonreciprocal
0000   26 NQSGGSTASKG 36 (9.99)
----------
      138 KQ 139 (13.24)
Regex: [KQR][DKQR] (1.000)
0000  478 RD 479 (14.67) nonreciprocal
0001  588 QR 589 (13.96) nonreciprocal
0004  155 KK 156 (12.16) nonreciprocal
0000  264 KQ 265 (10.34) nonreciprocal
----------
      304 P 304 (9.77)
Regex: [P] (1.000)
0000  359 P 359 (11.11) nonreciprocal
0005  483 P 483 (9.11) nonreciprocal
0002   52 P 52 (9.09) nonreciprocal
******************************
0004:
      125 SE 126 (12.85)
Regex: [ENPS][EN] (1.000)
0003  969 PE 970 (14.09) nonreciprocal
0003  168 EN 169 (13.26) nonreciprocal
0002  385 NE 386 (12.83) nonreciprocal
0001  606 NE 607 (11.62) nonreciprocal
----------
      131 E 131 (11.77)
Regex: . (1.000)
0003  174 S 174 (13.26) nonreciprocal
0003  114 V 114 (12.16) nonreciprocal
0001  612 E 612 (11.62) nonreciprocal
0005  465 D 465 (10.43) nonreciprocal
----------
      134 FY 135 (13.10)
Regex: [FLM][LVY] (1.000)
0001  212 FY 213 (15.61) nonreciprocal
0003  177 ML 178 (13.26) nonreciprocal
0003  117 FV 118 (12.16) nonreciprocal
0005  468 LY 469 (10.43) nonreciprocal
----------
      452 KSQPERDW 459 (13.41)
Regex: ...[EPQR].... (1.000)
0000  475 RSQRDREE 482 (16.59) nonreciprocal
0003  119 QKKEERDL 126 (13.18) nonreciprocal
0005      68 PGDDW 72 (10.93) nonreciprocal
0005   55 TQQQ 58 (9.88) nonreciprocal
----------
      559 GGGGGRS_____R 566 (18.58)
Regex: [GSV][GRS][AG].[GIPT][LQRS][ST]. (1.000)
0005   32 GGGGTQTQQQMNQ 44 (27.01)
0000  625 GGGHGSS-----R 632 (16.71) nonreciprocal
0002  550 VSAGIQT-----S 557 (12.02) nonreciprocal
0002   24 SRAGPLS 30 (11.97)
----------
      577 NLMYQ 581 (14.47)
Regex: ..... (1.000)
0005   55 T 55 (27.01) nonreciprocal
0000  573 NMAYE 577 (17.22) nonreciprocal
0002  564 TGTYQ 568 (12.02) nonreciprocal
0001   211 IFYQ 214 (10.97) nonreciprocal
----------
      683 GRSG 686 (13.83)
Regex: [GS].[GS][G] (0.283)
0000  621 SRSG 624 (15.71) nonreciprocal
0005   32 GGGG 35 (15.42)
0001  397 GSSG 400 (10.35)
******************************
0005:
       12 MGLSSQNGQLRGPVKPTGGPGGGGTQTQQQ 41 (15.52)
Regex: .............................. (1.000)
0004        545 NPKLMQLVDHRGGGGGGGGRS--- 565 (27.01)
0002                  560 TGNPTGTYQNGYDS 573 (16.78) nonreciprocal
0004                   680 SGIGRSGQQPQPL 692 (15.42)
0000             626 GGHGSSRGFGGGGYG 640 (13.99) nonreciprocal
0003   92 LCLSSSCEGAAGTPRPAGEPA 112 (13.95)
0002             10 RGRDRGFGAPRFGGS 24 (11.35)
0002  472 LQLVEDRGSGRS 483 (11.00)
0002  526 FGAKTQNGVYSAA 538 (10.34) nonreciprocal
0004                    98 GARGGGGLPP 107 (10.07) nonreciprocal
0001           237 LYSPFKP 243 (9.95) nonreciprocal
0004                   281 GAPKG 285 (9.95) nonreciprocal
0001                       396 GGSSGPLPQ 404 (9.82)
0003              862 AICPAPCPG 870 (9.49) nonreciprocal
0001  602 RDPSNENPKLE 612 (9.35) nonreciprocal
0001  661 LTGRGKTNQNTGMTLPA 677 (9.08)
0002                         373 GDKSQQE 379 (8.90) nonreciprocal
----------
       78 L 78 (14.88)
Regex: . (1.000)
0000  141 - 140 (22.78) nonreciprocal
0004  105 L 105 (12.13) nonreciprocal
0001  173 L 173 (9.73) nonreciprocal
----------
       81 K 81 (14.44)
Regex: [KS] (1.000)
0000  143 S 143 (22.78) nonreciprocal
0004  108 K 108 (12.13) nonreciprocal
0003  253 K 253 (8.40) nonreciprocal
----------
       84 RI 85 (14.50)
Regex: .. (1.000)
0000  146 LE 147 (22.78) nonreciprocal
0001  203 KM 204 (12.31) nonreciprocal
0003  256 RL 257 (8.40) nonreciprocal
----------
      465 DKSLY 469 (10.32)
Regex: ...[AFL]. (1.000)
0003     197 LV 198 (14.70) nonreciprocal
0004  131 EKNFY 135 (10.43) nonreciprocal
0000   65 DKDAY 69 (9.50) nonreciprocal
0003   89 DESL 92 (8.58) nonreciprocal
