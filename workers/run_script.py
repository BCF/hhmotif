from __future__ import absolute_import
import sys, os, time, pickle, shutil
from getpass import getpass
from subprocess import Popen, PIPE
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/'
sys.path.append(BASE_DIR)
from hhmotif_site import settings
from hhmotif_site.config import SERVER_URL, ADMIN_EMAIL, PYTHON_EXE, HHSUITE_ROOT, \
NETSURFP_ROOT, CORES
##
from hhmotif_site.custom_methods import connect_status_database, send_mail,\
send_mail_wrapped, generate_error_html, clear_record
##

MAIL_PASSWORD = getpass("Enter the mailbox password: ")

def finish_run(tag, success = True):
    """
    Method for clean finishing of the task
    """
    if success:
        shutil.rmtree(BASE_DIR + 'inputs/' + tag)
    cache_dir = BASE_DIR + 'caches/' + tag
    if os.path.isdir(cache_dir):
        shutil.rmtree(cache_dir)
    clear_record(tag, 'heavy')
    curtime = time.strftime('%Y-%m-%d %H:%M:%S')
    if success:
        print('{}: Completed the task {} successfully'.format(curtime, tag))
    else:
        print('{}: Finished the task {} with an error'.format(curtime, tag))

def process_error(bytes_message, output_html, log_file, user_email, tag,
                  job_name_):
    """
    Method for processing error occured during a run
    """
    with open(log_file, 'ab') as log:
        log.write(bytes_message)
    message = bytes_message.decode('utf-8')
    if message.startswith('Task error:'):
        message = message.split(': ', 1)[1].split('-' * 10, 1)[0]
    else:
        text = 'A problem with the task {} occured.\n'.format(tag)
        text += message
        send_mail_wrapped(log_file, MAIL_PASSWORD, ADMIN_EMAIL, text,
                          'Server Problem')
        message = 'Internal server error.\n\n\n' +\
        'The server administration has been notified.\n' +\
        'Necessary actions will be taken to fix the issue as soon as possible.'
        ##
    if user_email:
        subject = 'Your Job has Failed'
        text = 'Your job{} has failed. The following error occured:\n\n'
        text = text.format(job_name_)
        text += message
        send_mail_wrapped(log_file, MAIL_PASSWORD, user_email, text, subject)
    with open(output_html, 'w') as ofile:
        ofile.write(generate_error_html(message))
    finish_run(tag, success = False)

def run_script(args):
    """
    Method for running the main script
    """

    # Preparing for start
    starttime = time.strftime('%Y-%m-%d %H:%M:%S')
    mode, ortho_search, gepf, rsa, disorder_check, homology_filter, pv_max,\
    ignore_filters, proteome, full_length_only, user_email, job_name,\
    submit_time, tag = pickle.loads(args)
    ##

    print('{}: Received the task {}'.format(starttime, tag))
    job_name_= ' "{}"'.format(job_name) if job_name else ''

    # Setting paths
    input_fasta_dir = BASE_DIR + 'inputs/{}/input_fasta/'.format(tag)
    ortho_dir = BASE_DIR + 'inputs/{}/ortho/'.format(tag)
    cache_dir = BASE_DIR + 'caches/{}/'.format(tag)
    raw_input_fasta_file = BASE_DIR + 'inputs/{}/input.fasta'.format(tag)
    if os.path.isdir(cache_dir):
        shutil.rmtree(cache_dir)
    regions_file = BASE_DIR + 'inputs/{}/regions.txt'.format(tag)
    if not os.path.isfile(regions_file):
        regions_file = ''
    taxonomy_dir = '../db/taxonomy/'
    precalc_ortho_db_dir = '../db/orthologs_precalculated/'
    if (mode == 'ADVANCED') and (not ortho_search):
        precalc_netsurfp_db_dir = ''
        precalc_hhm_db_dir = ''
        labels_file = BASE_DIR + 'inputs/{}/input_labels.txt'.format(tag)
    else:
        precalc_netsurfp_db_dir = '../db/netsurfp_precalculated/'
        precalc_hhm_db_dir = '../db/hhm_precalculated/'
        labels_file = BASE_DIR + 'inputs/{}/input.fasta'.format(tag)
    if not os.path.isfile(labels_file):
        labels_file = ''
    python_prog = PYTHON_EXE
    ortho_searcher_name = '../core/ortholog_searcher_folder.py'
    script_name = '../core/'
    output_html_dir = BASE_DIR + 'outputs/{}/'.format(tag)
    output_html = output_html_dir + 'results.html'
    output_file = output_html_dir + 'results.txt'
    log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
    if mode != 'PROTEOME':
        script_name += 'comprehensive_motif_enrichment.py'
    else:
        script_name += 'motif_proteome_search.py'
        template_fasta_file = input_fasta_dir + 'sanitized_input.fasta'
        proteome_dir = '../db/proteomes/'
        proteome_fasta_file = proteome_dir + proteome + '.fa'
        proteome_hhm_file = proteome_dir + proteome + '.hhm'

    # Setting environment
    os.environ['PATH'] += ':{}'.format(NETSURFP_ROOT)
    # os.environ['HHLIB'] = HHSUITE_ROOT
    # os.environ['hhalign'] = HHSUITE_ROOT + '/applications/hh-suite/bin/hhalign'

    # Updating waiting HTML
    template = BASE_DIR + 'hhmotif_app/templates/hhmotif_app/running.html'
    target = output_html_dir + 'results.html'
    shutil.copy(template, target)

    # Copying the static elements
    source = BASE_DIR + 'hhmotif_app/static/hhmotif_app/img/logo.svg'
    target = output_html_dir + 'logo.svg'
    shutil.copy(source, target)
    suffix = '_pw' if mode == 'PROTEOME' else ''
    source = BASE_DIR +\
    'hhmotif_app/static/hhmotif_app/js/results{}.min.js'.format(suffix)
    ##
    target = output_html_dir + 'results.min.js'
    shutil.copy(source, target)
    source = BASE_DIR +\
    'hhmotif_app/static/hhmotif_app/css/results{}.min.css'.format(suffix)
    ##
    target = output_html_dir + 'results.min.css'
    shutil.copy(source, target)

    # Searching for orthologs if needed
    if mode == 'PROTEOME':
        pass
    elif ortho_search:
        if os.path.isdir(ortho_dir):
            shutil.rmtree(ortho_dir)
        with open(log_file, 'a') as log:
            c = Popen(['nice', python_prog, ortho_searcher_name, '-i',\
            input_fasta_dir, '-o', ortho_dir, '-t', taxonomy_dir, '-l', '0.9',\
            '-q', '70', '-a', '-p', precalc_ortho_db_dir], stdout = log,\
            stderr = PIPE)
            ##
        stdout, stderr = c.communicate()
        if stderr:
            process_error(stderr, output_html, log_file, user_email, tag,
                          job_name_)
            return
        for fname in os.listdir(ortho_dir):
            fpath = ortho_dir + fname
            fpath_new = fpath.rsplit('_o', 1)[0]+ '.fasta'
            os.rename(fpath, fpath_new)
    elif not os.listdir(ortho_dir):
        for fname in os.listdir(input_fasta_dir):
            shutil.copy(input_fasta_dir + fname, ortho_dir)

    # Generating the header
    if mode != 'PROTEOME':
        header = 'Query regions file: ' + ('yes' if regions_file else\
        'no') + '\n' +\
        '<br />Orthology search: ' + ('yes' if ortho_search else 'no') + '\n' +\
        '<br />Gap restriction: ' + ('yes' if gepf == '999' else 'no') + '\n' +\
        '<br />Surface accessibility check: ' +\
        ('yes' if float(rsa) else 'no') + '\n' +\
        '<br />Intrinsical disorder check: ' +\
        ('yes' if disorder_check else 'no') + '\n' +\
        '<br />Homology/domain filtering: ' + ('yes' if homology_filter else\
        'no') + '\n' +\
        '<br />Show best suboptimal if no motifs found: ' +\
        ('yes' if ignore_filters else 'no') + '\n' +\
        '<br />Proteins total: {}\n'.format(len(os.listdir(ortho_dir)))
        ##
    else:
        header =  'Organism: ' + proteome.replace('_', '. ') + '\n' +\
        '<br />Only full-length matches: ' + ('yes' if full_length_only else\
        'no') + '\n'
        ##
    header += '<br />Job ID: ' + tag + '\n'
    header += '<br />Submitted: ' + submit_time + '\n'

    # Running main tool
    with open(log_file, 'a') as log:
        if mode != 'PROTEOME':
            c = Popen(['nice', 'mpiexec', '-n', str(CORES), python_prog, '-W',\
            'ignore', script_name, '-i', ortho_dir, '-i1', regions_file, '-i2',\
            labels_file, '-c', cache_dir, '-o', output_file, '-html',\
            output_html_dir, '-g', gepf, '-hits', '4', '-r', rsa, '-pmax',\
            pv_max, '-cols_max', '30', '-vt_min', '11.0', '-vt_max', '40.0',\
            '-rchk', '4', '-rescore', '-lmin', '3', '-smin', '13.0', '-mmin',\
            '4', '-rmin', '6', '-fmin', '0.33', '-nocontxt', '-html_header',\
            header, '-job_name', job_name, '-precalc_netsurfp',\
            precalc_netsurfp_db_dir, '-precalc_hhm', precalc_hhm_db_dir, '-P',\
            str(CORES), '-production'] + ['-no_hf'] * (not homology_filter) +\
            ['-dis'] * disorder_check + ['-I'] * ignore_filters, stdout = log,\
            stderr = PIPE)
            ##
        else:
            c = Popen(['nice', python_prog, script_name, '-p',\
            proteome_hhm_file, '-f', proteome_fasta_file, '-t',\
            template_fasta_file, '-o', output_html_dir, '-html_header',\
            header, '-job_name', job_name, '-raw_input',\
            raw_input_fasta_file] + ['-fl'] * full_length_only, stdout = log,\
            stderr = PIPE)
            ##
    stdout, stderr = c.communicate()
    if stderr and (b"Normal termination\n" not in stderr):
    #if stderr:
        process_error(stderr, output_html, log_file, user_email, tag, job_name_)
        return
    else:
        with open(log_file, 'a') as log:
            Popen(['unix2dos', output_html_dir + 'results.txt'], stdout = log,\
            stderr = log).communicate()
            ##
        if user_email:
            subj = 'Your Job is Complete'
            text = 'Your job{} is complete.\n\n'.format(job_name_)
            text += 'Link to the result:\n'
            text += SERVER_URL + settings.MEDIA_URL + tag + '/results.html\n\n'
            text += 'Please note that your results are stored only for 7 days\n'
            send_mail_wrapped(log_file, MAIL_PASSWORD, user_email, text, subj)

    #Cleaning up (mysql record also erased here) and finishing
    finish_run(tag)

curtime = time.strftime('%Y-%m-%d %H:%M:%S')
print('{}: Worker is ready'.format(curtime))
while True:
    result = None
    try:
        mysql_conn = connect_status_database()
        cmd = "SELECT args FROM heavy_queue ORDER BY submitted LIMIT 1"
        cursor = mysql_conn.cursor()
        cursor.execute(cmd)
        result = cursor.fetchall()
        mysql_conn.close()
    except KeyboardInterrupt:
        sys.exit()
    if result:
        args = result[0][0]
        run_script(args)
    else:
        time.sleep(1)
        continue
