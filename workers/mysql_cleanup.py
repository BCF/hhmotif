import os, sys, time
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/'
sys.path.append(BASE_DIR)
from hhmotif_site.custom_methods import end_session

session_key = sys.argv[1]
time.sleep(30)
end_session(session_key)
