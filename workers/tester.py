from __future__ import absolute_import
import sys, os, shutil, datetime, time, pickle
from getpass import getpass
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/'
sys.path.append(BASE_DIR)
from hhmotif_site.custom_methods import queue_the_task, send_mail_wrapped,\
generate_tag
##
from hhmotif_site.config import ADMIN_EMAIL, PV_MAX, RSA_MIN

MAIL_PASSWORD = getpass("Enter the mailbox password: ")

def report(message):
    """
    Function for sending report on test results
    """
    print('The test finished with status: ' + message)
    message = 'Server routine test: ' + message
    send_mail_wrapped(os.devnull, MAIL_PASSWORD, ADMIN_EMAIL, message)

def run_test():
    """
    Main function for running a server test
    """

    # Generating input and output folder
    tag = generate_tag(ADMIN_EMAIL)
    input_dir = '{}inputs/{}/'.format(BASE_DIR, tag)
    try:
        os.mkdir(input_dir)
    except:
        report('Hash collision')
        return
    standards_dir = BASE_DIR + 'standards/'
    shutil.copytree(standards_dir + 'input_fasta/', input_dir + 'input_fasta')
    shutil.copyfile(standards_dir + 'input.fasta', input_dir + 'input.fasta')
    output_html_dir = BASE_DIR + 'outputs/{}/'.format(tag)
    os.mkdir(output_html_dir)

    # Emulating user submit
    mode = 'STANDARD'
    ortho_search = True
    gepf = '999'
    rsa_min = str(RSA_MIN)
    disorder_check = False
    homology_filter = True
    pv_max = str(PV_MAX)
    ignore_filters = False
    proteome = ''
    full_length_only = False
    job_name = 'test_job'
    submit_time  = str(datetime.datetime.utcnow())[: -7] + ' UTC'
    args = pickle.dumps((mode, ortho_search, gepf, rsa_min, disorder_check,\
    homology_filter, pv_max, ignore_filters, proteome, full_length_only,\
    ADMIN_EMAIL, job_name, submit_time, tag))
    ##
    queue_the_task('heavy', args, tag)

    # Check for results
    results_file = output_html_dir + 'results.txt'
    results_html = output_html_dir + 'results.html'
    stime = time.time()
    while True:
        time.sleep(5)
        if os.path.isfile(results_file):
            break
        else:
            time.sleep(1)
            if os.path.isfile(results_html):
                with open(results_html, 'r') as ifile:
                    if 'error' in ifile.read():
                        report('Error')
                        return
        if time.time() - stime > 3600 * 5:
            report("Waiting for too long")
            return
    standard_file = standards_dir + 'results.txt'
    with open(results_file, 'rb') as new, open(standard_file, 'rb') as standard:
        if new.read() == standard.read():
            report('Success')
        else:
            report('Failure')

while True:
    current_time = datetime.datetime.utcnow()
    if current_time.hour == 1:
        strtime = str(current_time)[: -7] + ' UTC'
        print('Launching a test at ' + strtime)
        run_test()
        time.sleep(3600)
    time.sleep(60)
