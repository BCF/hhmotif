import os, time, shutil

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/'

outputs_dir = BASE_DIR + 'outputs/'
while True:
    for folder_name in os.listdir(outputs_dir):
        folder_path = outputs_dir + folder_name
        if not os.path.isdir(folder_path):
            continue
        days_elapsed = (time.time() - os.path.getmtime(folder_path)) / 86400
        if days_elapsed > 7:
            shutil.rmtree(folder_path)
            curtime = time.strftime('%Y-%m-%d %H:%M:%S')
            print('{}: Results with tag {} deleted'.format(curtime,
                                                           folder_name))
    time.sleep(86400)
