from __future__ import absolute_import
import sys, os, shutil, zipfile, time, pickle, re, traceback
from getpass import getpass
from multiprocessing import Process
from mysql.connector import errors as MySQLErrors
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/'
sys.path.append(BASE_DIR)
from hhmotif_site.custom_methods import prepare_input_fasta, strip_orthologs,\
check_regions_file, connect_status_database, prepare_input_align, clear_record,\
ask_status, send_mail_wrapped
##
from hhmotif_site.config import PROTS_MAX, ADMIN_EMAIL

MAIL_PASSWORD = getpass("Enter the mailbox password: ")

if (len(sys.argv) != 2) or (not re.search('^[0-9]+$', sys.argv[1])):
    sys.stderr.write('Usage: python {} NUM_OF_PROCESSES'.format(sys.argv[0]))
    sys.exit(1)

def write_status(tag, status, log_message = '', success = False):
    """
    Method to write result status into the database
    """
    log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
    input_dir = BASE_DIR + 'inputs/' + tag
    with open(log_file, 'a') as log:
        log.write((log_message if log_message else status) + '\n')
    mysql_conn = connect_status_database()
    cmd = "UPDATE prioritized_queue SET status='{}' WHERE tag='{}'"
    cmd = cmd.format(status, tag)
    mysql_conn.cursor().execute(cmd)
    mysql_conn.commit()
    mysql_conn.close()
    if not success:
        shutil.rmtree(input_dir)
    curtime = time.strftime('%Y-%m-%d %H:%M:%S')
    print('{}: Completed checking for the task with tag {}. Status: \'{}\''.\
    format(curtime, tag, status))
    ##

    # Clean up in case of lost connection
    time.sleep(30)
    if ask_status(tag) != 'Connection timeout':
        clear_record(tag, 'prioritized')
        if success:
            shutil.rmtree(input_dir)
            shutil.rmtree(BASE_DIR + 'outputs/' + tag)

def check_advanced_input(tag, ortho_search, input_set_file, regions_file,
                         input_dir):
    """
    Method for checking user input submitted through the advanced form
    """

    # Copying input archive
    input_zip = input_dir + 'input.zip'
    with open(input_zip, 'wb+') as ofile:
        for chunk in input_set_file.chunks():
            ofile.write(chunk)

    # Extracting input data set
    input_content_dir = input_dir + 'input_content/'
    try:
        with zipfile.ZipFile(input_zip, 'r') as zfile:
            zfile.extractall(input_content_dir)
    except:
        if check_standard_input(tag, input_set_file, input_dir):
            if regions_file:
                msg =\
                'Query regions file can be submitted only with a ZIP archive'
                ##
                write_status(tag, msg)
                return False
            return True
        msg = 'The input file is neither a ZIP archive nor a FASTA'
        os.mkdir(input_dir = BASE_DIR + 'inputs/' + tag)
        write_status(tag, msg)
        return False
    os.remove(input_zip)

    # Generating additional folders
    ortho_dir_raw = input_dir + 'ortho_raw/'
    os.mkdir(ortho_dir_raw)
    ortho_dir = input_dir + 'ortho/'
    os.mkdir(ortho_dir)
    input_fasta_dir = input_dir + 'input_fasta/'
    if ortho_search:
        os.mkdir(input_fasta_dir)

    # Checking for validity the input protein set
    input_content_names_ = sorted(os.listdir(input_content_dir))
    input_content_names = []
    for name in input_content_names_:
        if (name[0].isalpha() or name[0].isdigit()):
            input_content_names.append(name)
        if ':' in name:
            message = "'{}': colons in filenames are not allowed"
            message = message.format(name)
            write_status(tag, message)
            return False
    if len(input_content_names) == 1:
        dirpath = '{}{}/'.format(input_content_dir, input_content_names[0])
        if os.path.isdir(dirpath):
            for name in os.listdir(dirpath):
                if not (name[0].isalpha() or name[0].isdigit()):
                    continue
                path = dirpath + name
                if os.path.isdir(path):
                    message = 'Input protein set contains subdirectories'
                    write_status(tag, message)
                    return False
                shutil.copy(path, ortho_dir_raw)
    else:
        for name in input_content_names:
            path = input_content_dir + name
            if os.path.isdir(path):
                message = 'Input protein set contains subdirectories'
                write_status(tag, message)
                return False
            shutil.copy(path, ortho_dir_raw)
    if len(os.listdir(ortho_dir_raw)) <= 2:
        message = 'Too few FASTA files were identified. '
        message += 'At least 3 files must be provided'
        write_status(tag, message)
        return False
    if len(os.listdir(ortho_dir_raw)) > PROTS_MAX:
        message = 'Too fany FASTA files is provided. '
        message += 'Maximal number is ' + PROTS_MAX
        write_status(tag, message)
        return False
    shutil.rmtree(input_content_dir)

    # Checking the FASTA files for validity and prepering the format for
    # subsequent processing
    labels = ''
    for fname_raw in os.listdir(ortho_dir_raw):
        if len(fname_raw) > 72:
            message = "Error: filename '{}...' is too long. "
            message += "Max. allowed length is 72 characters"
            write_status(tag, message.format(fname_raw[: 72]))
            return False
        fpath_raw = ortho_dir_raw + fname_raw
        fname = fname_raw + '.fasta'
        fpath = (input_fasta_dir if ortho_search else ortho_dir) + fname
        try:
            prepare_input_fasta(fpath_raw, fpath)
            if ortho_search:
                strip_orthologs(fpath)
        except UnicodeDecodeError as e:
            message = "Error while opening '{}': the file is not FASTA"
            write_status(tag, message.format(fname_raw))
            return False
        except IOError as e:
            write_status(tag, str(e))
            return False
        except Exception as e:
            message = ''.join(traceback.format_exception(*sys.exc_info()))
            log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
            send_mail_wrapped(log_file, MAIL_PASSWORD, ADMIN_EMAIL, message,
                              'Server Error')
            write_status(tag, 'Server internal error', message)
            return False
        labels += '>{}\n'.format(fname_raw)
    shutil.rmtree(ortho_dir_raw)
    with open(input_dir + 'input_labels.txt', 'w') as ofile:
        ofile.write(labels)

    # Copying and checking for validity the input regions file
    if regions_file:
        input_regions_file = input_dir + 'regions.txt'
        with open(input_regions_file, 'wb+') as ofile:
            for chunk in regions_file.chunks():
                ofile.write(chunk)
        try:
            check_regions_file(input_regions_file,
                               input_fasta_dir if ortho_search else ortho_dir)
        except UnicodeDecodeError as e:
            message =\
            "Error while opening the regions file: the file is not text"
            ##
            write_status(tag, message.format(fname_raw))
            return False
        except IOError as e:
            write_status(tag, str(e))
            return False
        except Exception as e:
            write_status(tag, 'Server internal error', str(e))
            return False

    # Success
    return True

def check_standard_input(tag, input_set_file, input_dir, text_input = False):
    """
    Method for checking user input submitted through the standard form
    """

    # Generating additional folders
    input_fasta_dir = input_dir + 'input_fasta/'
    os.mkdir(input_fasta_dir)
    ortho_dir = input_dir + 'ortho/'
    os.mkdir(ortho_dir)

    # Copying input file
    input_fasta = input_dir + 'input.fasta'
    if input_set_file:
        with open(input_fasta, 'wb+') as ofile:
            for chunk in input_set_file.chunks():
                ofile.write(chunk)
    else:
        sample_file = BASE_DIR + 'downloads/TRG_LysEnd_APsAcLL_3.fasta'
        shutil.copy(sample_file, input_fasta)

    # Extracting and checking for validity the input protein set
    try:
        real_name = 'input field' if text_input else 'FASTA file'
        prepare_input_fasta(input_fasta, '/dev/null', output_dir =\
        input_fasta_dir, prots_min = 3, real_name = real_name)
        ##
    except UnicodeDecodeError as e:
        message = "The input file is not FASTA"
        write_status(tag, message)
        return False
    except IOError as e:
        write_status(tag, str(e))
        return False
    except Exception as e:
        message = ''.join(traceback.format_exception(*sys.exc_info()))
        log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
        send_mail_wrapped(log_file, MAIL_PASSWORD, ADMIN_EMAIL, message,
                          'Server Error')
        write_status(tag, 'Server internal error', message)
        return False

    # Success
    return True

def check_proteome_input(tag, input_template_file, input_dir):
    """
    Method for checking user input submitted through proteome search form
    """

    # Generating additional folders
    input_fasta_dir = input_dir + 'input_fasta/'
    os.mkdir(input_fasta_dir)

    # Copying input file
    input_fasta = input_dir + 'input.fasta'
    if input_template_file:
        if input_template_file._size > 1 * 1024 * 1024:
            message = "The input protein set file is too large. "
            message += "Max. allowed size is 1 MB"
            write_status(tag, message)
            return False
        with open(input_fasta, 'wb+') as ofile:
            for chunk in input_template_file.chunks():
                ofile.write(chunk)
    else:
        sample_file = BASE_DIR + 'downloads/test_motif.fasta'
        shutil.copy(sample_file, input_fasta)

    # Extracting and checking for validity the input motif FASTA
    sanitized_input_fasta = input_fasta_dir + 'sanitized_input.fasta'
    try:
        prepare_input_align(input_fasta, sanitized_input_fasta)
    except UnicodeDecodeError as e:
        message = "The input file is not FASTA"
        write_status(tag, message)
        return False
    except IOError as e:
        write_status(tag, str(e))
        return False
        message = ''.join(traceback.format_exception(*sys.exc_info()))
        log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
        send_mail_wrapped(log_file, MAIL_PASSWORD, ADMIN_EMAIL, message,
                          'Server Error')
        write_status(tag, 'Server internal error', message)
        return False

    # Success
    return True

def check_user_input(args):
    """
    Method for validity checking of input files provided by the user.

    It also will place and name the files in the manner that they can be read
    by the main script
    """

    # Preparing for start
    starttime = time.strftime('%Y-%m-%d %H:%M:%S')
    mode, ortho_search, input_file, text_input, regions_file, tag =\
    pickle.loads(args)
    ##
    log_file = BASE_DIR + 'logs/{}.log.txt'.format(tag)
    print('{}: Received for checking the task with tag {}'.format(starttime, tag))
    with open(log_file, 'a') as log:
        log.write('Input check started\n')

    # Generating input folder
    input_dir = '{}inputs/{}/'.format(BASE_DIR, tag)
    try:
        os.mkdir(input_dir)
    except:
        write_status(tag, 'Duplicate request')
        return

    # Directing to the corresponding processing method
    if mode == 'ADVANCED':
         if not check_advanced_input(tag, ortho_search, input_file,
                                     regions_file, input_dir):
            return
    elif mode == 'STANDARD':
        if not check_standard_input(tag, input_file, input_dir, text_input):
            return
    elif mode == 'PROTEOME':
        if not check_proteome_input(tag, input_file, input_dir):
            return
    else:
        write_status(tag, 'Server internal error', 'Error: Unknown mode')
        return

    # Generating waiting HTML in case of successful check
    template = os.path.join(BASE_DIR,
                            'hhmotif_app/templates/hhmotif_app/queued.html')
    output_html_dir = BASE_DIR + 'outputs/{}/'.format(tag)
    os.mkdir(output_html_dir)
    target = output_html_dir + 'results.html'
    shutil.copy(template, target)

    #print(os.listdir(input_dir))##/

    # Report success
    write_status(tag, '', 'Input check successfully completed', success = True)



def main(i):
    curtime = time.strftime('%Y-%m-%d %H:%M:%S')
    print('{}: Worker {} is ready'.format(curtime, i))
    #mysql_conn = connect_status_database()
    while True:
        result = None
        try:
            #if not mysql_conn.is_connected():
            mysql_conn = connect_status_database()
            cmd = "SELECT args FROM prioritized_queue " +\
            "WHERE worker={} AND status IS NULL"
            ##
            cmd = cmd.format(i)
            cursor = mysql_conn.cursor()
            cursor.execute(cmd)
            result = cursor.fetchall()
            mysql_conn.commit()
            if not result:
                cmd = "SELECT tag INTO @tag{} FROM prioritized_queue " +\
                "WHERE worker=-1 ORDER BY submitted LIMIT 1 FOR UPDATE"
                ##
                cmd = cmd.format(i)
                mysql_conn.cursor().execute(cmd)
                cmd =\
                "UPDATE prioritized_queue SET worker={i} WHERE tag=@tag{i}"
                ##
                cmd = cmd.format(i = i)
                mysql_conn.cursor().execute(cmd)
                mysql_conn.commit()
                cmd = "SELECT args FROM prioritized_queue " +\
                "WHERE worker={} AND status IS NULL"
                ##
                cmd = cmd.format(i)
                cursor = mysql_conn.cursor()
                cursor.execute(cmd)
                result = cursor.fetchall()
            mysql_conn.close()
        except KeyboardInterrupt:
            sys.exit()
        if result:
            args = result[0][0]
            check_user_input(args)
        time.sleep(1)

for i in range(int(sys.argv[1])):
    p = Process(target = main, args = (i, ))
    p.start()
