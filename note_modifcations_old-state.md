------------ MODIFICATION TO  for old-state-hhmotif to work
# mod_wsgi
You need the mod-wsgi for Python_3.4
Be aware when you are installing this mod, use this command line  :
  sudo apt-get install libapache2-mod-wsgi-py3

  If you forgot to specify the py3 you will download the version for Python2.7,
  and the site won't be able to run properly.


# REMOVED TEMPLATE_CONTEXT_PROCESSORS from settings.py
  - replace by what are in the context_processors dict in setting.py
  - new import : from django.conf import settings to replace the import
  of the TEMPLATE_CONTEXT_PROCESSORS library which outdated.


# UPDATED mysql.connector in setting.py
  - installation :
      sudo apt-get install python-mysqldb
      pip3 install mysql-connector-python mysql-connector-python

  - modifications of parameters :

  DATABASES = {
      'default': {
          'NAME': 'django_dnhhmm',
          'ENGINE': 'mysql.connector.django',
          'USER': 'django',
          'PASSWORD': 'dnhhmm',
          'HOST': 'localhost',
          'PORT': '3306',
          'OPTIONS': {
              'autocommit' : True,
          }
      }
  }

# argument + '' added for model name used as ForeingKey on_delete=models.CASCADE add to ForeignKey, in model.py
  Into :
    - Sequence(models.Model)
      - species

    - Orthologs(models.Model)
      - qid
      - oid
      - taxid

    - Alignment(models.Model)
      - oid

    - Hsp(models.Model)
      - aln-id

    - Refseq_go(models.Model)
      - refseq_id
      - go_id

    - Proteome(models.Model)
      - taxid


# Mysql user and database create.

# Modify url.py hhmotif_site & app
  - add import : from django.conf import settings (site, app)

  - removed include() for admin urlpath  (site)

  - replace +static by :  (site)
      if settings.DEBUG:
        urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

  - add app_name (app)
    add +static and import (app)



------------ MODIFICATIONS REMOVED FROM / apache.conf
------------ + reset of hhmotif-site.conf

## SETTING FOR HH-MOTIF

#PATH TO THE PYTHON PACKAGES
WSGIPythonPath /home/eda/hhmotifVenV/lib/python3.6/sites-packages

# WSGI script to set the interface beetween the Django application and apache
WSGIScriptAlias / /home/eda/hhmotif/hhmotif_site/wsgi.py
<Directory /home/eda/hhmotif/hhmotif_site>
  <Files wsgi.py>
    Require all granted
  </Files>
</Directory>
WSGIScriptReloading On
