var regionId="";
var descrHTML="";
$(document).ready(function() {
    var prefix=window.location.href.split(/\/out|\/down/)[0];
    $("#search_href").attr("href", prefix+"/search/");
    $("#guide_href").attr("href", prefix+"/guide/?l=proteome_wide_output");
    var width=$("#sequences").outerWidth()+parseInt($("#info_box").css("max-width"))+8;
    $(".width-limit").css("width", width);
    width+=parseInt($("body").css("padding-left"));
    $("#logo_wrapper").width(Math.min(width, $(window).width()));
    var height=$("#logo_wrapper").outerHeight();
    $("#title_text").css("margin-top", height);
});
$(document).on("click", ".motif", function () {

    //Fetching the class
    for (var key in descr)
        if (descr.hasOwnProperty(key))
            if ($(this).hasClass(key))
                regionId=key;
    
    //Filling the description block
    descrHTML=descr[regionId];
    $("#info_box").html(descrHTML);
    var top0=$("#sequences").offset().top;
    var height0=$("#logo_wrapper").outerHeight()+9;
    if (top0<window.pageYOffset+height0)
        $("#info_box").css("margin-top", window.pageYOffset-top0+height0+"px");
    $("#info_box").css("display", "block");
    var seqWidth=$("#sequences").outerWidth();
    var descrWidth=$("#info_box").outerWidth();
    $("body").css("width", Math.ceil(seqWidth+descrWidth+10)+"px");
    var maxWidth=parseInt($("#info_box > div > pre").css("max-width"));
    var width=$("#info_box > div > pre").outerWidth();
    if (width<maxWidth)
        $("#info_box > div > pre").attr("style", "overflow-x: hidden");
});
$(document).on("scroll", function() {
    if (!descrHTML)
        return;
    var top0=$("#sequences").offset().top;
    var height0=$("#logo_wrapper").outerHeight()+9;
    if (top0<window.pageYOffset+height0)
        $("#info_box").css("margin-top", window.pageYOffset-top0+height0+"px");
    else
        $("#info_box").css("margin-top", "0px");
});
$(window).on("resize", function() {
    var width=$("#sequences").outerWidth()+parseInt($("#info_box").css("max-width"))+parseInt($("body").css("padding-left"));
    $("#logo_wrapper").width(Math.min(width, $(window).width()));
});