function shapeLine(id, x, y, yCenter) {//Adjust existing line to make it diagonal w/o changing x of the ends
    var length=Math.sqrt(x*x+y*y);
    var angleStr=Math.asin(y/length)*(180/Math.PI)+"deg";
    var xMargin=(x-length)/2;
    $(id).css("margin-top", yCenter+"px");
    $(id).css("margin-left", xMargin+"px");
    $(id).css("width", length+"px");
    $(id).css("transform", "rotate("+angleStr+")");
    $(id).css("-o-transform", "rotate("+angleStr+")");
    $(id).css("-moz-transform", "rotate("+angleStr+")");
    $(id).css("-webkit-transform", "rotate("+angleStr+")");
}

function adjustDiagonalLines() {//Reashape connecting diagonal lines upon the change of connected objects
    var x,y,yCenter;
    x=$(".lines-container").outerWidth();
    if ($("#de_novo_tab").hasClass("active")) {
        y=$("#tool_content").outerHeight()-$("#de_novo_tab").outerHeight();
        yCenter=$("#de_novo_tab").outerHeight()+y/2;
        shapeLine("#bottom_line", x, y, yCenter);
        shapeLine("#top_line", x, 0, 0);
    } else {
        y=$("#tool_content").outerHeight()-$("#de_novo_tab").outerHeight()*2;
        yCenter=$("#de_novo_tab").outerHeight()*2+y/2;
        shapeLine("#bottom_line", x, y, yCenter);
        y=$("#de_novo_tab").outerHeight();
        yCenter=y/2;
        shapeLine("#top_line", x, -y, yCenter);
    }
}

function showErrorMessage(message) {//Show an error message as a notification
    $(".js-message-section").css("display", "block");
    $(".js-message").html(message);
    adjustDiagonalLines();
}

function processErrorMessage(response, mode) {//React to an error during the input checking
    $(".main-descr").css("display", "block");
    $("#tool_switcher_container").attr("style", "display: block");
    $("#main_descr").css("display", "block");
    $(".lines-container").attr("style", "display: block");
    $("#tool_content").css("display", "block");
    if ($.inArray(mode, ['STANDARD','ADVANCED'])!=-1)
        $(".form-switcher a[href='#" + mode.toLowerCase() +"_form']").tab('show');
    else if (mode=='PROTEOME')
        $("#tool_switcher_container a[href='#proteome_wide']").tab('show');
    $(".waiting-section").css("display", "none");
    showErrorMessage(response);
}

function processSuccess(response, mode) {//React to a successful input check
    var tag=response.substring(9,49);
    var is_email=response.substring(50,51);
    if (mode=='PROTEOME')
        $("#tool_switcher_container a[href='#proteome_wide']").tab('show');
    $(".waiting-section").css("display", "none");
    var url=window.location.href;
    url=url.substring(0,url.length-1);
    var path=url.substring(0,url.lastIndexOf("/"))+"/output/"+tag+"/results.html";
    $(".result-link").attr("href", path);
    $(".result-link").text(path);
    if (is_email=="1")
        $(".notification").css("display", "inline");
    $(".accepted-section").css("display", "block");
}

function processServerAnswer(xhttp, mode, check_interval) {//Process server response on the AJAX request
    if (xhttp.readyState==4 && xhttp.status==200) {
        var response=xhttp.responseText;
        if (response.charAt(0)=='.') {
            if (response.substring(0,8)==".SUCCESS") {
                processSuccess(response, mode);
                clearInterval(check_interval);
            }
        } else {
            processErrorMessage(response, mode);
            clearInterval(check_interval);
        }
    }
}

function askServerIfComplete(data, mode, check_interval) {//Send AJAX requests to the server
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function() {
        processServerAnswer(xhttp, mode, check_interval);
    };
    xhttp.open("POST", "../search/", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhttp.send(data);
}

function listenToUploads() {//Listen to events of file uploads
    $("#id_input_set").on("change", function() {
        if (this.files[0].size>1*1024*1024) {
            $("#standard_form")[0].reset();
            showErrorMessage("Input protein set: The uploaded file is too large. Max. allowed size is 1 MB");
        }
    });
    $("#id_input_zip").on("change", function() {
        if (this.files[0].size>5*1024*1024) {
            $("#advanced_form")[0].reset();
            showErrorMessage("Input protein set: The uploaded file is too large. Max. allowed size is 5 MB");
        }
    });
    $("#id_query_regions").on("change", function() {
        if (this.files[0].size>1*1024*1024) {
            $("#advanced_form")[0].reset();
            showErrorMessage("Query regions: The uploaded file is too large. Max. allowed size is 1 MB");
        }
    });
    $("#id_input_motif").on("change", function() {
        if (this.files[0].size>1*1024*1024) {
            $("#proteome_form")[0].reset();
            showErrorMessage("Input motif: The uploaded file is too large. Max. allowed size is 1 MB");
        }
    });
}

$(document).ready(function () {
    $(".help-mark").attr("data-toggle", "tooltip");
    $(".help-mark").attr("data-placement", "auto top");
    $('[data-toggle="tooltip"]').tooltip();
    $("#js_warning_section").css("display", "none");
    $("#all_content").css("display", "block");
    var mode=$("#mode").html();
    var sessionKey=$("#session_key").html();
    if (!sessionKey) {//Display input forms
        if ($.inArray(mode, ['STANDARD','ADVANCED'])!=-1) {
            $(".form-switcher a[href='#" + mode.toLowerCase() +"_form']").tab('show');
        } else if (mode=='PROTEOME')
            $("#tool_switcher_container a[href='#proteome_wide']").tab('show');
        adjustDiagonalLines();
        listenToUploads();
    } else {//Display submission & processing information
        if (mode=='PROTEOME')
            $("#tool_switcher_container a[href='#proteome_wide']").tab('show');
        $(".main-descr").css("display", "none");
        $("#tool_switcher_container").css("display", "none");
        $("#main_descr").css("display", "none");
        $(".lines-container").css("display", "none");
        $("#tool_content").css("display", "none");
        $(".waiting-section").css("display", "block");
        var opts={position: "relative", scale: "1.5"};
        var spinner = new Spinner(opts).spin();
        $(".spinner-container").append(spinner.el);
        var data="session_key="+sessionKey;
        var check_interval=null;
        check_interval=setInterval(function() {askServerIfComplete(data, mode, check_interval); }, 1000);
        return true;
    }
});

$(document).on("shown.bs.tab", "a[data-toggle='tab']", function() {
    adjustDiagonalLines();
});

$(document).on("click", ".pseudo-button", function() {
    $(".django-message-section").html("");
    if ($(".js-message-section").css("display")!="none") {
        $(".js-message-section").css("display", "none");
        adjustDiagonalLines();
    }
    $("#standard_form")[0].reset();
    $("#advanced_form")[0].reset();
    $("#proteome_form")[0].reset();
});

$(document).on("click", "#input_fasta_area_collapser", function() {
    $("#input_fasta_area").toggle();
    adjustDiagonalLines();
});

$(window).resize( function() {
    adjustDiagonalLines();
});