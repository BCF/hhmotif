var i,c;
var treeId="";
var treeIdPrev="";
var descrHTML="";
var activeDescr="";
var origSpans = [];

function shapeLine(id, x, y) {//Adjust existing line to make it diagonal w/o changing x of the ends
    var length=Math.sqrt(x*x+y*y);
    var angleStr=Math.asin(y/length)*(180/Math.PI)+"deg";
    var xMargin=(x-length)/2;
    $("#"+id).css("margin-top", 0.5*y+"px");
    $("#"+id).css("margin-left", xMargin+"px");
    $("#"+id).css("width", length+"px");
    $("#"+id).css("transform", "rotate("+angleStr+")");
    $("#"+id).css("-o-transform", "rotate("+angleStr+")");
    $("#"+id).css("-moz-transform", "rotate("+angleStr+")");
    $("#"+id).css("-webkit-transform", "rotate("+angleStr+")");
}

function addDiagonalLine(lineClass, lineId, x0, y0, x1, y1) {
    var xLeft,yLeft,xRight,yRight,yDelta;
    if (x0<x1) {
        xLeft=x0;
        yLeft=y0;
        xRight=x1;
        yRight=y1;
    } else {
        xLeft=x1;
        yLeft=y1;
        xRight=x0;
        yRight=y0;
    }
    yDelta=((yRight>yLeft)?1:-1)*(Math.max(y0,y1)-Math.min(y0,y1));
    $("body").append("<div class='"+lineClass+"' id='"+lineId+"'></div>");
    $("#"+lineId).css("left", xLeft+"px");
    $("#"+lineId).css("top", yLeft+"px");
    $("#"+lineId).css("width", (xRight-xLeft)+"px");
    $("#"+lineId).css("height", "5px");
    shapeLine(lineId, xRight-xLeft, yDelta);
}

function shapeConnectingLines() {//Connect the motif root with its leaves by lines
    var rootLeft=Math.round($("#root_0").position().left);
    var rootTop=Math.round($("#root_0").position().top);
    $(".leaf").each(function() {
        if (!this.id)
            return;
        var lineId="line_"+this.id;
        var yLeaf=$(this).position().top;
        if ($(this).hasClass("upper_leaf"))
            yLeaf+=$(this).outerHeight();
        var yRoot=rootTop;
        if ($(this).hasClass("lower_leaf"))
            yRoot+=$("#root_0").outerHeight();
        addDiagonalLine("motif-line", lineId, $(this).position().left, yLeaf, rootLeft, yRoot);
    });
}

$(document).ready(function() {
    var prefix=window.location.href.split(/\/out|\/down/)[0];
    $("#search_href").attr("href", prefix+"/search/");
    $("#guide_href").attr("href", prefix+"/guide/?l=de_novo_output");
    var width=$("#sequences").outerWidth()+parseInt($("#info_box").css("max-width"));
    $(".width-limit").css("width", width);
    width+=parseInt($("body").css("padding-left"));
    $("#logo_wrapper").width(Math.min(width, $(window).width()));
    var height=$("#logo_wrapper").outerHeight();
    $("#title_text").css("margin-top", height);
});
$(document).on("click", ".overview", function(e) {
    e.preventDefault();
    var motifRoot=$($(this).attr("href"));
    $("html,body").animate({scrollTop: motifRoot.offset().top-64}, 300);
    motifRoot.click();
});
$(document).on("click", ".motif", function() {
    var rootProt=-1;

    //Reseting to the original state after previous clicks
    activeDescr="";
    $("#info_box").css('display', 'none');
    $("#info_box").html('');
    $("#info_box > pre").css("overflow-x", "hidden");
    for (var spanClass in origSpans)
        if (origSpans.hasOwnProperty(spanClass)) {
            $(spanClass).html(origSpans[spanClass]);
            delete origSpans[spanClass];
        }
    if (treeIdPrev) {
        $(".m"+treeIdPrev).attr("style", "");
        $(".m"+treeIdPrev).removeAttr("id");
    }
    $(".motif-line").remove();
    
    //Fetching the class
    for (var key in descr)
        if (descr.hasOwnProperty(key))
            if ($(this).hasClass('m'+key))
                treeId=key;
    rootProt=+treeId.slice(0,4);
    
    //Filling the information box with the description of the motif tree
    descrHTML=descr[treeId];
    activeDescr=descrHTML.split("<pre>")[1];
    $("#info_box").html(descrHTML);
    var top0=$("#sequences").offset().top;
    var height0=$("#logo_wrapper").outerHeight();
    if (top0<window.pageYOffset+height0)
        $("#info_box").css("margin-top", window.pageYOffset-top0+height0+"px");
    $("#info_box").css("display", "block");
    var pseudoMSAWidth=$("#info_box > pre").outerWidth();
    var pseudoMSAWidthMax=parseInt($("#info_box > pre").css("max-width"));
    if (pseudoMSAWidth>=pseudoMSAWidthMax)
        $("#info_box > pre").css("overflow-x", "scroll");
    var maxWidth=parseInt($("#info_box > div > pre").css("max-width"));
    var width=$("#info_box > div > pre").outerWidth();
    if (width<maxWidth)
        $("#info_box > div > pre").attr("style", "overflow-x: hidden");
    
    //Highlighting the clicked motif tree root and assigning the temporary ids
    var scale="scale(1,1.5)";
    $(".m"+treeId).css({"font-weight": "bold", color: "fuchsia", display: "inline-block",
                       transform: scale, "-webkit-transform": scale, "-moz-transform": scale, "-ms-transform": scale, "-o-transform": scale});
    $(".m"+treeId).attr("id", function(index) {return "root_"+index;});
    treeIdPrev=treeId;
    
    //Highlighting the motif leaves in other proteins
    var tagBegin="<span class='leaf";
    tagEnd = "'>";
    var pseudoMSALines=activeDescr.split("\n").slice(4,-1);
    for (var lineIdx=0; lineIdx<pseudoMSALines.length; lineIdx++) {
        var line=pseudoMSALines[lineIdx];
        var tokens=line.split(/ {1,}/);
        var leafProt=+tokens[0];
        var prefix=".l"+tokens[0]+"_";
        var beginIdx=parseInt(tokens[1])-1;
        var beginClass=prefix+(1e3+Math.floor(beginIdx/80)+"").slice(-3);
        var endIdx=parseInt(tokens[3])-1;
        var endClass=prefix+(1e3+Math.floor(endIdx/80)+"").slice(-3);
        var beginSpan=$(beginClass).html();
        var len=beginSpan.length;
        var idx=beginIdx-beginIdx%80;
        var newBeginSpan="";
        origSpans[beginClass]=beginSpan;
        for (i=0; i<len; i++) {
            c=beginSpan.charAt(i);
            var isAA=/[A-Z]/.test(c);
            if (isAA) {
                if (idx==beginIdx) {
                    var tagHeart=" "+((leafProt<rootProt)?"upper":"lower")+"_leaf' id='leaf_"+lineIdx;
                    newBeginSpan+=tagBegin+tagHeart+tagEnd;
                }
            } else if ((c=="<" || (c==" " && idx%80===0)) && idx>beginIdx)
                newBeginSpan+="</span>";
            newBeginSpan+=c;
            if (isAA) {
                if (idx==endIdx) {
                    newBeginSpan+="</span>"+beginSpan.substring(i+1,len);
                    break;
                }
                idx++;
            } else if (c==">" && idx>beginIdx) {
                if (i<len-1 && beginSpan.charAt(i+1)==" ") {//if a motif root ends/breaks at the end of a FASTA line
                    newBeginSpan+=beginSpan.substring(i+1,len);
                    break;
                }
                newBeginSpan+=tagBegin+tagEnd;
            }
        }
        if (beginClass!=endClass) {//if the leaf encounters a FASTA line break
            newBeginSpan+="</span>";
            var endSpan=$(endClass).html();
            len=endSpan.length;
            var newEndSpan=tagBegin+tagEnd;
            origSpans[endClass]=endSpan;
            for (i=0; i<len; i++) {
                c=endSpan.charAt(i);
                if (c=="<")
                    newEndSpan+="</span>";
                newEndSpan+=c;
                if (/[A-Z]/.test(c)) {
                    if (idx==endIdx) {
                        newEndSpan+="</span>"+endSpan.substring(i+1,len);
                        break;
                    }
                    idx++;
                } else if (c==">")
                    newEndSpan+=tagBegin+tagEnd;
            }
            $(endClass).html(newEndSpan);
        }
        $(beginClass).html(newBeginSpan);
    }
    
    //Connecting the motif root with its leaves by lines
    shapeConnectingLines();

});
$(document).on("scroll", function() {
    if (!activeDescr)
        return;
    var top0=$("#sequences").offset().top;
    var height0=$("#logo_wrapper").outerHeight();
    if (top0<window.pageYOffset+height0)
        $("#info_box").css("margin-top", window.pageYOffset-top0+height0+"px");
    else
        $("#info_box").css("margin-top", "0px");
});
$(window).on("resize", function() {
    var width=$("#sequences").outerWidth()+parseInt($("#info_box").css("max-width"))+parseInt($("body").css("padding-left"));
    $("#logo_wrapper").width(Math.min(width, $(window).width()));
    shapeConnectingLines();
});
