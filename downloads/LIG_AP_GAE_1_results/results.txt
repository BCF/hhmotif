+------------------------------------------------------------------------------+
| Input parameters:                                                            |
+------------------------------------------------------------------------------+
Query regions file: no
Orthology search: yes
Gap restriction: yes
Surface accessibility check: yes
Intrinsical disorder check: no
Homology/domain filtering: yes
Show best suboptimal if no motifs found: no
+------------------------------------------------------------------------------+
| Proteins coding:                                                             |
+------------------------------------------------------------------------------+
0000: O35550|RABE1_RAT
0001: P47160|ENT3_YEAST
0002: P78813|YCTB_SCHPO
0003: Q03769|ENT5_YEAST
0004: Q18453|Q18453_CAEEL
0005: Q6ULP2|AFTIN_HUMAN
0006: Q9UMZ2|SYNRG_HUMAN
+------------------------------------------------------------------------------+
| Short motif overview:                                                        |
+------------------------------------------------------------------------------+
0000:	
0001:	267 DDDDEFSEFQSAVP 280 (27.72)
0002:	302 EDDGFGDFQSS 312 (27.64), 340 DDDAFDDFQSA 350 (18.53)
0003:	218 DDDEFD 223 (14.23), 321 NSNSDDDDDEFGDFQSET 338 (31.83)
0004:	249 VDDSKKKDDDDGFGDFVSSRSSQP 272 (27.41)
0005:	20 AEDDDDDEFGEF 31 (29.32), 430 DDFGDFGDFGSASGSTP 446 (20.75), 547 DDCNGFQDSDDFADFSSAGPSQ 568 (19.34)
0006:	471 DDSFSDFQELPA 482 (19.42), 774 DDFADFHSS 782 (19.56), 1022 DDFGEFQS 1029 (22.90)
+------------------------------------------------------------------------------+
| Detailed information:                                                        |
+------------------------------------------------------------------------------+
0000:
******************************
0001:
Regex: ...[D][ADE][F][DGS][DE][F][Q][S]...
Regex p-value: 0.004
Av. alignment score: 27.72
0001  267 DDDDEFSEFQSAVP 280
0003  326 DDDDEFGDFQS 336 (35.53)
0002  339 NDDDAFDDFQSAPS 352 (24.72)
0006    1022 DDFGEFQSEKP 1032 (22.91)
******************************
0002:
Regex: .[DE][D][DEG][F][G][D][F]...
Regex p-value: 0.001
Av. alignment score: 27.64
0002  302 EDDGFGDFQSS 312
0004  257 DDDGFGDFVSS 267 (35.16)
0003  327 DDDEFGDFQSE 337 (30.79)
0005   429 EDDFGDF 435 (16.97)
----------
Regex: [D][DS][D][ADE][F][ADS].....
Regex p-value: 0.229
Av. alignment score: 18.53
0002  340 DDDAFDDFQSA 350
0001  268 DDDEFSEFQSA 278 (24.72)
0005  554 DSDDFADFSSA 564 (17.07)
0003  218 DDDEFD 223 (13.80)
******************************
0003:
Regex: [D][D][D][AE][F].
Regex p-value: 0.004
Av. alignment score: 14.23
0003  218 DDDEFD 223
0005   24 DDDEFG 29 (14.98)
0001  268 DDDEF 272 (13.92)
0002  340 DDDAFD 345 (13.80)
----------
Regex: ......[DE][D][D][EG][F][GS][DE][F][QV][S]..
Regex p-value: 0.000
Av. alignment score: 31.83
0003  321 NSNSDDDDDEFGDFQSET 338
0001  262 ADEEEDDDDEFSEFQS 277 (35.53)
0002        302 EDDGFGDFQSSA 313 (30.79)
0004  251 DSKKKDDDDGFGDFVSSR 268 (29.17)
******************************
0004:
Regex: ........[DE][DP][DEF][DEG][F][GV][D][F].[S][AES].....
Regex p-value: 0.068
Av. alignment score: 27.41
0004  249 VDDSKKKDDDDG-FGDFVSSRSSQP 272
0002          302 EDDG-FGDFQSS 312 (35.16)
0003   320 INSNSDDDDDE-FGDFQSETSPDT 342 (29.17)
0001        233 KQEPED-FVDFFSSESSKP 250 (24.05)
0005  422 VTCNDINEDDFGDFGDFGSASGSTP 446 (21.25)
******************************
0005:
Regex: ......[D][DE][F][G][DE][FM]
Regex p-value: 0.010
Av. alignment score: 29.32
0005   20 AEDDDDDEFGEF 31
0003  323 NSDDDDDEFGDF 334 (32.30)
0001  371 TDQDDDDEFGEM 382 (30.69) nonreciprocal
0006        668 DDFGEF 673 (24.97) nonreciprocal
----------
Regex: [D][DG].[G].[F]...........
Regex p-value: 0.007
Av. alignment score: 20.75
0005  430 DDFGDFGDFGSASGSTP 446
0006  668 DDFGEFSLFGEYSGLAP 684 (24.04) nonreciprocal
0004  257 DDDG-FGDFVSSRSSQP 272 (21.25)
0002  304 DGFGDF 309 (16.97)
----------
Regex: .........[D][DEG][F][AG][D][F][HQSV][S].....
Regex p-value: 0.085
Av. alignment score: 19.34
0005  547 DDCNGFQDSDDFADFSSAGPSQ 568
0006           774 DDFADFHS 781 (24.01)
0004  250 DDSKKKDDDDGFGDFVSSRSSQ 271 (17.18)
0003  320 INSNSDDDDDEFGDFQS 336 (16.83)
******************************
0006:
Regex: [D][D][EGS][F][GS][D][F].....
Regex p-value: 0.004
Av. alignment score: 19.42
0006  471 DDSFSDFQELPA 482
0003  328 DDEFGDFQSETS 339 (20.16)
0002  303 DDGFGDFQSSAA 314 (19.36)
0004  258 DDGFGDF 264 (18.74)
----------
Regex: [DE][DG][F][AGV][D][F][FHQV][S][S]
Regex p-value: 0.017
Av. alignment score: 19.56
0006  774 DDFADFHSS 782
0004  259 DGFGDFVSS 267 (20.74)
0001  237 EDFVDFFSS 245 (19.00)
0002  304 DGFGDFQSS 312 (18.95)
----------
Regex: [D][DEG][F][GS][DE][F].[S]
Regex p-value: 0.040
Av. alignment score: 22.90
0006  1022 DDFGEFQS 1029
0003   329 DEFGDFQS 336 (27.97)
0001   270 DEFSEFQS 277 (22.91)
0004   259 DGFGDFVS 266 (17.82)
