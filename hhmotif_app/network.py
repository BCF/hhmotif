#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
import cgi, json, os
import cgitb; cgitb.enable()



#this maybe relevant in a new version
#main_node = 'NP_012649'

form = cgi.FieldStorage()
job = form['job'].value

nodes_file = os.path.join("..", job + "_Attribute_cytoscape.sif")
#nodes_file = "NP_012649-08091647_Attribute_cytoscape.sif"
#nodes_file = "NP_012649-11131119_Attribute_cytoscape.sif"

#nodes = '{"name": "NP_012649", "eval": 8.13879e-114, "org": "Saccharomyces cerevisiae S288c", "score": 1}, {"name": "NP_080204.1", "eval": 25.9481, "org": "Mus musculus", "score": 0.3878}';
#nodes = ""
nodes = []

att_file = open(nodes_file)
lines = att_file.readlines()

n=0
for line in lines:
	attrs = iter(line.split("\t"))
	#nodes += "{name: '"+ attrs.next() +"', eval:"+ attrs.next() +", org:'"+ attrs.next() +"', score:"+ attrs.next().rstrip() +"},"
	nodes.append( { "name": attrs.next(), "eval": attrs.next(), "org": attrs.next(), "score": attrs.next().rstrip()} )
	n += 1

#in the current version we dont need edges!
#edges = ""

#main_file = open("files/NP_012649-08091647_Main_cytoscape.sif")
#lines = main_file.readlines()

#i=0
#for line in lines:
#	attrs = iter(line.split("\t"))
#	idOne = attrs.next()
#	type = attrs.next()
#	idTwo = attrs.next().rstrip()
	
	#if(type == 'bb'):
#	edges += "{source: '"+ idOne+"', type:'"+ type +"', target: '"+ idTwo +"'},"
#	i += 1

#print '[' + nodes[:-1] + ']'

print "Content-Type: application/json"
print
print json.dumps(nodes)

#print "MORFEUS.init(["+nodes+"],'chart');"
#print """
#			});
#		</script>
#	</head>
#	<body>
#		<div id="chart"></div>
#	</body>
#</html>	
#	"""