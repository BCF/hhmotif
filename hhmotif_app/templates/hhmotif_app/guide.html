{% extends "hhmotif_app/base.html" %}

{% block load_additional_scripts %}
<script type="text/javascript" src="../static/js/guide.min.js"></script>
{% endblock %}

{% block guide %}<li class="active"><a href="../guide/">Guide</a></li>{% endblock %}

{% block content %}
<div class="row">
    <div class="sidebar-container">
        <ul id="guide_nav_menu" class="nav">
            <li class="dropdown">
                <button id="btn-menu" type="button" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-menu-hamburger" style="font-size: 2em"></span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <div id="guide_nav_menu_inner_div" class="navbar sidebar-left"></div>
                    </li>
                </ul>
            </li>
        </ul>
        <div id="guide_nav_normal" class="navbar sidebar-left">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a class="sidebar-link guide-pseudo-button" href="#general_info">1. General Information</a></li>
                <li><a class="sidebar-link guide-pseudo-button" href="#de_novo_input">2. <i>De Novo</i> Input</a></li>
                <li><a class="sidebar-link guide-pseudo-button" href="#de_novo_output">3. <i>De Novo</i> Output</a></li>
                <li><a class="sidebar-link guide-pseudo-button" href="#proteome_wide_input">4. Proteome-wide Input</a></li>
                <li><a class="sidebar-link guide-pseudo-button" href="#proteome_wide_output">5. Proteome-wide Output</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-lg-9 col-md-9 col-md-offset-3 col-sm-11 col-sm-offset-1 col-xs-11 col-xs-offset-1" style="text-align: justify">

        <h2 id="general_info" class="text-section">1. General Information</h2>
        <hr class="line"></hr>

        <h3 class="text-sub-section">Terms of use</h3>
        <p>This tool is free for both academic and non-academic use.
        No registration is required to access the full functionality of the server.
        However, if you need to process larger protein sets or perform motif searches frequently, please <a href="../contact/">contact</a> the authors to get a standalone version for LINUX.
        </p>
        <p>Please note that results are stored on the server only for 7 days.
        It is recommended to either download the plain text summary or save the complete result HTML page locally through your browser (right click -> Save page as...) for further access.
        </p>

        <h3 class="text-sub-section">Requirements</h3>
        <p>The website design is kept simple and light-weight.
        Cookies as well as additional plugins are not required.
        All modern browsers (starting from IE9) are supported.
        The website is fully mobile-friendly.
        </p>
        <p>This website uses Javascript, both for submit and result pages.
        Please make sure that Javascript is enabled whenever you use the server or view results.
        </p>

        <h3 class="text-sub-section">Security and privacy</h3>
        <p>The web server does not collect any information about the users.
        Submission of the data can be done completely anonymously; however, users have the option to provide an e-mail address for a notification on job completion.
        Each accepted job gets a unique randomly generated identifier (a 40-character long hexadecimal string), which is required to access the results.
        Please share this identifier only with persons, who are authorized to view the results.
        The results will be permanently deleted from the server on the 8th day upon the job completion.
        </p>

        <h3 class="text-sub-section">More information</h3>
        <p>This guide addresses mainly the technical aspects of using the server.
        For information about the scope of use and internal workflow, please visit the <a href="../about/">About</a> page.
        </p>

        <h2 id="de_novo_input" class="text-section">2. <i>De Novo</i> Input</h2>
        <hr class="line"></hr>

        <h3 class="text-sub-section">Input protein set</h3>
        <p>The input set must contain ONLY full protein sequences, as the evolutionary conservation and solvent accessibility filters will be otherwise skewed!
        To restrict a motif search only to specific domains, the <a class="guide-pseudo-button" href="#query_regions_file">query regions file</a> in advanced mode must be used.
        The number of proteins in the input set must be no lower than 3 and no higher than {{prots_max}}.
        </p>
        <ul style="margin-left: 2em">
            <li>
                 <h4 class="text-sub2-section">Standard mode</h4>
                 <p>A FASTA file containing sequences of all proteins in the set, one sequence per protein.
                 Alignments as well as empty sequences are not allowed.
                 Each sequence must be no shorter than 50 and no longer than 10,000 amino acids.
                 Alternatively, the file content can be copied in the corresponding text field.
                 There is no difference between these input options; however, the user must use either text field or file, but not both simultaneously.
                 </p>
                 <div class="sample-download" style="float: left"><a href="../downloads/TRG_LysEnd_APsAcLL_3.fasta" download target="_blank">Sample</a></div>
                 <div style="clear:both"></div>
                 <div style="height: 0.5em"></div>
            </li>
            <li>
                <h4 class="text-sub2-section">Advanced mode</h4>
                <p>A set of FASTA files with input protein sequences, one file per protein. Each file can be single or multiple FASTA.
                A multiple FASTA will be understood as a collection of orthologs of the same protein.
                In case orthology search is activated, only the first FASTA records will be considered and orthologs will be re-detected with the standard 2-way-BLAST-based algorithm.
                Alignments as well as empty sequences are not allowed.
                Each sequence must be no shorter than 50 and no longer than 10,000 amino acids.
                The set must be provided as ZIP archive containing only FASTA files or, alternatively, a single directory with FASTA files; subdirectories are not allowed.
                All files and directories with names not starting with a Roman letter or digit will be ignored.
                Filenames must not contain colons, as well as non-ANSI characters.
                Length of a filename must not exceed 72 characters.
                </p>
                <p>A FASTA file can be also submitted instead of the ZIP archive, as in standard mode.
                In this case all the sequences will be treated as separate proteins and possibilities to submit user-defined orthologs as well as regions of interest are lost.
                </p>
                <div class="sample-download" style="float: left"><a href="../downloads/TRG_LysEnd_APsAcLL_3.zip" download target="_blank">Sample</a></div>
                <div style="clear:both"></div>
            </li>
        </ul>

        <h3 id="query_regions_file" class="text-sub-section">Query regions file (advanced mode only)</h3>
        <p>A file specifying the opening and closing residue numbers of the regions of interest in each protein to look for motifs in.
        This option is useful for a motif search only in specified domains (never truncate the input sequences for this, as this will skew the evolutionary conservation and solvent accessibility filters!).
        Each line of this file is a filename followed immediately by the colon and then sequence regions (as opening and closing residues connected by dash) separated by commas.
        If an input FASTA file name is not in this file, the whole sequence is considered.
        Filenames that do not correspond to files in the protein set are not allowed.
        Specifying regions that are shorter than 20 residues should be avoided, as these would not contain enough information for reliable motif detection.
        The algorithm can move boundaries between masked and unmasked regions for 1 residue in either direction if one of them is too short.
        The regions file can be submitted only with a ZIP archive as data set input, not with a FASTA file.
        </p>
        <div class="sample-download" style="float: left"><a href="../downloads/TRG_LysEnd_APsAcLL_3_query_regions.txt" download target="_blank">Sample</a></div>
        <div style="clear: both"></div>

        <h3 class="text-sub-section">Search for orthologs</h3>
        <p>Search for proteins with the same function and evolutionary origin in other species.
        These are considered in the evolutionary conservation filtering: motifs found in non-conserved stretches will get lower scores or will ultimately be removed from the results.
        The search is performed against the NCBI non-redundant database.
        No orthologs will be found for sequences that do not correspond exactly to one of the database records.
        This means, no orthologs will also be found for partial or mutant sequences.
        </p>
        <p>Orthology search is always on in standard mode.
        </p>

        <h3 class="text-sub-section">Restrict gaps</h3>
        <p>Restrict gap length in found motif instances to 1 residue.
        This restriction conforms with almost all known motifs.
        However, if long flexible linkers are expected to be contained in the sought-for motifs, this option should be deactivated.
        </p>
        <p>Gap restriction is always on in standard mode.
        </p>

        <h3 class="text-sub-section">Check surface accessibility</h3>
        <p>Predict, resolve, and mask the regions that are most likely lie inside globular domains and do not show up on the surface.
        However, it may lead to the loss of motifs located in cavities.
        Transmembrane regions will also be generally predicted as not accessible and subsequently masked.
        </p>
        <p>Surface accessibility check is always on in standard mode.
        </p>

        <h3 class="text-sub-section">Check disorder</h3>
        <p>Predict, resolve, and mask the regions that are most likely to be too ordered or rigid to harbor functional short motifs.
        This option is shown to result in a slight increase in motif prediction accuracy.
        </p>
        <p>Disorder check is always off in standard mode.
        </p>

        <h3 class="text-sub-section">Smart homology filtering</h3>
        <p>Discard motif trees located primarily to longer regions of homology and shared conserved domains.
        This filter works on pairwise level within the context of individual motif trees and therefore does not mask whole input sequences or their parts by themselves.
        Motif discovery in non-homologous regions of the sequences is not prohibited.
        Therefore, {{tool}} can tolerate homology among input sequences to quite large extent.
        However, the correct motif prediction in very close homologs is still not possible.
        Deactivating this filter normally results in increase in recall with simultaneous decrease in precision.
        </p>
        <p>The option is always on in standard mode.
        </p>

        <h3 class="text-sub-section">Show best suboptimal if no motifs found</h3>
        <p>If no motif trees can be found according to the input settings, then relax the search parameters and show the best suboptimal motif tree.
        This option may be useful to evaluate potential motif candidates, even if they are not strong enough to pass through the filters of HH-MOTiF.
        The suboptimal candidate will not be shown, if at least one strong enough motif tree is found.
        If this option is activated and gets exercised, the corresponding message appears at the top of the results page.
        In some extreme cases (for example, if submitted sequences are too similar, too diverse or specified query regions are too narrow), however, no results may be found even with the option activated.
        </p>
        <p>The option is always off in standard mode.
        </p>

        <h3 id="maximal_regex_pv" class="text-sub-section">Maximal regex p-value</h3>
        <p>After predicting motif trees, the server tries to represent each region in form of a classical motif by building the corresponding regular expression (regex).
        The quality of the resulting regex depends heavily on the degree of the underlying motif's conservation.
        The regex p-value is calculated on the basis of single amino acid frequencies, pair frequencies, and number of proteins containing the putative motif.
        The p-value lies in the range [0, 1] and estimates the probability of this motif to occur in these particular proteins just by chance.
        However, a regex is a very poor representation for weakly conserved motifs and therefore often shows high values even for experimentally confirmed motifs.
        That is why the default value is set to {{pv_max}}; however, if sought-for motifs are expected to be highly conserved, a value of 0.1 may be more appropriate.
        </p>
        <p>Maximal regex p-value is always {{pv_max}} in standard mode.
        </p>

        <h2 id="de_novo_output" class="text-section">3. <i>De Novo</i> Output</h2>
        <hr class="line"></hr>

        <br />
        <div class="sample-output"><a href="../downloads/TRG_LysEnd_APsAcLL_3_results_advanced/results.html" target="_blank">Output sample</a></div>

        <h3 class="text-sub-section">Results overview</h3>
        <p>All found <a class="guide-pseudo-button" href="#motif_trees">motif trees</a> are displayed in the form of a table, one line per motif tree.
        Protein number-codes are provided in place of the user defined FASTA headers or filenames (nevertheless, the full FASTA records are visible further down the page).
        Each tree repesents a distinct motif, which is generally independent from other found motifs.
        More information about a motif appears upon clicking.
        </p>
        <p>Absence of the results means that no motifs could be found.
        In this case a search with more relaxed parameters might be helpful.
        </p>

        <h3 class="text-sub-section">Input sequences</h3>
        <p>The input sequences are shown in the HTML version of the output.
        All sequences are number-coded (starting from 0000).
        In standard mode the order is preserved from the input file.
        In advanced mode the sequences are ordered alphabetically according to input filenames; only the first sequence from each file is shown (although the others are considered - as orthologs - during the calculations).
        If <a class="guide-pseudo-button" href="#query_regions_file">query regions</a> were specified, they are also indicated in the output (residues lying outside are masked and colored <span style="color: #bbb">gray</span>).
        </p>

        <h3 id ="motif_trees" class="text-sub-section">Motif trees</h3>
        <p>Motif trees are the most important information on the output page.
        A motif tree is an evolution-oriented model of a putative sequence motif.
        </p>
        <p>According to the model, each tree consists of two distinct components:
        <ul style="margin-left: 2em">
            <li>The motif root in a particular protein</li>
            <li><a class="guide-pseudo-button" href="#motif_leaves">Motif leaves</a> in other proteins</li>
        </ul>
        Motif roots are the 'hub points' for multiple motif leaves.
        A motif root has a high-quality alignment with each of the leaves, while pairwise alignments between individual leaves are not guaranteed.
        Only motif roots are highlighted (in <u style="color: red">underlined red</u>) in the input sequences by default.
        Upon clicking the highlighting changes to <b><u style="color: fuchsia">underlined bold purple</u></b>, the leaves are also highlighted, and additional information is shown in the motif information panel to the right.
        Dashed purple lines connect the motif root with the leaves in the input sequences.
        </p>

        <h3 id="motif_information_panel" class="text-sub-section">Motif information panel</h3>
        <p>The panel appears upon clicking on a motif root within a protein sequence or the results overview section.
        It contains, besides the sequence logo, the information about the motif's regex, scores, <a class="guide-pseudo-button" href="#motif_leaves">leaves</a>, as well as a link to the corresponding FASTA file.
        </p>
        <p>The regular expression (regex) is the compact representation of conserved amino acids in a motif.
        Dots represent completely unconserved positions (a.k.a. columns).
        If there is at least one gap or a mismatched residue in the column, it is considered completely unconserved.
        By conserved or partially conserved columns all occurring amino acids are listed in square brackets.
        The <a class="guide-pseudo-button" href="#maximal_regex_pv">regex p-value</a> is one of the estimates of a motif's strength.
        </p>
        <p>The average alignment score is calculated upon alignment scores of all the root-<a class="guide-pseudo-button" href="#motif_leaves">leaf</a> pairs, which are in turn based on the Viterbi algorithm scores.
        The higher the score - the stronger the motif tree.
        </p>
        <p>The motif root appears as the first sequence stretch and is followed by the motif leaves ordered by the alignment score.
        Although true sequence alignments are guaranteed only for root-leaf pairs (and not between individual leaves), for visualization purposes the whole tree is shown as multiple sequence alignment (MSA).
        In this context it is called a pseudo-MSA.
        </p>
        <p>The corresponding FASTA file contains the (pseudo-)alignment of the motif tree.
        It can be further submitted as an <a class="guide-pseudo-button" href="#input_motif">input motif file for a proteome-wide motif search</a> in order to find the same motif in other proteins and/or organisms.
        </p>

        <h3 id="motif_leaves" class="text-sub-section">Motif leaves</h3>
        <p>The motif leaves of each <a class="guide-pseudo-button" href="#motif_trees">motif tree</a> are listed together with their alignment scores (in brackets) in the motif information panel.
        They are also highlighted in <span style="color: fuchsia">purple</span> upon clicking on the corresponding motif root.
        The <u style="color: red"><span style="color: black">red underlinement</span></u> of all the motif roots is kept so that one can easily see the overlaps of leaves of a particular motif tree with the roots of other trees.
        The strong and unambiguous results would imply mutually overlapping motif trees with definite reciprocality.
        However, owing to weak conservation of real world motifs as well as the presence of noise, it is not always the case.
        Therefore, motif leaves that fail to overlap with another motif's root by at least 3 residues are marked as 'nonreciprocal' in the pseudo-MSAs for further user consideration.
        Due to their potentially large number and overlapping nature, the motif leaves are not displayed by default (they appear upon clicking).
        </p>

        <h3 id="de_novo_plain_text_summary" class="text-sub-section">Plain text summary</h3>
        <p>The output is alternatively provided in the plain text format, which is more suitable for machine parsing.
        If provided, the job name is displayed at the beginning of the file.
        The information in the file is divided into four sections.
        The first section contains input parameters.
        The second section contains the protein coding for the next two sections.
        In the third section all the input proteins (including those without any found motifs) are listed with all the predicted motifs tree roots appearing in sequence order with the average alignment scores in brackets.
        In the fourth section - detailed information - every found <a class="guide-pseudo-button" href="#motif_trees">motif tree</a> is listed together with its regex, scores and motif leaves in other proteins.
        Motif trees with roots in one protein are separated with <code>-</code>s. Different proteins are separated with <code>*</code>s.
        </p>

        <h3 class="text-sub-section">Submission information</h3>
        <p>The information about the job submission (including all the input parameters) is provided at the bottom of the output page.
        </p>

        <h2 id="proteome_wide_input" class="text-section">4. Proteome-wide Input</h2>
        <hr class="line"></hr>

        <h3 id="input_motif" class="text-sub-section">Input motif file</h3>
        <p>The aligned FASTA file with already known instances of this motif.
        For a better performance as many as possible should be provided.
        If there are several instances of the motif in the same protein (i.e., the motif is a tandem repeat), they should be provided separately.
        At least 2 motif instances must be provided, but much more are needed for accurate and reliable results.
        The alignment must be no shorter than 3 and no longer than 50 columns.
        </p>
        <p>The results from a <i>de novo</i> motif search on this server can be very easily submitted as the input for a proteome-wide search.
        For this download the FASTA file in the corresponding <a class="guide-pseudo-button" href="#motif_information_panel">motif information panel</a>.
        </p>
        <div class="sample-download" style="float: left"><a href="../downloads/test_motif.fasta" download target="_blank">Sample</a></div>
        <div style="clear: both"></div>

        <h3 class="text-sub-section">Organism</h3>
        <p>The organism to search for the motif in. Human and the mostly popular model organisms are supported.
        </p>

        <h3 class="text-sub-section">Only full-length matches</h3>
        <p>Leave only those matches that have the same length as the input motif.
        This option is useful when the input motif begins and ends with conserved residues.
        It must be turned off if less conserved flanking residues are provided.
        </p>

        <h2 id="proteome_wide_output" class="text-section">5. Proteome-wide Output</h2>
        <hr class="line"></hr>

        <br />
        <div class="sample-output"><a href="../downloads/test_results_proteome/results.html" target="_blank">Output sample</a></div>

        <h3 class="text-sub-section">Submission information</h3>
        <p>The information about the job submission is partially provided at the beginning and is continued at the bottom of the output page.
        Besides the download link to the original input file, the calculated consensus sequence is provided.
        </p>

        <h3 class="text-sub-section">Sequences with found motifs</h3>
        <p>The sequences from the selected organism's proteome, in which motifs were found.
        The sequences are taken from the NCBI database; the corresponding identifiers (GI and accession numbers) are provided in the headers.
        The found motifs are highlighted in <u style="color: red">underlined red</u>.
        A small information box at the right side with the actual underlying alignment appears upon clicking.
        </p>
        <p>Assuming no motifs are found, the corresponding message is shown instead of the sequences.
        The failure to find any results usually indicates that the input motif is too short and/or too unconserved.
        </p>

        <h3 class="text-sub-section">Plain text summary</h3>
        <p>The output is alternatively provided (at the bottom of the page) in plain text format, which is more suitable for machine parsing.
        If provided, the job name is displayed at the beginning of the file.
        The information in the file is divided into three sections.
        The first section contains only the input motif consensus sequence.
        The second section lists the input parameters.
        The last section consists of all found motif matches.
        Each motif match is provided in the form of an alignment of the found hit with the input motif consensus sequence.
        Below the alignment its Viterbi algorithm score is indicated.
        Motifs found in different proteins are separated with <code>*</code>s.
        </p>

    </div>

</div>
<div style="height: 99vh"></div>

<!--Hidden sesion info-->
<div class="session-info">
    <a id="inner_router" class="guide-pseudo-button" href="#{{inner_link}}"></a>
</div>
{% endblock %}
