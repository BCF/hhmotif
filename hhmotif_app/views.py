import re, os, pickle, shutil, datetime, mimetypes
from multiprocessing import Process
from django.core.files.base import ContentFile
from django.shortcuts import render, redirect
from django.contrib.sessions.models import Session
from django.http import HttpResponse, StreamingHttpResponse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.safestring import mark_safe
from wsgiref.util import FileWrapper
from hhmotif_app.forms import CookiesForm, StandardForm, AdvancedForm,\
ProteomeForm
##
from hhmotif_site.custom_methods import get_number_of_tasks, queue_the_task,\
generate_tag, ask_status, clear_record, start_session, get_session_dict,\
end_session, schedule_end_session
##
from hhmotif_site.config import MAX_TASKS_QUEUED, PV_MAX, RSA_MIN, PROTS_MAX
from hhmotif_site.settings import BASE_DIR

TOOL_NAME = '<span class="tool-name">HH-MOTiF</span>'

@csrf_exempt
def index(request):
    return redirect('search/')

@csrf_exempt
def search(request):
    
    # AJAX request
    if request.is_ajax():
        session_key = request.POST.get('session_key', '')
        session = None
        if re.search(r'^[0-9a-f]+$', session_key):
            session = get_session_dict(session_key)
        if not session:
            return HttpResponse('Connection timeout')
        tag = session.get('tag', None)
        status = ask_status(tag)
        if status == None:
            return HttpResponse('.RUNNING')
        elif status == '':
            user_email = session.get('user_email', None)
            is_email = int(bool(user_email))
            if session.get('sent', None):
                clear_record(tag, 'prioritized')
                return HttpResponse('.SUCCESS {} {}'.format(tag, is_email))
            mode = session.get('mode', None)
            ortho_search = session.get('ortho_search', None)
            gepf = session.get('gepf', None)
            rsa = session.get('rsa', None)
            disorder_check = session.get('disorder_check', None)
            homology_filter = session.get('homology_filter', None)
            pv_max = session.get('pv_max', None)
            ignore_filters = session.get('ignore_filters', None)
            proteome = session.get('proteome', None)
            full_length_only = session.get('full_length_only', None)
            job_name = session.get('job_name', None)
            submit_time = session.get('submit_time', None)
            args = pickle.dumps((mode, ortho_search, gepf, rsa, disorder_check,\
            homology_filter, pv_max, ignore_filters, proteome,\
            full_length_only, user_email, job_name, submit_time, tag))
            ##
            queue_the_task('heavy', args, tag)
            end_session(session_key)
            clear_record(tag, 'prioritized')
            return HttpResponse('.SUCCESS {} {}'.format(tag, is_email))
        else:
            end_session(session_key)
            clear_record(tag, 'prioritized')
            return HttpResponse(status)
    
    # Generating empty forms
    cookies_form = CookiesForm()
    standard_form = StandardForm()
    advanced_form = AdvancedForm()
    proteome_form = ProteomeForm()

    # GET request
    if not request.POST:
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'pv_max': str(PV_MAX),
                 'prots_max': str(PROTS_MAX),
                 'mode': 'STANDARD',
                 'standard_form': standard_form,
                 'advanced_form': advanced_form,
                 'proteome_form': proteome_form}
        return render(request, 'hhmotif_app/index.html', dict_)
    
    # Generating empty forms
    standard_form = StandardForm()
    advanced_form = AdvancedForm()
    proteome_form = ProteomeForm()
    mode = request.POST['form_type'].split('_')[0].upper()
    input_file = None
    text_input = False
    regions_file = None
    form_error = ''
    
    # Check for server overload
    if get_number_of_tasks('heavy') >= MAX_TASKS_QUEUED:
        message = 'The server is overloaded now. Please try again later'
        messages.info(request, message)
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'pv_max': str(PV_MAX),
                 'prots_max': str(PROTS_MAX),
                 'mode': mode,
                 'standard_form': standard_form,
                 'advanced_form': advanced_form,
                 'proteome_form': proteome_form}
        return render(request, 'hhmotif_app/index.html', dict_)

    # Process submission of main forms
    if request.POST['form_type'] == 'standard_form':
        standard_form = StandardForm(data = request.POST, files = request.FILES)
        if standard_form.is_valid():
            input_fasta_text = standard_form.cleaned_data['input_text'] or ''
            if input_fasta_text:
                text_input = True
                input_file = ContentFile(input_fasta_text.encode())
            if 'input_set' in request.FILES:
                if text_input:
                    form_error =\
                    'Select ONLY one input method: either text field or file'
                    ##
                input_file = request.FILES['input_set']
            ortho_search = True
            gepf = '999'
            rsa = str(RSA_MIN)
            disorder_check = False
            homology_filter = True
            pv_max = str(PV_MAX)
            ignore_filters = False
            proteome = ''
            full_length_only = False
            user_email = standard_form.cleaned_data['user_email'] or ''
            job_name = standard_form.cleaned_data['job_name'] or ''
        else:
            for key in standard_form.errors:
                form_error = '{} {}\n'.format(key, standard_form.errors[key])
    elif request.POST['form_type'] == 'advanced_form':
        advanced_form = AdvancedForm(data = request.POST, files = request.FILES)
        if advanced_form.is_valid():
            input_file = request.FILES['input_zip']
            if 'query_regions' in request.FILES:
                regions_file = request.FILES['query_regions']
            gepf_infinity = advanced_form.cleaned_data['gepf_infinity']
            gepf = '999' if gepf_infinity else '0.6'
            ortho_search = advanced_form.cleaned_data['ortho_search']
            rsa_check = advanced_form.cleaned_data['rsa_check']
            rsa = str(RSA_MIN) if rsa_check else '0.0'
            disorder_check = advanced_form.cleaned_data['disorder_check']
            homology_filter = advanced_form.cleaned_data['homology_filter']
            pv_max = str(advanced_form.cleaned_data['pv_max'])
            ignore_filters = advanced_form.cleaned_data['ignore_filters']
            proteome = ''
            full_length_only = False
            user_email = advanced_form.cleaned_data['user_email'] or ''
            job_name = advanced_form.cleaned_data['job_name'] or ''
        else:
            for key in advanced_form.errors:
                form_error = '{} {}\n'.format(key, advanced_form.errors[key])
    elif request.POST['form_type'] == 'proteome_form':
        proteome_form = ProteomeForm(data = request.POST, files = request.FILES)
        if proteome_form.is_valid():
            if 'input_motif' in request.FILES:
                input_file = request.FILES['input_motif']
            ortho_search = False
            gepf = '0.6'
            rsa = '0.16'
            disorder_check = False
            homology_filter = True
            pv_max = '0.4'
            ignore_filters = False
            proteome = proteome_form.cleaned_data['proteome']
            full_length_only = proteome_form.cleaned_data['full_length_only']
            user_email = proteome_form.cleaned_data['user_email'] or ''
            job_name = proteome_form.cleaned_data['job_name'] or ''
        else:
            for key in proteome_form.errors:
                form_error = '{} {}\n'.format(key, proteome_form.errors[key])
    if form_error:
        form_error = re.sub(r'<.*?>', '', form_error)
        form_error = form_error.replace('This ', 'the ')
        form_error = form_error.replace('input_set', 'Input protein set:')
        form_error = form_error.replace('input_zip', 'Input protein set:')
        form_error = form_error.replace('input_motif', 'Input motif:')
        form_error = form_error.replace('regions', 'Regions file:')
        form_error = form_error.replace('gepf_infinity', 'Restrict gaps:')
        form_error = form_error.replace('rsa_check',
                                        'Surface accessibility check:')
        form_error = form_error.replace('pv_max', 'Maximal regex p-value:')
        form_error = form_error.replace('user_email', 'E-mail:')
        form_error = form_error.replace('job_name', 'Job name:')
        messages.info(request, form_error)
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'pv_max': str(PV_MAX),
                 'prots_max': str(PROTS_MAX),
                 'mode': mode,
                 'standard_form': standard_form,
                 'advanced_form': advanced_form,
                 'proteome_form': proteome_form}
        return render(request, 'hhmotif_app/index.html', dict_)
    try:
        submit_time  = str(datetime.datetime.utcnow())[: -7] + ' UTC'
        tag = generate_tag(user_email)
        args = pickle.dumps((mode, ortho_search, input_file, text_input,
                             regions_file, tag))
        queue_the_task('prioritized', args, tag)
        session = {}
        session['mode'] = mode
        session['ortho_search'] = ortho_search
        session['gepf'] = gepf
        session['rsa'] = rsa
        session['disorder_check'] = disorder_check
        session['homology_filter'] = homology_filter
        session['pv_max'] = pv_max
        session['ignore_filters'] = ignore_filters
        session['proteome'] = proteome
        session['full_length_only'] = full_length_only
        session['user_email'] = user_email
        session['job_name'] = job_name
        session['tag'] = tag
        session['submit_time'] = submit_time
        session['sent'] = False
        session_key = generate_tag('session_tag')
        start_session(session_key, pickle.dumps(session))
        Process(target = schedule_end_session, args = (session_key, 30)).start()
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'pv_max': str(PV_MAX),
                 'prots_max': str(PROTS_MAX),
                 'mode': mode,
                 'session_key': session_key,
                 'standard_form': standard_form,
                 'advanced_form': advanced_form,
                 'proteome_form': proteome_form}
        return render(request, 'hhmotif_app/index.html', dict_)
    except Exception as e:
        messages.info(request, str("Server internal error"))
        dict_ = {'tool': mark_safe(TOOL_NAME),
                 'pv_max': str(PV_MAX),
                 'prots_max': str(PROTS_MAX),
                 'mode': mode,
                 'standard_form': standard_form,
                 'advanced_form': advanced_form,
                 'proteome_form': proteome_form}
        return render(request, 'hhmotif_app/index.html', dict_)

def guide(request):
    dict_ = {'inner_link': request.GET.get('l', ''),
             'pv_max': str(PV_MAX),
             'prots_max': str(PROTS_MAX)}
    return render(request, 'hhmotif_app/guide.html', dict_)

def about(request):
    dict_ = {'tool': mark_safe(TOOL_NAME)}
    return render(request, 'hhmotif_app/about.html', dict_)

def tests(request):
    dict_ = {'tool': mark_safe(TOOL_NAME)}
    return render(request, 'hhmotif_app/tests.html', dict_)

def contact(request):
    return render(request, 'hhmotif_app/contact.html')

def output_not_found(request):
    dict_ = {'output': True}
    return render(request, 'hhmotif_app/not_found.html', dict_)

def file_not_found(request):
    return render(request, 'hhmotif_app/not_found.html')

def custom_404(request):
    return render(request, 'hhmotif_app/404.html')
