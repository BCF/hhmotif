import django_tables2 as tables
from hhmotif_app.models import Orthologs, Sequence, GOterms, Refseq_go

class OrthologTable(tables.Table):

        #for a checkbox that marks all others use  attrs, dont forget the javascript-method you can see in the template
        selection = tables.CheckBoxColumn(accessor='oid.qid', orderable=False, attrs = { "th__input": {"onclick": "toggle(this)"}} )

        oid = tables.Column(verbose_name="Ortholog ID", orderable=False)
        taxid = tables.Column(verbose_name="Taxonomy",orderable=False)
        e_value = tables.Column(verbose_name="E-Value",orderable=True)
        score = tables.Column(verbose_name="Score", orderable=True)
        descr = tables.Column(verbose_name="Description", orderable=True, accessor= 'oid.descr')
        confidence = tables.Column(verbose_name="Confidence", visible=False)

        class meta:
                model = Orthologs
                exclude = ('id', 'qid')
                attrs = {'class': 'table'}



class GoTable(tables.Table):
        
        refseq_id = tables.Column(verbose_name="RefSeq ID", orderable=True)
        go_id = tables.Column(verbose_name="GO ID", orderable=True, accessor='go_id.go_id')
        go_flag = tables.Column(verbose_name="GO Flag", orderable=True, accessor='go_id.go_flag')
        go_terms = tables.Column(verbose_name="GO Term", orderable=True, accessor='go_id.go_terms')


class GoAnnotationsTable(tables.Table):

        go_id = tables.Column(verbose_name="GO ID", orderable=True)
        go_flag = tables.Column(verbose_name="GO Flag", orderable=True)
        go_terms = tables.Column(verbose_name="GO Term", orderable=True)







