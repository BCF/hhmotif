from django.db import models
from django import forms
from datetime import datetime
from django.utils import timezone


class Species(models.Model):

    def __str__(self): # Python 3 -- identisch zu __unicode (Python 2)
        return str(self.get_fields())

    def __unicode__(self):
        return unicode(self.get_fields())

    taxid = models.CharField(max_length=20, primary_key=True, unique=True)
    tax = models.CharField(max_length=150)
    name = models.CharField(max_length=50)

    def get_fields(self):
        container={}
        for field in Species._meta.fields:
            container[field.name] = field.value_to_string(self)
        return container
    class Meta:
        verbose_name = "Species"
        verbose_name_plural = "Species"


class Sequence(models.Model):

#    def __unicode__(self):
#        return unicode(self.get_fields())

    qid = models.CharField(max_length=20, primary_key=True, unique=True)
    seq = models.TextField()
    descr = models.CharField(max_length=254)
    gi = models.CharField(max_length=12)
    taxid = models.ForeignKey('Species',on_delete=models.CASCADE)

#    def get_fields(self):
#        container={}
#        for field in Sequence._meta.fields:
#            container[field.name] = field.value_to_string(self)
#        return container
    class Meta:
        verbose_name = "Sequence"
        verbose_name_plural = "Sequences"


class Orthologs(models.Model):

    def __unicode__(self):
        return unicode(self.get_fields())

    #choices for confidence of orthologs
    CONFIDENCE_LEVELS = (
        ('U', 'unknown'), #default
        ('N', 'no confidence'), #ortholog with low/ no confidence
        ('C', 'confirmed'), #manually confirmed ortholog
    )

#    qid = models.CharField(max_length=20) #why not models.ForeignKey(Sequence)
#The related_name attribute specifies the name of the reverse relation
    qid = models.ForeignKey('Sequence', on_delete=models.CASCADE, related_name='protein_id')
    oid = models.ForeignKey('Sequence',on_delete=models.CASCADE, related_name='ortho_id')
    taxid = models.ForeignKey('Species',on_delete=models.CASCADE) #taxid of ortholog
    e_value = models.FloatField()
    score = models.CharField(max_length=10)
    confidence = models.CharField(max_length=1, choices=CONFIDENCE_LEVELS, default="")

    def get_fields(self):
        container={}
        for field in Orthologs._meta.fields:
            container[field.name] = field.value_to_string(self)
        return container
    class Meta:
        verbose_name = "Ortholog"
        verbose_name_plural = "Orthologs"


class Alignment(models.Model):

    def __unicode__(self):
        return unicode(self.get_fields())

    qid = models.CharField(max_length=20) #why not models.ForeignKey(Sequence)
#    qid = models.ForeignKey(Sequence, related_name='protein_id')
    oid = models.ForeignKey('Sequence',on_delete=models.CASCADE)
#    oid = models.ForeignKey(Sequence, related_name='ortho_id')
    hsp_count = models.IntegerField()
    length = models.IntegerField()
    score = models.CharField(max_length=25)
    e_value = models.FloatField()
    identities = models.IntegerField()
    positives = models.IntegerField()
    gaps = models.IntegerField()

    def get_fields(self):
        container={}
        for field in Alignment._meta.fields:
            container[field.name] = field.value_to_string(self)
        return container
    class Meta:
        verbose_name = "Alignment"
        verbose_name_plural = "Alignments"


class Hsp(models.Model): #high-scoring segment pairs

    def __unicode__(self):
        return unicode(self.get_fields())

    qid = models.CharField(max_length=20) #why not models.ForeignKey(Sequence)
    oid = models.CharField(max_length=20) #why not models.ForeignKey(Sequence)
#    qid = models.ForeignKey(Sequence, related_name='protein_id')
#    oid = models.ForeignKey(Sequence, related_name='ortho_id')
    aln_id = models.ForeignKey('Alignment',on_delete=models.CASCADE)#PK of Alignment
    hsp_nr = models.IntegerField()
    q_aln = models.TextField()
    o_aln = models.TextField()
    q_start = models.IntegerField()
    o_start = models.IntegerField()
    q_end = models.IntegerField()
    o_end = models.IntegerField()

    def get_fields(self):
        container={}
        for field in Hsp._meta.fields:
            container[field.name] = field.value_to_string(self)
        return container
    class Meta:
        verbose_name = "Hsp"
        verbose_name_plural = "Hsp"


class GOterms(models.Model):
    def __unicode__(self):
        return unicode(self.get_fields())

    #choices for go-flag
    GO_FLAGS = (
        ('F', 'F'), #molecular function
        ('P', 'P'), #biological process
        ('C', 'C'), #cellular component
    )
    go_id = models.CharField(max_length=20, primary_key=True, unique=True)
    go_terms = models.CharField(max_length=200)
    go_flag = models.CharField(max_length=1, choices=GO_FLAGS, default="")

    def get_fields(self):
        container={}
        for field in GOterms._meta.fields:
            container[field.name] = field.value_to_string(self)
        return container
    class Meta:
        verbose_name = "GO Term"
        verbose_name_plural = "GO Terms"


class Refseq_go(models.Model):
    def __unicode__(self):
        return unicode(self.get_fields())

    refseq_id = models.ForeignKey('Sequence',on_delete=models.CASCADE) # foreign key zu Sequence.qid (primary key)
    go_id = models.ForeignKey('GOterms',on_delete=models.CASCADE) # foreign key zu GOterms.go_id (primary key)

    def get_fields(self):
        container={}
        for field in Refseq_go._meta.fields:
            container[field.name] = field.value_to_string(self)
        return container
    class Meta:
        verbose_name = "GO Annotation"
        verbose_name_plural = "GO Annotations"


class Proteome(models.Model):
    def __unicode__(self):
        return unicode(self.get_fields())

    #choices for complete-flag
    STATE_FLAGS = (
        ('C', 'complete'), #all proteins of this species having orthologs are included in the DB
        ('N', 'not complete'),
        ('U', 'unknown'),
    )

    taxid = models.ForeignKey('Species',on_delete=models.CASCADE)
    state_flag = models.CharField(max_length=1, choices=STATE_FLAGS)
    last_modified = models.DateField(auto_now=True)

    def get_fields(self):
        container={}
        for field in Proteome._meta.fields:
            container[field.name] = field.value_to_string(self)
        return container
    class Meta:
        verbose_name = "Proteome"
        verbose_name_plural = "Proteomes"




# Create your models here.
