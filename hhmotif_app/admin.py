from django.contrib import admin
from hhmotif_app.models import Sequence, Orthologs, Species, Alignment, Hsp, GOterms, Refseq_go, Proteome

# methods
def mark_wrong(modeladmin, request, queryset):
    queryset.update(confidence='N')
mark_wrong.short_description = "Mark as wrong orthologs"

def mark_true(modeladmin, request, queryset):
    queryset.update(confidence='C')
mark_true.short_description = "Mark as true, confirmed orthologs"

def mark_unknown(modeladmin, request, queryset):
    queryset.update(confidence='U')
mark_unknown.short_description = "Mark as orthologs with unknown confidence"

####################################
# for foreign key fields (to picture only the selected fields, not the whole foreingn-key-object) 
def sequence_oid(object):
    return object.oid.qid
sequence_oid.admin_order_field  = 'oid'  #Allows column order sorting
sequence_oid.short_description = 'Ortholog ID'  #Renames column head

def sequence_qid(object):
    return object.qid.qid
sequence_qid.admin_order_field  = 'qid'
sequence_qid.short_description = 'Sequence ID'

def sequence_id(object):
    return object.refseq_id.qid
sequence_id.admin_order_field  = 'refseq_id'

def go_id(object):
    return object.go_id.go_id
go_id.admin_order_field  = 'go_id'

def go_term(object):
    return object.go_id.go_terms
go_term.admin_order_field  = 'go_id__go_terms'

def go_flag(object):
    return object.go_id.go_flag
go_flag.admin_order_field  = 'go_id__go_flag'

def tax_id(object):
    return object.taxid.taxid
tax_id.admin_order_field  = 'taxid__taxid'

def tax_name(object):
    return object.taxid.name
tax_name.admin_order_field  = 'taxid__name'

def ortho_tax_id(object):
    return object.taxid.taxid
ortho_tax_id.admin_order_field  = 'taxid__taxid'
ortho_tax_id.short_description = 'Ortholog Tax ID'

def ortho_tax_name(object):
    return object.taxid.name
ortho_tax_name.admin_order_field  = 'taxid__name'
ortho_tax_name.short_description = 'Ortholog Tax Name'

def tax_tax(object):
    return object.taxid.tax
tax_tax.admin_order_field  = 'taxid__tax'
tax_tax.short_description = 'Tax'

def alignment_id(object):
    return object.aln_id.id
alignment_id.admin_order_field  = 'aln_id__id'
alignment_id.short_description = 'Alignment ID'

def alignment_features(object):
    hsp = str(object.aln_id.hsp_count)
    l = str(object.aln_id.length)
    s = object.aln_id.score
    e = str(object.aln_id.e_value)
    i = str(object.aln_id.identities)
    p = str(object.aln_id.positives)
    g = str(object.aln_id.gaps)
    features = 'HSP count:'+hsp+ '; lenght:'+l+ '; score:'+s+ '; e-value:'+e+ '; identities:'+i+ '; positives:'+p+ '; gaps:'+g
    return features
alignment_features.short_description = 'Alignment Features'
###################################

class SequenceAdmin(admin.ModelAdmin):
    list_display =('qid', 'descr','gi',tax_id, tax_tax, tax_name, 'seq')
    search_fields = ('qid','descr', 'taxid__taxid', 'taxid__tax', 'taxid__name')
    raw_id_fields = ('taxid',)

class OrthologsAdmin(admin.ModelAdmin):
    list_display = ('id', sequence_qid,sequence_oid,'e_value','score','confidence', ortho_tax_id, ortho_tax_name )
    actions = [mark_wrong, mark_true, mark_unknown]
    list_filter = ('confidence',)
    search_fields = ('qid__qid','oid__qid')
    raw_id_fields = ('qid','oid', 'taxid')

class SpeciesAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Species._meta.fields]
    search_fields = ('taxid','tax','name')

class AlignmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'qid',sequence_oid,'hsp_count','length','score','e_value','identities', 'positives','gaps' )
    raw_id_fields = ('oid',)
#when qid is a foreign-key field use the following instead:
#    list_display = ('id', sequence_qid,sequence_oid,'hsp_count','length','score','e_value','identities', 'positives','gaps' )
#    raw_id_fields = ('qid','oid')
    search_fields = ('qid','oid')    

class HspAdmin(admin.ModelAdmin):
    list_display = ('id', 'qid','oid',alignment_id, alignment_features, 'hsp_nr','q_aln','o_aln','q_start','o_start', 'q_end','o_end' )
    raw_id_fields = ('aln_id',)
#when qid and oid are foreign-key fields use the following instead:
#    list_display = ('id', sequence_qid,sequence_oid,alignment_id, alignment_features, 'hsp_nr','q_aln','o_aln','q_start','o_start', 'q_end','o_end' )
#    raw_id_fields = ('qid','oid','aln_id')
    search_fields = ('qid','oid')

class GoTermsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in GOterms._meta.fields]
    search_fields = ('go_id','go_terms')
    list_filter = ('go_flag',)

class RefseqGoAnnotationAdmin(admin.ModelAdmin):
    list_display = ('id',sequence_id,go_id,  go_term, go_flag )
    search_fields = ('refseq_id__qid','go_id__go_id','go_id__go_terms')
    raw_id_fields = ('refseq_id', 'go_id')
    list_filter = ('go_id__go_flag',)

class ProteomeAdmin(admin.ModelAdmin):
    list_display = ('id', tax_id, tax_tax, tax_name, 'state_flag', 'last_modified' )
    search_fields = ('taxid',)
    date_hierarchy = 'last_modified'
    raw_id_fields = ('taxid',)


# Register your models here.
admin.site.register(Sequence, SequenceAdmin)
admin.site.register(Orthologs, OrthologsAdmin)
admin.site.register(Species, SpeciesAdmin)
admin.site.register(Alignment, AlignmentAdmin)
admin.site.register(Hsp, HspAdmin)
admin.site.register(GOterms, GoTermsAdmin)
admin.site.register(Refseq_go, RefseqGoAnnotationAdmin)
admin.site.register(Proteome, ProteomeAdmin)



