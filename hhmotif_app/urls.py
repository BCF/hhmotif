from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from hhmotif_app import views

app_name = 'hhmotif_app'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^search/$', views.search, name='search'),
    url(r'^guide/$', views.guide, name='guide'),
    url(r'^about/$', views.about, name='about'),
    url(r'^tests/$', views.tests, name='tests'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^file_not_found/$', views.file_not_found, name='file_not_found'),
    url(r'^output_not_found/$', views.output_not_found, name='output_not_found')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = 'custom_404'
