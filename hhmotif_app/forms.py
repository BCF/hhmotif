from django import forms
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from hhmotif_site.config import PV_MAX

# Custom validators
def custom_validate_email(email):
    """Function for additional e-mail validation"""
    #if not email.endswith('biochem.mpg.de'):
        #raise ValidationError('Only internal MPIB e-mails are allowed')
        
def custom_validate_file(file_):
    """Function for additional uploaded file validation"""
    if file_.size > 1 * 1024 * 1024:
        raise ValidationError(\
        'The uploaded file is too large. Max. allowed size is 1 MB')
        ##
        
def custom_validate_zip_file(file_):
    """Function for additional uploaded ZIP file validation"""
    if file_.size > 5 * 1024 * 1024:
        raise ValidationError(\
        'The uploaded file is too large. Max. allowed size is 5 MB')
        ##
        
def custom_validate_zip_file(file_):
    """Function for additional uploaded ZIP file validation"""
    if file_.size > 5 * 1024 * 1024:
        raise ValidationError(\
        'The uploaded file is too large. Max. allowed size is 5 MB')
        ##
        
def custom_validator_job_name(job_name):
    """Function for additional job name validation"""
    if '"' in job_name:
        raise ValidationError('Double quotes are not allowed in job names')

class CookiesForm(forms.Form):
    form_type = forms.CharField(initial = 'cookies_form')

class StandardForm(forms.Form):
    form_type = forms.CharField(initial = 'standard_form')
    input_text = forms.CharField(widget = forms.Textarea, required = False)
    input_set = forms.FileField(validators = [custom_validate_file],
                                required = False)
    user_email = forms.EmailField(validators = [validate_email,\
    custom_validate_email], required = False)
    ##
    job_name = forms.CharField(max_length = 50, required = False)

class AdvancedForm(forms.Form):
    form_type = forms.CharField(initial = 'advanced_form')
    input_zip = forms.FileField(validators = [custom_validate_zip_file])
    query_regions = forms.FileField(validators = [custom_validate_file],
                                    required = False)
    ortho_search = forms.BooleanField(initial = False, required = False)
    gepf_infinity = forms.BooleanField(initial = True, required = False)
    rsa_check = forms.BooleanField(initial = True, required = False)
    disorder_check = forms.BooleanField(initial = False, required = False)
    homology_filter = forms.BooleanField(initial = True, required = False)
    pv_max = forms.DecimalField(localize = True, min_value = 0.01,\
    max_value = 1.0, decimal_places = 2, initial = PV_MAX)
    ##
    ignore_filters = forms.BooleanField(initial = False, required = False)
    user_email = forms.EmailField(validators = [validate_email,\
    custom_validate_email], required = False)
    ##
    job_name = forms.CharField(max_length = 50, required = False)

class ProteomeForm(forms.Form):
    proteome_choices = (('H_sapiens', 'H. sapiens'), ('S_scrofa', 'S. scrofa'),\
    ('R_norvegicus', 'R. norvegicus'), ('M_musculus', 'M. musculus'),\
    ('X_tropicalis', 'X. tropicalis'), ('D_rerio', 'D. rerio'),\
    ('D_melanogaster', 'D. melanogaster'), ('C_elegans', 'C. elegans'),\
    ('S_cerevisiae', 'S. cerevisiae'), ('S_pombe', 'S. pombe'))
    ##
    form_type = forms.CharField(initial = 'proteome_form')
    input_motif = forms.FileField(validators = [custom_validate_file],
                                  required = False)
    proteome = forms.ChoiceField(choices = proteome_choices)
    full_length_only = forms.BooleanField(initial = True, required = False)
    user_email = forms.EmailField(validators = [validate_email,\
    custom_validate_email], required = False)
    ##
    job_name = forms.CharField(max_length = 50, required = False)
