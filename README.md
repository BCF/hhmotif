# HH-MOTiF
* Authors: Roman Prytuliak (web-server), Adrien Bonnard (stand-alone version)
* Current maintainers: Roman Prytuliak, Michael Volkmer, Bianca Habermann


HHMOTiF is a software performing de novo detection of short linear motifs in proteins by
Hidden Markov Model comparisons.

HHMOTiF has been published and can be cited as : 
HH-MOTiF, R. Prytuliak, M. Volkmer, M. Meier, B. Habermann, in 
`Nucleic Acids Research, Volume 45, Issue W1, 3 July 2017, Pages W470–W477', [DOI](https://doi.org/10.1093/nar/gkx341)

# Authors & contributions
Roman Prytuliak (main author), Adrien Bonnard (created stand-alone version of hh-motif), Michael Volkmer (helped in code implementation and supervision), 
Edlira Nano (helped in code transfer and supervision of implementation of the stand-alone version), Bianca Habermann (conceived and supervised project)

# Funding
RP and MV were paid by the Max Planck Society, Adrien Bonnard was financed by Aix-Marseille Univ, CNRS, IBDM UMR 7288
This work was partially funded by the IFB project [T5](https://www.france-bioinformatique.fr/fr/projets2015/t5-project)
The continuation of this work is possible due to funding from CNRS.

