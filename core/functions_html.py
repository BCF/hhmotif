import os, re, subprocess, datetime
from functions import u, error, parse_input_fasta_to_dict

class EmptyRegion():
    """Class to handle absense of found regions for some proteins"""
    begin = -1
    end = -1

def html_escape(string_):
    """Function to escape a string to be used in HTML"""
    return string_.replace('&', '&amp;').replace('>', '&gt;').replace('<',\
    '&lt;').replace('"', '&qupt;').replace("'", '&apos;')
    ##

def read_regions_file_basic(regions_file, input_fname):
    """Function for getting boundaries of the regions of interest for a \
particular protein"""
    ##
    if not regions_file:
        return []
    with open(regions_file, 'r') as ifile:
        lines = ifile.readlines() 
    found = False
    line_begin = input_fname + ':'
    for line in lines:
        if line.startswith(line_begin):
            ranges = line.strip().split(':')[1].split(',')
            for idx, range_ in enumerate(ranges):
                ranges[idx] = [int(x) for x in range_.split('-')]
            return ranges
    if not found:
        return []

def write_results_to_html(html_dir, regions_file, input_files, g_prots_listed,
                          region_list, html_header, ignore_filters,
                          input_labels = None, job_name = ''):
    """Function to write output in an HTML document"""
    def set_region():
        """Closure to get the next accepted region for the current protein"""
        nonlocal region_idx, region, regions_count, filters_ignored
        while region_idx < regions_n:
            region_ = region_list[prot][region_idx]
            region_idx += 1
            if region_.accept and hasattr(region_, 'info'):
                region = region_
                regions_count += 1
                if str(region.pv) == 'nan':
                    filters_ignored = True
                break
            else:
                continue
    html_template =\
"""<!DOCTYPE html>
<html lang="en">
<head>
<title>Motif prediction results</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0,
maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet"
href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="results.min.css" />
<script
src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js">
</script>
<script>
var descr = [];
{descr_block}
</script>
<script type="text/javascript" src="results.min.js"></script>
</head>

<body>
<div id="logo_wrapper"><img id="logo" src="logo.svg" /></div>
<div style="height: 1em"></div>
<h2 id="title_text">Results of the <i>de novo</i> motif search</h2>
{job_name}
<a id="guide_href" href="" target="_blank">Read the guide</a>
&nbsp;|&nbsp;
<a id="search_href" href="" target="_blank">Launch a new motif search</a>
<br /><br />

{overview_block}<br />

<div class="width-limit">
<div id="left_panel">
<div id="sequences">{seq_block}</div>
<p>
Plain text summary: <a href="results.txt" download target="_blank">download</a>
<br /><br />
<span class="section_header">Submission details:</span>
<br />{header_block}<br />Completed: {curtime}
<br />This page will be deleted from the server after 7 days!
</p>
<br />
</div>

<div id='info_box'></div>
</div>

</body>
</html>"""
    ##
    overview_reg_template =\
"""<br />{prot:0>4}<span class="overview-span">
&nbsp;&nbsp;{begin} <a class='overview' href='.{id_}'>{seq}</a> {end}
</span>
"""
    ##
    descr_reg_template =\
"""descr["{id_}"]="<img src='region{id_}.svg' alt='seqlogo' /><br />"+
"<pre>{pseudo_msa}</pre>"+
"<a href='region{id_}.fasta' download target='_blank'>"+
"Download the motif as FASTA</a>";
"""
    ##
    overview_template =\
"""{motif_tree_count}<br /><br />
<span class="section_header">Found motif trees:</span>
<br />(click to see more information)
<div id="overviews" class="width-limit">
<u>Prot.</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Motif root</u>
{overview}</div>
"""
    ##
    only_filters_ignored =\
"""The initial motif search yielded no results.
The search was retried with relaxed parameters and some filters ignored.
Only the <u>best</u> suboptimal motif tree detected is shown.<br />
Note that regex p-value cannot be computed in this mode.
"""
    ##
    no_results_filters =\
"""The motif search yielded no results.<br />
You may try to repeat the search with relaxed parameters or activate the option
'Show best suboptimal if no motifs found' in advanced mode.
"""
    ##
    no_results_filters_ignored =\
"""The motif search yielded no results even with relaxed settings.<br />
This could have been caused by one of the following reasons:
<ul>
    <li>There are not enough sequence similarity even for weak motifs</li>
    <li>The submitted proteins are very close homologs</li>
</ul>
"""
    ##
    output_html = html_dir + 'results.html'
    prots = len(g_prots_listed)
    seq_html = ''
    overview = ''
    descr = ''
    cpv = re.compile(' \(\d\.\d{3}\)')
    regions_count = 0
    filters_ignored = False
    for prot in range(prots):
        fname = input_files[prot].split('/')[-1].rsplit('.fasta', 1)[0]
        interest_regions = read_regions_file_basic(regions_file, fname)
        seq_html += '<p><div class="fasta_header">'
        if input_labels:
            label = html_escape(input_labels[prot][1: ])
            seq_html += '{:0>4} ({}):</div>\n'.format(prot, label)
        else:
            seq_html += '{:0>4} ({}):</div>\n'.format(prot, fname)
        seq = u(g_prots_listed[prot]['seq'])
        regions_n = len(region_list[prot])
        ir_n = len(interest_regions)
        region = EmptyRegion()
        region_idx = 0
        set_region()
        region_attr = ''
        ir_idx = 0
        out_of_interest = True if ir_n else False
        for idx, aa in enumerate(seq):
            if idx % 80 == 0:
                line_end = ''
                if idx:
                    if region_attr:
                        line_end += '</u>'
                    if out_of_interest:
                        line_end += '</span>'
                    line_end += ' {}</span>\n<br />'.format(idx)
                span = '{}<span class="l{:0>4}_{:0>3}">'.format(line_end,\
                prot, idx // 80)
                ##
                seq_html += span
                if out_of_interest and idx:
                    seq_html += '<span class="grayed">'
                if region_attr:
                    seq_html += '<u {}>'.format(region_attr)
            if (ir_idx < ir_n) and (idx == interest_regions[ir_idx][1]):
                seq_html += '<span class="grayed">'
                out_of_interest = True
                ir_idx += 1
            if idx + 1 == region.begin:
                region_id = 'm{:0>4}_{:0>2}'.format(prot, region_idx - 1)
                region_attr = 'class="motif {}"'.format(region_id)
                seq_html += '<u {}>'.format(region_attr)
                pseudo_msa = region.info.replace('\n', ' \\n"+\n"')
                pseudo_msa = cpv.sub('', pseudo_msa)
                overview += overview_reg_template.format(prot = prot, begin =\
                region.begin, id_ = region_id, seq = region.seq, end =\
                region.end)
                ##
                descr += descr_reg_template.format(id_ = region_id[1: ],
                                                   pseudo_msa = pseudo_msa)
            elif idx == region.end:
                seq_html += '</u>'
                region_attr = ''
                set_region()
            if (ir_idx < ir_n) and (idx + 1 == interest_regions[ir_idx][0]):
                if idx:
                    seq_html += '</span>'
                out_of_interest = False
            seq_html += aa
        if region_attr:
            seq_html += '</u>'
        if out_of_interest:
            seq_html += '</span>'
        seq_html += ' {}</span>'.format(idx + 1)
        seq_html += '</p>\n\n'

    if regions_count:
        if filters_ignored:
            motif_tree_count = only_filters_ignored
        else:
            motif_tree_count = 'Number of found motif trees: ' +\
            str(regions_count)
            ##
        overview_block = overview_template.format(motif_tree_count =\
        motif_tree_count, overview = overview)
        ##
    else:
        if ignore_filters:
            overview_block = no_results_filters_ignored
        else:
            overview_block = no_results_filters
    curtime = str(datetime.datetime.utcnow())[: -7] + ' UTC'
    if job_name:
        job_name_line = '<b>Job name:</b> {}\n<br /><br />\n'.format(\
        html_escape(job_name))
        ##
    else:
        job_name_line = ''
    html_template = html_template.format(header_block = html_header,\
    job_name = job_name_line, curtime = curtime, descr_block = descr,\
    overview_block = overview_block, seq_block = seq_html)
    ##
    with open(output_html, 'w') as ofile:
        ofile.write(html_template)
        
def make_logos(output_dir, weblogo):
    """Function for generation of sequence logos for found regions"""
    cfname = re.compile('^region\d{4}_\d{2}\.fasta$')
    for fname in os.listdir(output_dir):
        if not cfname.search(fname):
            continue
        fpath = output_dir + fname
        fpath_ = output_dir + fname[: -5] + 'svg'
        with open(os.devnull, 'wb+') as devnull:
            check = subprocess.Popen([weblogo, '-f', fpath, '-o', fpath_,\
            '-A', 'protein', '-F', 'svg', '--errorbars', 'no'], stdout =\
            devnull, stderr = subprocess.PIPE)
            ##
        stdout, stderr = check.communicate()
        if stderr:
            error(u(stderr), ' in weblogo')

def hhr_to_html(html_dir, input_hhr, proteome_fasta, html_header, job_name,
                template_cons, full_length_only, score_min):
    """Function to convert HHSearch output with a full proteome as templates \
to an HTML file"""
    ##
    html_template =\
"""<!DOCTYPE html>
<html lang="en">
<head>
<title>Motif prediction results</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0,
maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet"
href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="results.min.css" />
<script
src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js">
</script>
<script>
var descr = [];
{descr_block}
</script>
<script src="results.min.js"></script>
</head>

<body>
<div id="logo_wrapper"><img id="logo" src="logo.svg" /></div>
<div style="height: 1em"></div>
<h2 id="title_text">Results of the proteome-wide motif search</h2>
{job_name}
<a id="guide_href" href="" target="_blank">Read the guide</a>
&nbsp;|&nbsp;
<a id="search_href" href="" target="_blank">Launch a new motif search</a>
<br /><br />

<p style="clear: both">
Input template motif:
<a href="input_motif.fasta" download target="_blank">download</a>
<br />
Template motif consensus: <span id="cons">{template_cons}</span>

<div class="width-limit">
<div id="left_panel">
<div id="sequences">{align_block}</div>

<br />
Plain text summary: <a href="results.txt" download target="_blank">download</a>
<br /><br />

<p><span class="section_header">Further submission details:</span>
<br />{header_block}<br />Completed: {curtime}
<br />This page will be deleted from the server after 7 days!
</p>
</div>

<div id='info_box'></div>
</div>

</body>
</html>"""
    ##
    descr_hit_template =\
"""descr["{code}"]="<pre>{header} \\n"+
"{hit_begin: >{len_}} {hit_seq} {hit_end} \\n"+
"{spacer}{align_signs} \\n"+
"{templ_begin: >{len_}} {templ_seq} {templ_end} \\n"+
">consensus </pre>"+
"{score_str}";\n"""
    ##
    text_output_hit_template =\
"""{header}
{hit_begin: >{len_}} {hit_seq} {hit_end}
{spacer}{align_signs}
{templ_begin: >{len_}} {templ_seq} {templ_end}
>consensus
{score_str}"""
    ##
    output_html = html_dir + 'results.html'
    output_txt = html_dir + 'results.txt'
    records_dict = parse_input_fasta_to_dict(proteome_fasta)
    seq_html = ''
    descr = ''
    text_output = 'Job name: "{}"\n'.format(job_name) if job_name else ''
    text_output += '+{}+\n'.format('-' * 78)
    text_output += '| Input motif consensus:{}|\n'.format(' ' * 55)
    text_output += '+{}+\n'.format('-' * 78)
    text_output += template_cons + '\n'
    text_output += '+{}+\n'.format('-' * 78)
    text_output += '| Input parameters:{}|\n'.format(' ' * 60)
    text_output += '+{}+\n'.format('-' * 78)
    c = re.compile(r'Job.*$', re.DOTALL)
    text_output += c.sub('', html_header.replace('<br />', ''))
    text_output += '+{}+\n'.format('-' * 78)
    text_output += '| Found motif matches:{}|\n'.format(' ' * 57)
    text_output += '+{}+\n'.format('-' * 78)
    with open(input_hhr, 'r') as ifile:
        lines = ifile.readlines()
    count = 0
    for idx in range(len(lines)):
        if not lines[idx].startswith('>'):
            continue
        fields = lines[idx + 7].split()
        hit_begin = int(fields[2])
        hit_end = int(fields[4])
        if full_length_only and (hit_end - hit_begin < len(template_cons) - 1):
            continue
        score_str = lines[idx + 1].split()[2]
        score = float(score_str.split('=')[1])
        if score < score_min:
            continue
        header = lines[idx].strip()
        header_short = header.split()[0]
        seq = records_dict[header]
        seq_html += '<p><div class="fasta_header">{}</div>\n'.format(header)
        hit_seq = fields[3]
        align_signs = lines[idx + 5].strip()
        fields = lines[idx + 4].split()
        templ_begin = int(fields[2])
        templ_seq = fields[3]
        templ_end = int(fields[4])
        len_ = max(len(str(hit_begin)), len(str(templ_begin)))
        spacer = ' ' * (len_ + 1)
        if count > 0:
            text_output += '\n' + '*' * 30 + '\n'
        text_output += text_output_hit_template.format(header = header, len_ =\
        len_, hit_begin = hit_begin, hit_seq = hit_seq, hit_end = hit_end,\
        spacer = spacer, align_signs = align_signs, templ_begin = templ_begin,\
        templ_seq = templ_seq, templ_end = templ_end, score_str = score_str)
        ##
        code = 'hit_' + str(count)
        in_region = False
        for idx, aa in enumerate(seq):
            if idx % 80 == 0:
                line_end = ' {}</span>\n<br />'.format(idx) if idx else ''
                span = '{}<span>'.format(line_end)
                ##
                if in_region:
                    seq_html += '</u>'
                seq_html += span
                if in_region:
                    seq_html += '<u class="motif {}">'.format(code)
            if idx + 1 == hit_begin:
                in_region = True
                seq_html += '<u class="motif {}">'.format(code)
                descr += descr_hit_template.format(code = code, header =\
                header_short, len_ = len_, hit_begin = hit_begin, hit_seq =\
                hit_seq, hit_end = hit_end, spacer = spacer, align_signs =\
                align_signs, templ_begin = templ_begin, templ_seq = templ_seq,\
                templ_end = templ_end, score_str = score_str)
                ##
            elif idx == hit_end:
                seq_html += '</u>'
                in_region = False
            seq_html += aa
        if in_region:
            seq_html += '</u>'
        seq_html += ' {}</span>'.format(idx + 1)
        seq_html += '</p>\n\n'
        count += 1
    if count == 0:
        text_output += '(No results for this motif search)'
        seq_html = '<p>(No results for this motif search)</p>'
    curtime = str(datetime.datetime.utcnow())[: -7] + ' UTC'
    if job_name:
        job_name_line = '<b>Job name:</b> {}\n<br /><br />\n'.format(\
        html_escape(job_name))
        ##
    else:
        job_name_line = ''
    html_template = html_template.format(template_cons = template_cons,\
    job_name = job_name_line, descr_block = descr, header_block =\
    html_header, curtime = curtime, align_block = seq_html)
    ##
    with open(output_txt, 'w') as ofile:
        ofile.write(text_output)
    with open(output_html, 'w') as ofile:
        ofile.write(html_template)