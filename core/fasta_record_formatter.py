#!/usr/local/bin/python3

#Warning: this script works only with strictly specified file names!

#Importing
import sys, os, re
from optparse import OptionParser

#Initialization parameters
path = os.getcwd() + '/'

#Other global variables
cheader = re.compile('^>')
cseq = re.compile('^[ABCDEFGHIKLMNPQRSTVWYUX]+\*?$')
cblank = re.compile('^[\ \t]*$')

#Processing command line arguments
usage = '%prog [options] -i INPUT_FILE -o OUTPUT_FILE -l LINE_LENGTH'
parser = OptionParser(usage = usage)
parser.add_option('-i', metavar = "INPUT_FILE", dest = 'input', help =\
'Input FASTA file')
##
parser.add_option('-o', metavar = 'OUTPUT_FILE', dest = 'output', default =\
'', help = 'Output FASTA file')
##
parser.add_option('-s', dest = 'split_recs_dir', default = '', help =\
'Split records to separate files in the output folder')
##
parser.add_option('-l', metavar = 'LINE_LENGTH', dest = 'lnlen',\
type = 'int', default = 60, help =\
'Length of a single line in sequences. 0: whole sequences')
##
parser.add_option('-c', metavar = 'HEADER_LENGTH', dest = 'hlen',\
type = 'int', default = 0, help = 'Max. length of headers. 0: unlimited')
##
parser.add_option('-n', dest = 'print_res', action = 'store_true', default =\
False, help = 'Print residue numbers at the line ends')
##
if __name__ == "__main__":
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()
    (opt, args) = parser.parse_args()
    opt.input = os.path.abspath(opt.input)
    if (not os.path.isfile(opt.input)):
        sys.stderr.write('Error: Input file does not exist\n')
        sys.exit(1)
    if opt.output:
        opt.output = os.path.abspath(opt.output)
    if opt.split_recs_dir:
        if not os.path.exists(opt.split_recs_dir):
            os.makedirs(opt.split_recs_dir)
        elif os.listdir(opt.split_recs_dir):
            sys.stderr.write('Error: Splitted files directory is not empty\n')
            sys.exit(1)
        opt.split_recs_dir = os.path.abspath(opt.split_recs_dir) + '/'
    if (not opt.output) and (not opt.split_recs_dir):
        sys.stderr.write('Error: No output specified')
        sys.exit(1)
    if ((opt.lnlen < 10) and (opt.lnlen != 0)):
        sys.stderr.write('Error: Line length cannot be lower than 10\n')
    if ((opt.hlen < 10) and (opt.hlen != 0)):
        sys.stderr.write('Error: Header length cannot be lower than 10\n')

#---Defining functions---
def parse_input_fasta(input_file):
    """Function for parsing and placing into list an input FASTA file"""
    records = []
    first = True
    expect_seq = False
    error = False
    header = ''
    seq = ''
    with open(input_file, 'r') as file:
        for line in file:
            if cheader.search(line):
                if expect_seq:
                    first = True
                if first:
                    first = False
                else:
                    if seq.strip():
                        records.append([header, seq.replace('*', '')])
                header = line[: -1]
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                seq += line[: -1]
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                error = True
                break
        if seq.strip():
            records.append([header, seq.replace('*', '')])
        if error:
            sys.stderr.write('Error: input file {} is not valid\n'.\
            format(input_file))
            ##
            sys.exit(1)
    return records

def write_record(record, ofile, lnlen, hlen, print_res):
    """Function for writing into specified file object a FASTA record"""
    if hlen > 0:
        ofile.write(record[0][: hlen])
    else:
        ofile.write(record[0])
    ofile.write('\n')
    rlen = len(record[1])
    if lnlen > 0:
        begin = 0
        while begin < rlen:
            ofile.write(record[1][begin: begin + lnlen])
            if print_res:
                ofile.write(' {}\n'.format(min(begin + lnlen, rlen)))
            else:
                ofile.write('\n')
            begin += lnlen
    else:
        ofile.write(record[1])
        if print_res:
            ofile.write(' {}\n'.format(rlen))
        else:
            ofile.write('\n')

def write_records_to_file(records, output_file, lnlen, hlen, print_res,\
split_recs_dir):
##
    """Function for writing to file FASTA records with defined line length"""
    if split_recs_dir:
        len_ = len(str(len(records)))
        count = 0
        for record in records:
            fname = '{}{:0{}}.fasta'.format(split_recs_dir, count, len_)
            hhmname = '{}{:0{}}.hhm'.format(split_recs_dir, count, len_)
            with open(fname, 'w') as ofile:
                write_record(record, ofile, lnlen, hlen, print_res)
            count += 1
    else:
        with open(output_file, 'w') as ofile:
            for record in records:
                write_record(record, ofile, lnlen, hlen, print_res)

#---Main section---
records = parse_input_fasta(opt.input)
write_records_to_file(records, opt.output, opt.lnlen, opt.hlen,\
opt.print_res, opt.split_recs_dir)
##
