import sys, os, subprocess, argparse
from functions import error, u, available_cpu_count

#Processing command line arguments
usage = "Usage: %prog -i INPUT_DIR -o OUTPUT_DIR"
parser = argparse.ArgumentParser(usage = usage)
parser.add_argument('-i', dest = 'input_dir', required = True, help =\
'Input directory (single FASTA files). May be empty if the file with IDs is \
provided')
##
parser.add_argument('-i1', dest = 'input_file', default = '',
help = 'Input file with IDs. Will overwrite existing input FASTA files')
##
parser.add_argument('-o', dest = 'output_dir',
                    help = 'Output directory (orthologs FASTA files)')
parser.add_argument('-d0', dest = 'database_ids', default = 'refseq_protein',
                    help = 'Database to resolve IDs from the file')
parser.add_argument('-d1', dest = 'database', default = 'refseq_protein',
                    help = 'Database to search in')  
parser.add_argument('-a', dest = 'auto_db', action = 'store_true',
                    default = False, help = 'Auto db and gi list')
parser.add_argument('-t', dest = 'taxonomy_dir', default = '../taxonomy/',
                    help = 'Directory with taxonomy-related files')
parser.add_argument('-l', type = float, dest = 'coverage', default = 0,\
help = 'Mininmal coverage of input sequence by alignment, [0, 1]')
##
parser.add_argument('-q', type = int, dest = 'id_min', default = 0,\
help = 'Mininmal % identity in the alignment, [0, 100]')
##
parser.add_argument('-p', dest = 'precalc_db_dir', default = '',
                    help = 'Directory with precalculated orthologs database')
if __name__ == "__main__":
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    opt = parser.parse_args()
    opt.input_dir = os.path.abspath(opt.input_dir) + '/'
    if not os.path.isdir(opt.input_dir):
        error('Input directory does not exist')
    opt.input_files = []
    for file in os.listdir(opt.input_dir):
        filepath = opt.input_dir + file
        if os.path.isdir(filepath):
            error('Input directory contains subdirectories')
        if file.endswith('.fasta'):
            opt.input_files.append(filepath)
    if (len(opt.input_files) == 0) and (not opt.input_file):
        error('No input is provided')
    if opt.input_file:
        opt.input_file = os.path.abspath(opt.input_file)
    if not opt.output_dir:
        error('Output directory is not specified')
    opt.output_dir = os.path.abspath(opt.output_dir) + '/'
    if not os.path.exists(opt.output_dir):
        os.makedirs(opt.output_dir)
    opt.taxonomy_dir = os.path.abspath(opt.taxonomy_dir) + '/'
    if not os.path.isdir(opt.taxonomy_dir):
        error('Taxonomy directory does not exist')
    if opt.input_file and (not opt.database_ids):
        error()
    if (opt.coverage > 1.0) or (opt.coverage < 0.0):
        error('Minimal coverage must be in range [0, 1]')
    if (opt.id_min > 100) or (opt.id_min < 0):
        error('Minimal % identity must be in range [0, 100]')
    if opt.precalc_db_dir:
        if not os.path.isdir(opt.precalc_db_dir):
            error('Precalculated orthologs directory does not exist')
        opt.precalc_db_dir = os.path.abspath(opt.precalc_db_dir) + '/'

#---Main section---
os.chdir(os.path.dirname(os.path.abspath(__file__)))
if opt.input_file:
    ids = []
    with open(opt.input_file, 'r') as ifile:
        for line in ifile:
            line = line.strip()
            if line:
                ids.append(line)
    for id_ in ids:
        with open(os.devnull, 'wb') as devnull:
            fasta_fname = '{}{}.fasta'.format(opt.input_dir, id_)
            check = subprocess.Popen(['blastdbcmd', '-db', opt.database_ids,\
            '-entry', id_, '-outfmt', '%f', '-out', fasta_fname], stdout =\
            devnull, stderr = subprocess.PIPE)
            ##
            stdout, stderr = check.communicate()
            if stderr:
                sys.stderr.write(u(stderr) + '\n')
                continue
            check = subprocess.Popen(['python3', 'fasta_record_formatter.py',\
            '-i', fasta_fname, '-o', fasta_fname, '-c', '1000'], stdout =\
            devnull, stderr = subprocess.PIPE)
            ##
            opt.input_files.append(fasta_fname)
opt.input_files.sort()
for idx, fname in enumerate(opt.input_files):
    name = fname.rsplit('/', 1)[-1].rsplit('.', 1)[0]
    ortho = '{}{}_orthologs.fasta'.format(opt.output_dir, name)
    files = len(opt.input_files)
    print('Processing {} ({} of {})...'.format(name, idx + 1, files),
          end = '')

    print("debug -1")
    sys.stdout.flush()
    if os.path.isfile(ortho):
        print(' Already exists!')
        continue
    
    with open(os.devnull, 'wb') as devnull:

        print("debug 0")
        cpus = available_cpu_count() // 2
        print("debug 1")
        check = subprocess.Popen(['python3', 'ortholog_searcher_3.py', '-i',\
        fname, '-o', ortho, '-c', str(cpus), '-d', opt.database, '-l',\
        str(opt.coverage), '-t', opt.taxonomy_dir, '-q', str(opt.id_min),\
        '-p', opt.precalc_db_dir] + ['-a'] * opt.auto_db, stdout = devnull,\
        stderr = subprocess.PIPE)
        ##
        print(' '.join(['python3', 'ortholog_searcher_3.py', '-i',\
        fname, '-o', ortho, '-c', str(cpus), '-d', opt.database, '-l',\
        str(opt.coverage), '-t', opt.taxonomy_dir, '-q', str(opt.id_min),\
        '-p', opt.precalc_db_dir] + ['-a'] * opt.auto_db))##/
        ##

        sys.stdout.flush()
        stdout, stderr = check.communicate()
        print("debug 2")
        sys.stdout.flush()
        if stderr:
            sys.stderr.write(u(stderr) + '\n')##/
            continue##/
            #error(u(stderr))
    print(' Done')

