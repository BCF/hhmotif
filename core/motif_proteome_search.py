import os, sys, argparse, subprocess, tempfile, shutil, linecache
from functions import u, error, check_binaries
from functions_html import hhr_to_html

#Processing command line arguments
usage = "Usage: %(prog)s [options] -p PROTEOME_HHM -f PROTEOME_FASTA -t \
TEMPLATE_HHM -o OUTPUT_DIR"
##
parser = argparse.ArgumentParser(usage = usage)
parser.add_argument('-p', dest = 'proteome_hhm', required = True,
                    help = 'Input proteome concatenated HHM')
parser.add_argument('-f', dest = 'proteome_fasta', required = True,
                    help = 'Input proteome NCBI-styled FASTA')
parser.add_argument('-t', dest = 'template_fasta', required = True,
                    help = 'Input template aligned FASTA')
parser.add_argument('-o', dest = 'output_dir', required = True,
                    help = 'Output HTML directory')
parser.add_argument('-s', dest = 'score_min', type = float, default = 10,
                    help = 'Minimal score in HHSearch')
parser.add_argument('-html_header', dest = 'html_header', default = '',
                    help = 'HTML header')
parser.add_argument('-job_name', dest = 'job_name', default = '',
                    help = 'Job name')
parser.add_argument('-raw_input', dest = 'raw_input_file', required = True,
                    help = 'Raw input file')
parser.add_argument('-fl', dest = 'full_length_only', action = "store_true",
                    default = False, help = 'Only full length hits')
if __name__ == "__main__":
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    opt = parser.parse_args()
    opt.proteome_hhm = os.path.abspath(opt.proteome_hhm)
    if not os.path.isfile(opt.proteome_hhm):
        error('Imput proteome HHM file does not exist')
    opt.proteome_fasta = os.path.abspath(opt.proteome_fasta)
    if not os.path.isfile(opt.proteome_fasta):
        error('Imput proteome FASTA file does not exist')
    opt.template_fasta = os.path.abspath(opt.template_fasta)
    if not os.path.isfile(opt.template_fasta):
        error('Imput template FASTA file does not exist')
    opt.output_dir = os.path.abspath(opt.output_dir) + '/'
    if not os.path.isdir(opt.output_dir):
        try:
            os.mkdir(opt.output_dir)
        except:
            error('Incorrect path for the output HTML directory')
    if (opt.score_min > 100) or (opt.score_min < 0):
        error('Minimal score must be in range [0, 100]')
    opt.raw_input_file = os.path.abspath(opt.raw_input_file)
    if not os.path.isfile(opt.raw_input_file):
        error('Raw imput FASTA file does not exist')

#---Main section---
check_binaries(['hhmake', 'hhsearch'])
tmp_hhm = tempfile.NamedTemporaryFile('r+')
with open(os.devnull, 'wb+') as devnull:
    check = subprocess.Popen(['hhmake', '-i', opt.template_fasta, '-o',\
    tmp_hhm.name, '-M', 'first', '-name', 'no_name'], stdout = devnull,\
    stderr = subprocess.PIPE)
    ##
stdout, stderr = check.communicate()
if stderr and ('ERROR' in u(stderr)):
    error(u(stderr), ' in hhmake')
tmp_hhr = tempfile.NamedTemporaryFile('r+')
with open(os.devnull, 'wb') as devnull:
    check = subprocess.Popen(['hhsearch', '-i', tmp_hhm.name, '-d',\
    opt.proteome_hhm, '-o', tmp_hhr.name, '-ssm', '0', '-norealign', '-cpu',\
    '18'], stdout = devnull, stderr = subprocess.PIPE)
    ##
    print('~{}~'.format(' '.join(['hhsearch', '-i', tmp_hhm.name, '-d',\
    opt.proteome_hhm, '-o', tmp_hhr.name, '-ssm', '0', '-norealign', '-cpu',\
    '18'])))##/
    ##
stdout, stderr = check.communicate()
if stderr and ('ERROR' in u(stderr)):
    error(u(stderr), 'in hhsearch')
template_cons = linecache.getline(tmp_hhm.name, 12).strip()
#tmp_hhm.close()##/
shutil.copyfile(opt.raw_input_file, opt.output_dir + 'input_motif.fasta')
hhr_to_html(opt.output_dir, tmp_hhr.name, opt.proteome_fasta, opt.html_header,
            opt.job_name, template_cons, opt.full_length_only, opt.score_min)
tmp_hhr.close()