import os, sys, re, tempfile, numpy, time, shutil, subprocess, threading, gzip
import pickle, math, signal

#---Declaring classes
#============================================================================
class AligningThread(threading.Thread):
    #------------------------------------------------------------------------
    def __init__(self, input_file, output_file):
        threading.Thread.__init__(self)
        self.input_file = input_file
        self.output_file = output_file
    #------------------------------------------------------------------------
    def run(self):
        align_fasta_file(self.input_file, self.output_file)
    #------------------------------------------------------------------------
#============================================================================
class Struct:
    """Class for emulation of C-like structures"""
    #------------------------------------------------------------------------
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
    #------------------------------------------------------------------------
#============================================================================

#---Declaring functions---
def error(message, specification = ''):
    """Function for error reporting"""
    sys.stderr.write('Error{}: {}\n'.format(specification, message))
    sys.stderr.flush()
    time.sleep(1)
    parent = os.popen('ps -p {}'.format(os.getppid())).read().split()[-1]
    gparent_pid = os.popen("ps -p {} -oppid=".format(os.getppid())).read().\
    strip()
    ##
    gparent = os.popen('ps -p {}'.format(gparent_pid)).read().split()[-1]
    if parent == 'mpiexec':
        from mpi4py import MPI
        MPI.COMM_WORLD.Abort(1)
    elif gparent == 'mpiexec':
        os.kill(os.getppid(), signal.SIGTERM)
    else:
        sys.exit(1)
        
def user_error(error_code, prot, input_files, labels_file):
    """Function for reporting error caused by inappropriate user input"""
    user_errors = {}
    user_errors[1] = 'Masking of \'{}\' failed.\nThis is caused by very high \
length of the sequence.\nUnfortunately, for very long proteins  evolutionary \
conservation and surface accessibility filtering loses its statistical \
power.\nPlaese, try to submit in advanced mode without orthology search and \
surface accessibility check, as well as with only few regions masked manually \
in the input regions file.'
    ##
    user_errors[2] = 'Too many regions for \'{}\'.\nThis is caused by very \
high length of the sequence.\nUnfortunately, no reliable results can be \
produced for very long proteins.\nPlaese, try to resubmit the task without \
\'{}\'.'
    ##
    input_labels = read_input_labels(input_files, labels_file)
    message = user_errors[error_code].format(input_labels[prot][1: ])
    sys.stderr.write('Task error: {}\n'.format(message))
    sys.stderr.flush()
    time.sleep(1)
    parent = os.popen('ps -p {}'.format(os.getppid())).read().split()[-1]
    gparent_pid = os.popen("ps -p {} -oppid=".format(os.getppid())).read().\
    strip()
    ##
    gparent = os.popen('ps -p {}'.format(gparent_pid)).read().split()[-1]
    if parent == 'mpiexec':
        from mpi4py import MPI
        MPI.COMM_WORLD.Abort(1)
    elif gparent == 'mpiexec':
        os.kill(os.getppid(), signal.SIGTERM)
    else:
        sys.exit(1)

def read_input_labels(input_files, labels_file):
    """Add protein labels to the list of input files"""
    input_labels = [None] * len(input_files)
    if not labels_file:
        for prot, fpath in enumerate(input_files):
            input_labels[prot] = '>' + fpath.rsplit('/', 1)[-1].\
            rsplit('.fasta', 1)[0]
            ##
        return input_labels
    with open(labels_file, 'r') as ifile:
        count = 0
        for line in ifile:
            if not line.startswith('>'):
                continue
            line = line.strip()
            if len(line) == 1:
                input_labels[prot] = '>' + input_files[count].\
                rsplit('/', 1)[-1].rsplit('.fasta', 1)[0]
                ##
            else:
                input_labels[count] = line
            count += 1
    if count != len(input_files):
        error('Number of labels does not match: found {} instaed of {}'.\
        format(count, len(input_files)))
        ##
    return input_labels
    
def check_binaries(binary_list):
    """Function for checking the availability of required binaries"""
    for binary in binary_list:
        stdout, stderr = subprocess.Popen(['which', binary], stdout =\
        subprocess.PIPE, stderr = subprocess.PIPE).communicate()
        ##
        if (not stdout) or (not stdout.startswith(b'/')):
            error('{} is not available\n'.format(binary))

def check_hhalign_version(hhalign):
    """Function for checking hhalign version"""
    check = subprocess.Popen([hhalign, '-h', 'all'], stdout = subprocess.PIPE,
                             stderr = subprocess.PIPE)
    stdout, stderr = check.communicate()
    if b'template_excl' not in stdout:
        error("The version of hhalign is incorrect: '-template_excl' option \
must be provided")
        ##

def sorted_by_key_dict_values_to_str(dict_, sep = '\t'):
    """Function for printing values of a dictionary sorted by keys to string"""
    result = ''
    for el in sorted(dict_.items()):
        result += str(el[1]) + sep
    if result:
        return result[: -len(sep)]
    else:
        return ''

def safe_numpy_memmap(fname, dtype, mode, shape = None, **kwargs):
    if mode == 'w+':
        nbytes = numpy.prod(shape) * numpy.dtype(dtype).itemsize
    elif mode in ('r', 'r+', 'c'):
        if shape:
            nbytes = numpy.prod(shape) * numpy.dtype(dtype).itemsize
        else:
            nbytes = os.path.getsize(fname)
            if nbytes % numpy.dtype(dtype).itemsize > 0:
                error('File size is not dividable by item size',
                      ' in safe_numpy_memmap')
            shape = nbytes // numpy.dtype(dtype).itemsize
    else:
        error('Invalid mode', ' in safe_numpy_memmap')
    try:
        subprocess.check_call(['fallocate', '-l', str(nbytes), fname])
    except subprocess.CalledProcessError:
        error('Cannot allocate memory')
    return numpy.memmap(fname, dtype, mode, shape = shape, **kwargs)

def empty_dir(path):
    """Function for generation empty directory at given path"""
    if not os.path.isdir(path):
        os.mkdir(path)
    shutil.rmtree(path)
    try:
        os.mkdir(path)
    except:
        raise RuntimeError("Directory emptying failed due to race condition")

def gen_el(list_):
    """Function generating elements of a multidimentional list"""
    if type(list_) == list:
        for el in list_:
            for el in gen_el(el):
                yield el
    else:
        yield list_

def gen_idx_el(list_):
    """Function generating lists of indexes and corresponding elements of a \
multidimentional list"""
    ##
    if type(list_) == list:
        for idx, el in enumerate(list_):
            for sub in gen_idx_el(el):
                yield [idx] + sub
    else:
        yield [list_]

def u(byte_string):
    """Function for decoding byte strings as unicode"""
    return byte_string.decode('utf-8')

def strtime():
    """Function for string representation of current local time"""
    return time.strftime("%b %d %Y %H:%M:%S", time.localtime())

def median(array):
    """Function for calculating median of the given list"""
    array = sorted(array)
    half, odd = divmod(len(array), 2)
    if odd:
        return array[half]
    return (array[half - 1] + array[half]) / 2

def dump_gzip_file(fname, obj, compresslevel = 9):
    """Function for dumping the object into the gzip-ped file"""
    if os.path.isfile(fname):
        os.remove(fname)
    file_dat = gzip.open(fname, 'wb', compresslevel = compresslevel)
    pickle.dump(obj, file_dat, protocol = 4)
    file_dat.close()
    
def load_gzip_file(fname):
    """Function for loading the object from the gzip-ped file"""
    file_dat = gzip.open(fname, 'rb')
    obj = pickle.load(file_dat)
    file_dat.close()
    return obj

def determine_cutoff(values, included = 0.5, cut_below = True):
    """Function for determining the cut-off that keeps at least provided \
share of all values"""
    ##
    included = math.ceil(included * len(values))
    return sorted(values, reverse = cut_below)[included - 1]

def check_input_fasta(input_file, align = False):
    """Function for checking for validity input FASTA file"""
    with open(input_file, 'r') as file:
        err = False
        first_line = True
        line_prev = ''
        header = re.compile('^>')
        if align:
            record = re.compile('^[A-Z\-]+\*?$')
        else:
            record = re.compile('^[A-Z]+\*?$')
        index = 0
        for line in file:
            index += 1
            match_re = False
            if header.search(line):
                match_re = True
                if first_line:
                    first_line = False
                if header.search(line_prev):
                    err = True
            if record.search(line):
                match_re = True
                if line_prev.isspace():
                    err = True
            if line.isspace():
                match_re = True
                if header.search(line_prev):
                    err = True
            if not match_re:
                err = True
            if err:
                raise IOError('Input file {} is corrupt or not FASTA. Error wh\
ile parsing line {}'.format(input_file.split('/')[-1], index))
                ##
            line_prev = line

def parse_input_fasta(input_file):
    """Function for parsing and placing into list an input FASTA file"""
    records = []
    cheader = re.compile('^>')
    cseq = re.compile('^[ABCDEFGHIKLMNPQRSTVWYUXBZ]+\*?$')
    cblank = re.compile('^[\ \t]*$')
    first = True
    expect_seq = False
    err = False
    header = ''
    seq = ''
    with open(input_file, 'r') as file:
        for line in file:
            if cheader.search(line):
                if expect_seq:
                    first = True
                if first:
                    first = False
                else:
                    records.append([header, seq])
                header = line.strip()
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                seq += line.strip()
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                err = True
                break
        records.append([header, seq])
        if err:
            error('Input file {} is not valid'.format(input_file))
    return records

def parse_input_fasta_to_dict(input_file):
    """Function for parsing and placing into dictionary an input FASTA file"""
    records_dict = {}
    cheader = re.compile('^>')
    cseq = re.compile('^[A-Z]+\*?$')
    cblank = re.compile('^[\ \t]*$')
    first = True
    expect_seq = False
    err = False
    header = ''
    seq = ''
    with open(input_file, 'r') as file:
        for line in file:
            if cheader.search(line):
                if expect_seq:
                    first = True
                if first:
                    first = False
                else:
                    if header in records_dict:
                        error("Duplicate header '{}'".format(header))
                    records_dict[header] = seq
                header = line.strip()
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                seq += line.strip()
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                err = True
                break
        if header in records_dict:
            error("Duplicate header '{}'".format(header))
        records_dict[header] = seq
        if err:
            error('Input file {} is not valid'.format(input_file))
    return records_dict

def get_input_fasta_first_record(input_file, loose = False):
    """Function for the first record from the input FASTA file"""
    cheader = re.compile('^>')
    if loose:
        cseq = re.compile('^[^>]')
    else:
        cseq = re.compile('^[ABCDEFGHIKLMNPQRSTVWYUXJBZ-]+\*?$')
    cblank = re.compile('^[\ \t]*$')
    first = True
    expect_seq = False
    err = False
    header = ''
    seq = ''
    with open(input_file, 'r') as file:
        for line in file:
            if cheader.search(line):
                if expect_seq:
                    first = True
                if first:
                    first = False
                else:
                    break
                header = line.strip()
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                seq += line.strip()
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                err = True
                break
        record = [header, seq]
        if err:
            raise IOError('Input file {} is not valid'.format(input_file))
    return record

def parse_input_fastas_first_records(input_dir):
    """Function for parsing and placing into list first records from an input \
directory of FASTA files"""
    ##
    if not input_dir.endswith('/'):
        input_dir += '/'
    records = []
    for fname in os.listdir(input_dir):
        fname = input_dir + fname
        records.append(get_input_fasta_first_record(fname))
    return records

def align_fasta_file(input_file, output_file, op = 1.53, mafft = 'mafft'):
    """Function for aligning a FASTA file using MAFFT"""
    with open(input_file, 'r') as file:
        if len(re.findall('(?:^|\n)>', file.read())) <= 1:
            shutil.copyfile(input_file, output_file)
            return
    with open(os.devnull, 'wb') as devnull:
        check = subprocess.Popen([mafft, '--maxiterate', '1000', '--op',\
        str(op), '--thread', '8', '--globalpair', input_file], stdout =\
        subprocess.PIPE, stderr = devnull)
        ##
        stdout, stderr = check.communicate()
        if stderr:
            error(u(stderr), ' in mafft')
        with open(output_file, 'w') as file:
            file.write(u(stdout))

def parse_align(input_file, begin = 0, end = 0, max_frag_len = 1):
    """Function for parsing as well as for comprehensive check of validity \
of an alignment file"""
    ##
    records = []
    seq = ''
    read_seq = False
    with open(input_file, 'r') as file:
        lines = file.readlines()
    index = 0
    align_len = -1
    for line in lines:
        if read_seq:
            if line.startswith('>') or line.isspace():
                read_seq = False
            if read_seq:
                seq += line[: -1] if line.endswith('\n') else line
            if (read_seq == False) or (index == len(lines) - 1):
                if (align_len >= 0) and (align_len != len(seq)):
                    raise ValueError('MSA records have different length: \
expected {} instead of {}'.format(align_len, len(seq)))
                    ##
                records.append([header, seq])
                align_len = len(seq)
                seq = ''
        if line.startswith('>'):
            header = line[: -1]
            read_seq = True
        index += 1
    if len(records) == 0:
        raise ValueError('MSA file is corrupt')
    if align_len < max_frag_len:
        raise ValueError(\
        'MSA is too short. Must be at least {} residues long'.\
        format(max_frag_len))
        ##
    if begin >= 1:
        begin = get_align_residue_number(records[0][1], begin)
    if end >= 1:
        end = get_align_residue_number(records[0][1], end)
    if end <= -1:
        end += align_len + 1
        end = get_align_residue_number(records[0][1], end)
    if begin == 0:
        begin = 1
    if end == 0:
        end = align_len
    if begin < 0:
        raise ValueError('Begin of the region of interest cannot be negative')
    if end > align_len:
        end = align_len
    if end < 1:
        raise ValueError('End of the region of interest is less then 1 after \
resolving its negative value')
        ##
    if begin > end - (max_frag_len - 1):
        raise ValueError('Begin of the region of interest must be at least {}\
residues away from its end\n'.format(max_frag_len - 1))
        ##
    return records, begin, end

def dealign_records(records):
    """Function for dealigning of aligned FASTA records"""
    for record in records:
        str1 = record[1]
        record[1] = ''.join(char for char in str1 if char != '-')

def get_seq_begin_residue_number(align_seq, res):
    """Function for mapping begin residue number from alignment to ungapped \
sequence"""
    ##
    res -= align_seq[: res - 1].count('-')
    return res

def get_seq_end_residue_number(align_seq, res):
    """Function for mapping end residue number from alignment to ungapped \
sequence"""
    ##
    res -= align_seq[: res].count('-')
    return res

def get_align_residue_number(align_seq, res):
    """Function for mapping residue number from ungapped sequence to \
alignment"""
    ##
    for i in range(len(align_seq)):
        if i < res:
            if align_seq[i] == '-':
                res += 1
        else:
            break
    if res > len(align_seq):
        raise ValueError('Impossible to map residue to the alignment')
    return res

def seq_percent_identity(seq1, seq2, in_longest = True):
    """Function for calculating percent identity of 2 aligned protein \
sequences. Sequences should be FASTA, of the same length, and without eol"""
    ##
    identical = 0
    for i in range(len(seq1)):
        if (seq1[i] in {'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L',\
        'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y'}) and (seq1[i] ==\
        seq2[i]):
        ##
            identical += 1
    gaps = min(seq1.count('-'), seq2.count('-')) if in_longest else\
    max(seq1.count('-'), seq2.count('-'))
    ##
    identity = (identical * 100) / (len(seq1) - gaps)
    return identity

def build_exposure_seq(align_seq, rsa_min, netsurfp = 'netsurfp',
                       cache_file = None, precalc_db = ''):
    """Function for classifying alignment residues to buried and exposed"""
    #rsa_min may be a float in range [0, 1) for flat cut-off for all amino acid
    #types, 1 -for using netsurfp NN classifications instead of numbers or
    #dictionary with cut-off values in range [0, 1) for every amino acid types.
    #Non-canonical or missing residues are always assumed buried
    
    def mismatch():
        """Closure for processing sequence mismatch case"""
        cache_seq = ''
        for line in netsurfp_out:
            if line.strip() and (not line.startswith('#')):
                cache_seq += line[2]
        raise RuntimeError(\
        'Cache file mismatch: got sequence\n{}\ninstead of\n{}'.\
        format(cache_seq, align_seq))
        ##
    
    if len(align_seq) == 0:
        return ''
    if (type(rsa_min) != dict) and (rsa_min <= 0):
        return re.sub('[A-Z]', 'E', align_seq)
    suffix = '_' + str(os.getpid())
    exposure_seq = ''
    netsurfp_out = None
    if cache_file and os.path.isfile(cache_file):
        netsurfp_out = load_gzip_file(cache_file)
    elif precalc_db:
        seq = align_seq.replace('-', '')
        with open(precalc_db + 'index.tsv', 'r') as ifile:
            for line in ifile:
                if line.split('\t')[0] == seq:
                    precalc_fpath = precalc_db
                    precalc_fpath += line.split('\t')[1].strip() + '.dat'
                    netsurfp_out = load_gzip_file(precalc_fpath)
    if not netsurfp_out:
        tmp_fasta = tempfile.NamedTemporaryFile('w+', suffix = suffix)
        with open(tmp_fasta.name, 'w') as ofile:
            ofile.write('>Sequence\n')
            ofile.write(align_seq.replace('-', ''))
        with open(os.devnull, 'wb') as devnull:
            check = subprocess.Popen([netsurfp, '-i', tmp_fasta.name],\
            stdout = subprocess.PIPE, stderr = devnull)
            ##
        stdout, stderr = check.communicate()
        tmp_fasta.close()
        netsurfp_out = stdout.decode('utf-8').split('\n')
        if cache_file != None:
            dump_gzip_file(cache_file, netsurfp_out)
    i = 0
    if len(netsurfp_out) - 8 != len(align_seq) - align_seq.count('-'):
        mismatch()
    for line in netsurfp_out:
        if (line.startswith('#')) or (not line.strip()):
            continue
        while align_seq[i] == '-':
            exposure_seq += '-'
            i += 1
        if line[2] != align_seq[i].replace('U', 'X'):
            mismatch()
        if type(rsa_min) == dict:
            if line[2] not in rsa_min:
                exposure_seq += 'B'
            else:
                rsa_min_ = rsa_min[line[2]]
                exposure_seq += 'E' if float(line.split()[4]) >= rsa_min_ else\
                'B'
                ##
        else:
            if rsa_min == 1.0:
                exposure_seq += line[0] if line[0].strip() else 'B'
            else:
                exposure_seq += 'E' if float(line.split()[4]) >= rsa_min else\
                'B'
                ##
        i += 1
    return exposure_seq

def build_disorder_seq(align_seq, cutoff = 0.2, iupred = 'iupred'):
    """Function for classifying alignment residues to ordered and disordered"""
    if len(align_seq) == 0:
        return ''
    if cutoff <= 0:
        return re.sub('[A-Z]', 'D', align_seq)
    suffix = '_' + str(os.getpid())
    disorder_seq = ''
    tmp_fasta = tempfile.NamedTemporaryFile('w+', suffix = suffix)
    with open(tmp_fasta.name, 'w') as ofile:
        ofile.write('>Sequence\n')
        ofile.write(align_seq.replace('-', ''))
    check = subprocess.Popen([iupred, tmp_fasta.name, 'short'], stdout =\
    subprocess.PIPE, stderr = subprocess.PIPE)
    ##
    stdout, stderr = check.communicate()
    if stderr:
        error(u(stderr), ' in IUPred')
    tmp_fasta.close()
    iupred_out = stdout.decode('utf-8').split('\n')
    i = 0
    if len(iupred_out) - 10 != len(align_seq) - align_seq.count('-'):
        error('Sequence length mismatch', ' in IUPred')
    for line in iupred_out:
        if (line.startswith('#')) or (not line.strip()):
            continue
        while align_seq[i] == '-':
            disorder_seq += '-'
            i += 1
        if line[6] != align_seq[i]:
            error('Sequence mismatch', ' in IUPred')
        disorder_seq += 'D' if float(line.split()[2]) > cutoff else 'O'
        i += 1
    return disorder_seq

def build_ss_seq(align_seq, runpsipred = 'runpsipred'):
    """Function for classifying alignment residues according to the \
secondary structure"""
    ##
    if len(align_seq) == 0:
        return ''
    ss_seq = ''
    suffix = '_' + str(os.getpid())
    tmp_fasta = tempfile.NamedTemporaryFile('w+', suffix = suffix)
    with open(tmp_fasta.name, 'w') as ofile:
        ofile.write('>Sequence\n')
        ofile.write(align_seq.replace('-', ''))
    tempcwd = tempfile.mkdtemp()
    cwd = os.getcwd()
    os.chdir(tempcwd)
    with open(os.devnull, 'wb') as devnull:
        check = subprocess.Popen([runpsipred, tmp_fasta.name], stdout =\
        devnull, stderr = subprocess.PIPE)
        ##
        stdout, stderr = check.communicate()
        if stderr:
            error(u(stderr), ' in runpsipred')
    ss2name = tmp_fasta.name.rsplit('/', 1)[1].split('.')[0] + '.ss2'
    i = 0
    with open(ss2name, 'r') as ifile:
        for line in ifile:
            if (line.startswith('#')) or (not line.strip()):
                continue
            while align_seq[i] == '-':
                ss_seq += '-'
                i += 1
            ss_seq += line.split()[2]
            i += 1
    os.chdir(cwd)
    shutil.rmtree(tempcwd)
    return ss_seq

def build_lc_seq(seq, segmasker = 'segmasker'):
    """Function for low complexity masking of ungapped sequences"""
    if len(seq) == 0:
        return ''
    lc_seq = ''
    suffix = '_' + str(os.getpid())
    tmp_fasta = tempfile.NamedTemporaryFile('w+', suffix = suffix)
    with open(tmp_fasta.name, 'w') as ofile:
        ofile.write('>Sequence\n')
        ofile.write(seq)
    tmp_lc = tempfile.NamedTemporaryFile('w+', suffix = suffix)
    with open(os.devnull, 'wb') as devnull:
        check = subprocess.Popen([segmasker, '-in', tmp_fasta.name, '-out',\
        tmp_lc.name, '-outfmt', 'fasta', '-locut', '1.8', '-hicut', '1.9',\
        '-window', '8'], stdout = devnull, stderr = subprocess.PIPE)
        ##
        stdout, stderr = check.communicate()
        if stderr:
            raise RuntimeError('Error in segmarker: ' + u(stderr))
    tmp_fasta.close()
    lc_seq = get_input_fasta_first_record(tmp_lc.name, loose = True)[1]
    if len(lc_seq) != len(seq):
        raise RuntimeError('Error while processing output of segmasker')
    tmp_lc.close()
    return lc_seq

def clusterize_records(records, qid, begin = 0, end = 0, only_first = False,
                       input_aligned = False, close_temp_files = False,
                       align_clusters = False, cores = 16, min_len = 50):
    """Function for clusterization of alignment and writing the clusters to \
separate temporary FASTA files"""
    ##

    #Sorting records by length
    records = [record for record in records if len(record[1]) >= min_len]
    records.sort(key = lambda x: len(x[1]), reverse = True)

    #Clusterization of the records
    clusterized_records = [[records[0]]]
    for i in range(1, len(records)):
        assigned = False
        for cluster in clusterized_records:
            if input_aligned:
                if seq_percent_identity(records[i][1], cluster[0][1],
                                        in_longest = False) > qid:
                    cluster.append(records[i])
                    assigned = True
                    break
            else:
                tmp_fasta = tempfile.NamedTemporaryFile('w+')
                with open(tmp_fasta.name, 'w') as ofile:
                    ofile.write(cluster[0][0] + '\n')
                    ofile.write(cluster[0][1] + '\n')
                    ofile.write(records[i][0] + '\n')
                    ofile.write(records[i][1])
                tmp_align = tempfile.NamedTemporaryFile('w+')
                align_fasta_file(tmp_fasta.name, tmp_align.name)
                tmp_fasta.close()
                aligned_records = parse_align(tmp_align.name)[0]
                tmp_align.close()
                if seq_percent_identity(aligned_records[0][1],\
                aligned_records[1][1], in_longest = False) > qid:
                ##
                    cluster.append(records[i])
                    assigned = True
                    break
        if (not assigned) and (not only_first):
            clusterized_records.append([records[i]])

    #Dealignment of clusterized records
    begins = []
    ends = []
    if input_aligned:
        for cluster in clusterized_records:
            begins.append(get_seq_begin_residue_number(cluster[0][1], begin))
            ends.append(get_seq_end_residue_number(cluster[0][1], end))
            dealign_records(cluster)

    #Alignment of clusters
    if align_clusters:
        tmp_na_clusters = []
        for cluster in clusterized_records:
            tmp_na_clusters.append(tempfile.NamedTemporaryFile('w+'))
            with open(tmp_na_clusters[len(tmp_na_clusters) - 1].name, 'w') as\
            file:
            ##
                for record in cluster:
                    file.write('{}\n{}\n'.format(record[0], record[1]))
        count = 0
        threads = [None] * len(tmp_na_clusters)
        tmp_clusters = []
        for tmp in tmp_na_clusters:
            while threading.active_count() >= cores + 1:
                time.sleep(0.1)
            tmp_clusters.append(tempfile.NamedTemporaryFile('w+'))
            threads[count] = AligningThread(tmp.name,\
            tmp_clusters[count].name)
            ##
            threads[count].start()
            count += 1
        for thread in threading.enumerate():
            if thread != threading.current_thread():
                thread.join()
        for tmp in tmp_na_clusters:
            tmp.close()
        for i in range(len(tmp_clusters)):
            clusterized_records[i], begins[i], ends[i] = parse_align(\
            tmp_clusters[i].name, begins[i], ends[i])
            ##
        if close_temp_files:
            if align_clusters:
                for tmp_cluster in tmp_clusters:
                    tmp_cluster.close()
            return clusterized_records, begins, ends
        return clusterized_records, tmp_clusters, begins, ends
        
    return clusterized_records, begins, ends

def nr_records(records, qid, min_len = 50):
    """Function for returning non-redundant subset of FASTA records"""
    clusterized_records = clusterize_records(records, qid, min_len = min_len)\
    [0]
    ##
    nr_records = []
    for cluster in clusterized_records:
        nr_records.append(cluster[0])
    return nr_records

def clusterize_fastas(input_files_list, qid, min_len = 50, mafft = 'mafft'):
    """Function for clusterization of input FASTA files according to sequence \
identity of the first records"""
    ##

    #Reading in protein sequences
    seqs = []
    for prot, fname in enumerate(input_files_list):
        seqs.append([prot, get_input_fasta_first_record(fname)[1]])
    
    #Sorting records by length
    seqs = [record for record in seqs if len(record[1]) >= min_len]
    seqs.sort(key = lambda x: len(x[1]), reverse = True)

    #Clusterization of the records
    clusterized_seqs = [[seqs[0]]]
    for i in range(1, len(seqs)):
        assigned = False
        for cluster in clusterized_seqs:
            if qid == 100:
                if cluster[0][1] == seqs[i][1]:
                    cluster.append(seqs[i])
                    assigned = True
                    break
                continue
            tmp_fasta = tempfile.NamedTemporaryFile('w+')
            with open(tmp_fasta.name, 'w') as ofile:
                ofile.write('>{}\n'.format(cluster[0][0]))
                ofile.write(cluster[0][1] + '\n')
                ofile.write('>{}\n'.format(seqs[i][0]))
                ofile.write(seqs[i][1])
            tmp_align = tempfile.NamedTemporaryFile('w+')
            align_fasta_file(tmp_fasta.name, tmp_align.name, mafft = mafft)
            tmp_fasta.close()
            aligned_records = parse_align(tmp_align.name)[0]
            tmp_align.close()
            if seq_percent_identity(aligned_records[0][1],\
            aligned_records[1][1], in_longest = False) > qid:
            ##
                cluster.append(seqs[i])
                assigned = True
                break
        if not assigned:
            clusterized_seqs.append([seqs[i]])
        
    return clusterized_seqs

def write_records_to_file(records, output_file, lnlen = 0, hlen = 0,
print_res = False):
##
    """Function for writing to file FASTA records with defined line length"""
    with open(output_file, 'w') as ofile:
        for record in records:
            if hlen > 0:
                ofile.write(record[0][: hlen])
            else:
                ofile.write(record[0])
            ofile.write('\n')
            rlen = len(record[1])
            if lnlen > 0:
                begin = 0
                while begin < rlen:
                    ofile.write(record[1][begin: begin + lnlen])
                    if print_res:
                        ofile.write(' {}\n'.format(min(begin + lnlen, rlen)))
                    else:
                        ofile.write('\n')
                    begin += lnlen
            else:
                ofile.write(record[1])
                if print_res:
                    ofile.write(' {}\n'.format(rlen))
                else:
                    ofile.write('\n')

#def factorial(m):
#    """Returns the factorial of the number m"""
#    value = 1
#    if m != 0:
#        while m != 1:
#            value = value * m
#            m = m - 1
#    return value
#
#def binomial(observed, trials, prob, exact = False):
#    """
#    Returns the binomial probability of observed+ occurrences.
#    >> observed:int = number of successes (k)
#    >> trials:int = number of trials (n)
#    >> prob:int = probability of success of each trial
#    >> usepoisson:bool = whether to use Poisson if Binomial fails [True]
#    """
#    if (not prob) or (not trials):
#        if observed:
#            return 0.0
#        else:
#            return 1.0
#    if exact:
#        return (factorial(trials) / (factorial(observed) *\
#        factorial(trials - observed))) * math.pow(prob, observed) *\
#        math.pow(1 - prob, trials - observed)
#        ##
#    if observed == 0:
#        return 1.0
#    elif observed == 1:
#        p = 1 - math.pow((1 - prob), trials)
#        if p > 0.0:
#            return p
#    p = 0.0
#    for k in range(observed, trials + 1):
#        try:        
#            p += (1.0 * factorial(trials) / (factorial(k) * factorial(trials -\
#            k))) * math.pow(prob, k) * math.pow(1 - prob, trials - k)
#            ##
#        except:
#            break
#    if p > 1:
#        return 1.0
#    return p

def available_cpu_count():
    """ Number of available virtual or physical CPUs on this system, i.e.
    user/real as output by time(1) when called with an optimally scaling
    userspace-only program"""

    # cpuset
    # cpuset may restrict the number of *available* processors
    try:
        m = re.search(r'(?m)^Cpus_allowed:\s*(.*)$',
                      open('/proc/self/status').read())
        if m:
            res = bin(int(m.group(1).replace(',', ''), 16)).count('1')
            if res > 0:
                return res
    except IOError:
        pass

    # Python 2.6+
    try:
        import multiprocessing
        return multiprocessing.cpu_count()
    except (ImportError, NotImplementedError):
        pass

    # http://code.google.com/p/psutil/
    try:
        import psutil
        return psutil.cpu_count()   # psutil.NUM_CPUS on old versions
    except (ImportError, AttributeError):
        pass

    # POSIX
    try:
        res = int(os.sysconf('SC_NPROCESSORS_ONLN'))

        if res > 0:
            return res
    except (AttributeError, ValueError):
        pass

    # Windows
    try:
        res = int(os.environ['NUMBER_OF_PROCESSORS'])

        if res > 0:
            return res
    except (KeyError, ValueError):
        pass

    # jython
    try:
        from java.lang import Runtime
        runtime = Runtime.getRuntime()
        res = runtime.availableProcessors()
        if res > 0:
            return res
    except ImportError:
        pass

    # BSD
    try:
        sysctl = subprocess.Popen(['sysctl', '-n', 'hw.ncpu'],
                                  stdout=subprocess.PIPE)
        scStdout = sysctl.communicate()[0]
        res = int(scStdout)

        if res > 0:
            return res
    except (OSError, ValueError):
        pass

    # Linux
    try:
        res = open('/proc/cpuinfo').read().count('processor\t:')

        if res > 0:
            return res
    except IOError:
        pass

    # Solaris
    try:
        pseudoDevices = os.listdir('/devices/pseudo/')
        res = 0
        for pd in pseudoDevices:
            if re.match(r'^cpuid@[0-9]+$', pd):
                res += 1

        if res > 0:
            return res
    except OSError:
        pass

    # Other UNIXes (heuristic)
    try:
        try:
            dmesg = open('/var/run/dmesg.boot').read()
        except IOError:
            dmesgProcess = subprocess.Popen(['dmesg'], stdout=subprocess.PIPE)
            dmesg = dmesgProcess.communicate()[0]

        res = 0
        while '\ncpu' + str(res) + ':' in dmesg:
            res += 1

        if res > 0:
            return res
    except OSError:
        pass

    raise Exception('Can not determine number of CPUs on this system')
