from mpi4py import MPI
import os, sys, re, argparse, subprocess, tempfile, math, time, itertools
import numpy, linecache, multiprocessing, sharedmem, socket, traceback, copy
import shutil
#from scipy.stats.distributions import binom
from functions import Struct, error, user_error, read_input_labels,\
check_binaries, check_hhalign_version, gen_el, u, dump_gzip_file,\
load_gzip_file, align_fasta_file, check_input_fasta,\
get_input_fasta_first_record, get_seq_end_residue_number,\
get_align_residue_number, build_exposure_seq, build_lc_seq, build_disorder_seq
##
from functions_mpi import bcast_array, sendrecv_array, custom_barrier
from functions_html import write_results_to_html, make_logos


#Set MPI environment
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
def excepthook(type_, value, traceback_):
    traceback.print_exception(type_, value, traceback_)
    sys.stderr.flush()
    comm.Abort(1)
sys.excepthook = excepthook


#Class with global variables
class GV():
    """Class containing glbal variables"""
    #Environment:
    mafft = 'mafft'
    hhmake = 'hhmake'
    hhalign = 'hhalign'
    netsurfp = 'netsurfp'
    runpsipred = 'runpsipred'
    weblogo = '/var/www/testsite/test_env/bin/weblogo'
    #Constants:
    f4_min = numpy.finfo(numpy.float32).min
    i2_min = numpy.iinfo(numpy.int16).min
    i2_max = numpy.iinfo(numpy.int16).max
    cgap = re.compile('^[-,X]+$')
    cinterest = re.compile('^[^:]+:([1-9]\d*-[1-9]\d*,)*[1-9]\d*-[1-9]\d*$')
    aas = {'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P',
           'Q', 'R', 'S', 'T', 'V', 'W', 'Y'}
    #Hardcoded variables
    homology_part = 0.9
    vt_ts_homology = 150.0
    #Process variables
    dtype = ['empty']
    dtype_3 = ['empty']
    dtype_sc = [('vt', 'f4'), ('ev', 'f4'), ('ss', 'f4')]
    dtype_regions = ('i2', (100, 2))
    stats = Struct(matches_total = 0, matches_reciprocal = 0)
    child_memory = sharedmem.empty(2, 'f4')
    matches_totals = None
    matches_passed = None
    size_eff = -1
    rank_eff = -1
    rsa_min_dict_90 = {'A': 0.108, 'C': 0.019, 'D': 0.364, 'E': 0.372,\
    'F': 0.070, 'G': 0.228, 'H': 0.187, 'I': 0.056, 'K': 0.341, 'L': 0.075,\
    'M': 0.131, 'N': 0.226, 'P': 0.187, 'Q': 0.244, 'R': 0.226, 'S': 0.216,\
    'T': 0.232, 'V': 0.094, 'W': 0.069, 'Y': 0.113}
    rsa_min_dict_95 = {'A': 0.062, 'C': 0.015, 'D': 0.264, 'E': 0.295,\
    'F': 0.044, 'G': 0.166, 'H': 0.150, 'I': 0.036, 'K': 0.249, 'L': 0.047,\
    'M': 0.098, 'N': 0.167, 'P': 0.129, 'Q': 0.091, 'R': 0.172, 'S': 0.140,\
    'T': 0.147, 'V': 0.049, 'W': 0.046, 'Y': 0.074}

#Processing command line arguments
usage =\
"Usage: %(prog)s [options] -i INPUT_DIR -p PROTEOME_FILE -o OUTPUT_FILE"
##
parser = argparse.ArgumentParser(usage = usage)
parser.add_argument('-i', dest = 'input_dir', required = True, help =\
'Input partial proteome directory (orthologs FASTA files)')
##
parser.add_argument('-i1', dest = 'regions_file', default = '',
                    help = 'Input file with regions of interest')
parser.add_argument('-i2', dest = 'labels_file', default = '',
                    help = 'Input file with labels for input proteins')  
parser.add_argument('-o', metavar = 'OUTPUT_FILE', dest = 'output',
                    required = True, help = 'Output file')
parser.add_argument('-html', metavar = 'HTML_DIR', dest = 'html_dir',
                    default = '', help = 'HTML output directory')
parser.add_argument('-c', dest = 'cache_dir', default = '/tmp',
                    help = 'Cache directory')
parser.add_argument('-precalc_netsurfp', dest = 'netsurfp_precalc_db',\
default = '../db/netsurfp_precalculated/', help =\
'Precalculated netsurfp directory')
##
parser.add_argument('-precalc_hhm', dest = 'hhm_precalc_db', default = '',
                    help = 'Precalculated HHM directory')
parser.add_argument('-M', metavar = 'X', dest = 'M', type = int, default = -1,\
help = 'Query columns with fewer than X% gaps (or, if -1: with residue in 1st \
sequence) are match states')
##
parser.add_argument('-q', dest = 'qid', type = int, default = 70, help =\
'Ortholog minimal % sequence identity to the first species sequence')
##
parser.add_argument('-Q', dest = 'id', type = int, default = 95, help =\
'Ortholog maximal % sequence identity to the first species sequence')
##
parser.add_argument('-r', dest = 'rsa_min', default = 'dict95', help =\
"Minimal RSA to consider residue as exposed, 'class', 'dict90' or 'dict95'")
##
parser.add_argument('-ss', dest = 'ss', choices =\
['any', 'helix', 'extended', 'coil'], default = 'any', help =\
'Preferred secondary structure type')
##
parser.add_argument('-lc', dest = 'lc', action = "store_true",
                    default = False, help = 'Low complexity masking')
parser.add_argument('-dis', dest = 'disorder', action = "store_true",
                    default = False, help = 'Ordered region masking')
parser.add_argument('-no_hf', dest = 'homology_filter', action = "store_false",
                    default = True, help = 'Homology & domain filtering')
parser.add_argument('-g', dest = 'gepf', type = float, default = 0.6,
                    help = 'Gap extend penalty factor')
parser.add_argument('-s', dest = 'scoring', choices = ['vt', 'ev'],
                    default = 'vt', help = 'Score type')
parser.add_argument('-mac', dest = 'mac', action = "store_true",
                    default = False, help = 'Use MAC realignment')
parser.add_argument('-nocontxt', dest = 'nocontxt', action = "store_true",
                    default = False, help = 'Switch off context')
parser.add_argument('-corr', dest = 'corr', type = float, default = 0.1,
                    help = 'Correlation term')
parser.add_argument('-vt_min', dest = 'vt_min', type = float, default = 0.0,
                    help = 'Minimal Vitterbi score for a motif alignment')
parser.add_argument('-vt_max', dest = 'vt_max', type = float, default = 35.0,
                    help = 'Maximal Vitterbi score for a motif alignment')
parser.add_argument('-cols_max', dest = 'cols_max', type = int, default = 50,
                    help = 'Maximal columns for a match alignment')
parser.add_argument('-hits', dest = 'hits', type = int, default = 10,
                    help = 'Maximal initial number of hits per protein')
parser.add_argument('-noisy', dest = 'noisy', action = 'store_true',
                    default = False, help = 'Mode for a noisy dataset')
parser.add_argument('-p', dest = 'p_false', type = float, default = 0.26,\
help = 'Probability of a false positive signal in single protein')
##
parser.add_argument('-smin', dest = 'score_min', type = float, default = 0.0,
                    help = 'Minimal output region score')
parser.add_argument('-lmin', dest = 'len_min', type = int, default = 1,
                    help = 'Minimal outut region length')
parser.add_argument('-mmin', dest = 'match_len_min', type = int, default = 1,
                    help = 'Minimal match length in the base protein')
parser.add_argument('-rmin', dest = 'regex_score_min', type = int, default = 4,
                    help = 'Minimal regex score for found regions')
parser.add_argument('-fmin', dest = 'match_cov_min', type = float,
                    default = 0.0, help = 'Minimal final match coverage')
parser.add_argument('-pmax', dest = 'pv_max', type = float, default = 1.0,
                    help = 'Maximal p-value')
parser.add_argument('-rchk', dest = 'recheck', type = int, default = 0, help =\
'Recheck mode: 0 - off, 1 - single-pass, 2 - iterative, 3 - plus trimming, \
4 - plus same protein hits resolving, 5 - plus trimming to regex')
##
parser.add_argument('-rescore', dest = 'rescore', action = "store_true",
                    default = False, help = 'Rescore found motif regions')
parser.add_argument('-e', dest = 'expand_matches', action = "store_true",
                    default = False, help = 'Expand matches to regions')
parser.add_argument('-I', dest = 'ignore_filters', action = "store_true",
default = False, help =\
'If no regions found, ignore the filters and report the best one score-wise')
##
parser.add_argument('-P', dest = 'proc_n', type = int, default = 1,
                    help = 'Number of processes per node')
parser.add_argument('-html_header', dest = 'html_header', default = '',
                    help = 'HTML header')
parser.add_argument('-job_name', dest = 'job_name', default = '',
                    help = 'Job name')
parser.add_argument('-l', dest = 'load_data', choices =\
['none', 'models', 'blanks', 'results', 'final'], default = 'none', help =\
'[debug] Load pre-calculated data from previous runs')
##
parser.add_argument('-production', dest = 'production', action = "store_true",
                    default = False, help = 'Run in production mode')
parser.add_argument('-special', dest = 'special_output', default = '',
                    help = 'Special output file')
if (rank == 0) and (__name__ == "__main__"):
    if len(sys.argv) == 1:
        parser.print_help()
        comm.Abort(1)
    try:
        check = False

        print(sys.argv)
        opt = parser.parse_args()
        check = True
    finally:
        if not check:
            sys.stderr.flush()
            comm.Abort(1)
    opt.input_dir = os.path.abspath(opt.input_dir) + '/'
    if not os.path.isdir(opt.input_dir):
        error('Input directory does not exist')
    opt.input_files = []
    for file in os.listdir(opt.input_dir):
        filepath = opt.input_dir + file
        if os.path.isdir(filepath):
            error('Input directory contains subdirectories')
        if file.endswith('.fasta'):
            opt.input_files.append(filepath)
    if (len(opt.input_files) == 0):
        error('Input directory does not contain any FASTA files')
    if (len(opt.input_files) < 3):
        error('At least 3 input FASTA files must be provided')
    if (len(opt.input_files) > 10000):
        error('Number of input files is too large. Max.: 10000')
    opt.input_files.sort()
    if opt.regions_file:
        opt.regions_file = os.path.abspath(opt.regions_file)
        if not os.path.isfile(opt.regions_file):
            error('Input file with regions of interest does not exist')
    if opt.labels_file:
        opt.labelss_file = os.path.abspath(opt.labels_file)
        if not os.path.isfile(opt.labels_file):
            error('Input file with protein labels does not exist')
        if (not opt.special_output) and (not opt.html_dir):
            error('Protein labels can be used only for HTML or special output')
    opt.cache_dir = os.path.abspath(opt.cache_dir) + '/'
    if opt.netsurfp_precalc_db:
        opt.netsurfp_precalc_db = os.path.abspath(opt.netsurfp_precalc_db) +\
        '/'
        ##
        if not os.path.isdir(opt.netsurfp_precalc_db):
            error('Precalculated netsurfp directory does not exist')
    if opt.hhm_precalc_db:
        opt.hhm_precalc_db = os.path.abspath(opt.hhm_precalc_db) + '/'
        if not os.path.isdir(opt.hhm_precalc_db):
            error('Precalculated HHM directory does not exist')
    if opt.load_data == 'none':
        if not os.path.exists(opt.cache_dir):
            os.makedirs(opt.cache_dir)
        elif os.listdir(opt.cache_dir):
            error('Cache directory is not empty')
    else:
        if not os.path.exists(opt.cache_dir):
            error('Cache directory does not exist')
    if not opt.output:
        error('Output file is not specified')
    opt.output = os.path.abspath(opt.output)
    if opt.html_dir:
        if opt.load_data == 'final':
            error('HTML output cannot be written with \'-l final\'')
        opt.html_dir = os.path.abspath(opt.html_dir) + '/'
        if not os.path.isdir(opt.html_dir):
            os.mkdir(opt.html_dir)
    if (opt.qid < 0):
        error('Minimal % sequence identity must be at least 0')
    if (opt.qid > opt.id):
        error('Minimal % sequence identity must be no greater than maximal \
one')
        ##
    if (opt.id > 100):
        error('Maximal % sequence identity must be at most 100')
    if opt.rsa_min == 'class':
        opt.rsa_min = 1.0
    elif opt.rsa_min == 'dict90':
        opt.rsa_min = 90.0
    elif opt.rsa_min == 'dict95':
        opt.rsa_min = 95.0
    elif re.match('^0?\.\d+$', opt.rsa_min):
        opt.rsa_min = float(opt.rsa_min)
    else:
        error(\
        "Minimal RSA must be in range [0, 1), 'class', 'dict90' or 'dict95'")
        ##
    if opt.gepf < 0.0:
        error('Gap extention penalty factor must be non-negative')
    if (opt.corr < 0.0) or (opt.corr > 1.0):
        error('Correlation term must be in range [0, 1]')
    if (opt.cols_max < 3) or (opt.cols_max > 50):
        error('Maximal number of alignment columns must be in range [3, 50]')
    if (opt.hits < 1) or (opt.hits > 20):
        error('Maximal number of hits per protein must be in range [1, 20]')
    if (opt.p_false < 0.0) or (opt.p_false > 1.0):
        error('Probability of a false positive signal must be in range [0,1]')
    if (opt.len_min < 1):
        error('Minimal output region length must be positive integer')
    if (opt.regex_score_min < 3) or (opt.regex_score_min > 32):
        error('Minimal regex score must be in range [3, 32]')
    if (opt.match_cov_min < 0.0) or (opt.match_cov_min > 1.0):
        error('Final motif coverage must be in range [0, 1]')
    if (opt.pv_max < 0.0) or (opt.pv_max > 1.0):
        error('Maximal p-value must be in ranga [0, 1]')
    if (opt.recheck < 0) or (opt.recheck > 5):
        error('Recheck mode can be only in range [0, 5]')
    if (opt.proc_n < 1):
        error('Number of processes must be positive')
    if opt.expand_matches and opt.html_dir:
        error('Matches expantion is not compatible with HTML output')
    if opt.html_header and (not opt.html_dir):
        error('HTML header can be provided only with HRML output')
    if opt.job_name and (not opt.production):
        error("Job name can be provided only in production")

#---Defining functions---
def check_memory(mode_peak = True, mark = '', silent = False, proc_id = -1):
    """Function for measuring the memory consumption"""
    pid = os.getpid()
    with open('/proc/{}/status'.format(pid), 'r') as ifile:
        for line in ifile:
            if line.startswith('VmPeak' if mode_peak else 'VmSize'):
                memory = line[: -1].split(':')[1].strip().split()[0]
                memory = int(memory) / (1024 * 1024)
                break
    if not silent:
        print('{}{} memory of the {} process: {:.3f} GB'.format(mark, 'Peak'\
        if mode_peak else 'Current', 'child' if rank else 'parent', memory))
        ##
    if rank:
        GV.child_memory[0 if proc_id < 0 else 1] = memory

def set_binaries():
    """Function for setting names of the binaries and making corresponding \
changes to the environment"""
    ##
    pass    
#    with open(os.devnull, 'wb') as devnull:
#        check = subprocess.Popen(['which', 'non_existing_application'],
#                                stdout = devnull, stderr = subprocess.PIPE)
#        ##
#    stdout, stderr = check.communicate()
#    if stderr:
#        GV.mafft = '/fs/home/prytuliak/applications/mafft/bin/mafft'
#        GV.netsurfp = '/fs/home/prytuliak/applications/netsurfp-1.0/netsurfp'
#        GV.runpsipred = '$runpsipred'
#    else:
#        GV.hhalign = '/home/prytuliak/applications/hh-suite/bin/hhalign'
#        GV.hhmake = '/home/prytuliak/applications/hh-suite/bin/hhmake'

def set_dtype(prots, hits):
    """Function for setting the data type of the protein numpy arrays"""
    dtype_bm = [('scores', [('vt', 'f4'), ('ev', 'f4'), ('ss', 'f4')]),
                ('av_f_score', 'f4'),
                ('begin0', 'i2'), ('end0', 'i2'), ('seq0', 'a64'),
                ('begin1', 'i2'), ('end1', 'i2'), ('seq1', 'a64'),
                ('align_signs', 'a64')]
    dtype_bl = [('begin0', 'i2'), ('end0', 'i2'),
                ('begin1', 'i2'), ('end1', 'i2')]
    GV.dtype = [('seq', 'a32768'), ('seq_masked', 'a32768'),
                ('excl', 'a4096'), ('magic', 'f8'),
                ('res_scores', 'f4', 32768),
                ('match_counts', 'i2', 32768),
                ('best_matches', [('vt', dtype_bm, (prots, hits)),
                                  ('ev', dtype_bm, (prots, hits))]),
                ('black_list', dtype_bl, (prots, 5))]

def read_regions_file_prot(regions_file, input_fname, seq_len):
    """Function for parsing the input file with regions of interest for a \
particular protein"""
    ##
    if not regions_file:
        return [True] * seq_len
    with open(regions_file, 'r') as ifile:
        lines = ifile.readlines() 
    found = False
    line_begin = input_fname + ':'
    for line in lines:
        if line.startswith(line_begin):
            if not GV.cinterest.search(line):
                error('File with regions of interest is invalid')
            mask = [False] * seq_len
            ranges = line.strip().split(':')[1].split(',')
            for interest_range in ranges:
                interest_range = interest_range.split('-')
                begin_idx = int(interest_range[0]) - 1
                end_res = min(seq_len, int(interest_range[1]))
                for idx in range(begin_idx, end_res):
                    mask[idx] = True
            return mask
    if not found:
        return [True] * seq_len

def get_partial_av_seq_length(input_files_list):
    """Function for calculating average length of partial ptoteome\
sequences"""
    ##
    av_seq_len = 0
    for filename in input_files_list:
        av_seq_len += len(linecache.getline(filename, 2)) - 1
    av_seq_len /= len(input_files_list)
    return av_seq_len

def check_parameters(cache_dir, opt, load_data):
    """Function for checking compatibility of input parameters"""
    param_file = '{}parameters.dat'.format(cache_dir)
    if not os.path.isfile(param_file):
        if load_data != 'none':
            print('Warning: Parameters file is not found')
        return
    parameters = load_gzip_file(param_file)
    check_list = ['M', 'qid', 'id']
    if opt.load_data in ('blanks', 'results'):
        check_list.extend(['rsa_min', 'ss', 'hits', 'lc'])
    if opt.load_data == 'results':
        check_list.extend(['mac', 'corr', 'nocontxt', 'vt_min', 'vt_max',
                           'cols_max', 'gepf'])
    if opt.load_data == 'final':
        check_list.extend(['recheck', 'rescore', 'match_len_min', 'noisy',
                           'regex_score_min', 'match_cov_min', 'pv_max'])
    for field in check_list:
        if field not in parameters.dtype.fields:
            print('Warning: Parameters file is produced by earlier version')
            break
    for field in parameters.dtype.fields:
        if field not in check_list:
            continue
        cached_value = parameters[0][field]
        if str(type(cached_value)) == "<class 'numpy.bytes_'>":
            cached_value = cached_value.decode('utf-8')
        new_value = getattr(opt, field)
        if type(new_value) == float:
            new_value = numpy.float32(new_value)
        if cached_value != new_value:
            error('Unable to use the cache, as option \'{}\' does not match. E\
xpected value: {}'.format(field, cached_value))
            ##

def write_parameters_to_file(cache_dir, opt):
    """Function for saving command line parameters into file"""
    dtype = [('M', 'i2'), ('qid', 'i2'), ('id', 'i2'), ('rsa_min', 'f4'),\
    ('ss', 'a8'), ('scoring', 'a2'), ('mac', 'i1'), ('hits', 'i2'),\
    ('lc', 'i1'), ('gepf', 'f4'), ('nocontxt', 'i1'), ('corr', 'f4'),\
    ('vt_min', 'f4'), ('vt_max', 'f4'), ('cols_max', 'i1'), ('recheck', 'i1'),\
    ('rescore', 'i1'), ('noisy', 'i1'), ('match_len_min', 'i1'),\
    ('regex_score_min', 'i1'), ('match_cov_min', 'f4'), ('pv_max', 'f4'),\
    ('residues_total', 'i4')]
    ##
    parameters = numpy.zeros(1, dtype = dtype)
    for field in parameters.dtype.fields:
        if field == 'residues_total':
            continue
        parameters[field] = getattr(opt, field)
    dump_gzip_file('{}parameters.dat'.format(cache_dir), parameters)

def write_prots_to_files(g_prots_listed, cache_dir, opt):
    """Function for saving informations from g_prots_listed to files"""
    if rank != 0:
        error('write_prots_to_files cannot be called from rank != 0')
    if os.path.isfile('{}parameters.dat'.format(cache_dir)):
        os.remove('{}parameters.dat'.format(cache_dir))
    for prot, prot_info in enumerate(g_prots_listed):
        fname = '{}{:0>4}.dat'.format(cache_dir, prot)
        dump_gzip_file(fname, prot_info)
    write_parameters_to_file(cache_dir, opt)

def fetch_db_hhm(seq, output_file, hhm_precalc_db_dir):
    """Function for fetching the HHM file from the database"""
    precalc_fpath = ''
    with open(hhm_precalc_db_dir + 'index.tsv', 'r') as ifile:
        for line in ifile:
            if line.split('\t')[0] == seq:
                precalc_fpath = hhm_precalc_db_dir
                precalc_fpath += line.split('\t')[1].strip() + '.dat'
                break
    if precalc_fpath:
        shutil.copy(precalc_fpath, output_file)

def build_model(input_file, hhmfile, qid, id_, M, hhm_precalc_db, production):
    """Function for building HMM"""
    suffix = '_' + str(os.getpid())
    
    #Checking for validity
    check_input_fasta(input_file)
    seq = get_input_fasta_first_record(input_file)[1]
    len_ = len(seq)
    if len_ > 32767:
        raise ValueError('Protein is too long: {} residues (max. 32767)'.\
        format(len_))
        ##
    
    #Primal alignment
    tmp_align = tempfile.NamedTemporaryFile('w+', suffix = suffix)
    align_fasta_file(input_file, tmp_align.name, mafft = GV.mafft)
#    records = parse_align(tmp_align.name)[0]
#    tmp_align.close()
#    
#    #Filtering
#    filtered_records = [records[0]]
#    for i in range(1, len(records)):
#        if seq_percent_identity(records[i][1], records[0][1]) >= qid:
#            accept = True
#        else:
#            continue
#        for j in range(len(filtered_records)):
#            if seq_percent_identity(records[i][1], filtered_records[j][1]) >\
#               id_:
#                accept = False
#                break
#        if accept:
#            filtered_records.append(records[i])
#
#    #Dealignment
#    for record in filtered_records:
#        str1 = record[1]
#        record[1] = ''.join(char for char in str1 if char != '-')
#    tmp_dealign = tempfile.NamedTemporaryFile('w+', suffix = suffix)
#    with open(tmp_dealign.name, 'w') as ofile:
#        for record in filtered_records:
#            ofile.write('{}\n{}\n'.format(record[0], record[1]))
#
#    #Final alignment
#    tmp_align = tempfile.NamedTemporaryFile('w+', suffix = suffix)
#    align_fasta_file(tmp_dealign.name, tmp_align.name, mafft = GV.mafft)
#    tmp_dealign.close()
#    filtered_records = parse_align(tmp_align.name)[0]
    
    #Building HMM profile
    if (not hhm_precalc_db) or production:
        M = 'first' if M == -1 else M
        hhmname = hhmfile.split('/')[-1].split('.hhm')[0]
        with open(os.devnull, 'wb') as devnull:
            check = subprocess.Popen([GV.hhmake, '-i', tmp_align.name, '-o',\
            hhmfile, '-M', str(M), '-id', '100', '-name', hhmname], stdout =\
            devnull, stderr = subprocess.PIPE, stdin = None)
            ##
            stdout, stderr = check.communicate()
            if stderr and (not u(stderr).startswith('WARNING')) and (not\
            'INFO' in u(stderr)):
            ##
                error(u(stderr), ' in hhmake')
    tmp_align.close()
    
    #Fetching relevant HHM from the database
    if hhm_precalc_db:
        fetch_db_hhm(seq, hhmfile, hhm_precalc_db)
    
    #Returning first unaligned sequence
    return seq

def process_input_fasta(g_prots_listed, prot, input_files, regions_file,
                        cache_dir, qid, id_, rsa_min, M, ss, lc, disorder,
                        production, netsurfp_precalc_db, hhm_precalc_db):
    """Function for producing the HMM profiles from an input ortholog FASTA
file and saving the basic information about it in g_prots_listed"""
    ##
    
    #Building HMM
    input_file = input_files[prot]
    hhmfile = '{}{:0>4}.hhm'.format(cache_dir, prot)
    seq = ''
    if not os.path.isfile(hhmfile):
        try:
            seq = build_model(input_file, hhmfile, qid, id_, M, hhm_precalc_db,
                              production)
        except ValueError as e:
            raise ValueError('Error while processing {}: {}'.format(input_file,
                                                                    e))
    if not seq:
        seq = get_input_fasta_first_record(input_file)[1]
    len_ = len(seq)

    #Checking surface exposure and secondary structure
    cache_file = '{}{:0>4}.netsurfp.dat'.format(cache_dir, prot)
    try:
        exposure_seq = build_exposure_seq(seq, rsa_min, netsurfp =\
        GV.netsurfp, cache_file = cache_file, precalc_db = netsurfp_precalc_db)
        ##
    except RuntimeError as e:
        raise RuntimeError(\
        'Error while processing {} with cache file {}: {}'.\
        format(input_file, cache_file, e)) from None
        ##
    ss_seq = ''#build_ss_seq(seq) if ss != 'any' else ''
    lc_seq = build_lc_seq(seq) if lc else ''
    disorder_seq = build_disorder_seq(seq) if disorder else ''
    
    #Fetching user defined masking
    input_fname = input_file.rsplit('/', 1)[-1]
    if production:
        input_fname = input_fname[: -6]
    user_interest = read_regions_file_prot(regions_file, input_fname, len_)
    
    #Masking region(s) if needed
    seq_mask = [1] * len_
    for i in range(len_):
        if not user_interest[i]:
            seq_mask[i] = -1
        if exposure_seq[i] == 'B':
            seq_mask[i] = -1
        if (ss == 'helix') and (ss_seq[i] != 'H'):
            seq_mask[i] = -1
        if (ss == 'extended') and (ss_seq[i] != 'E'):
            seq_mask[i] = -1
        if (ss == 'coil') and (ss_seq[i] != 'C'):
            seq_mask[i] = -1
        if lc and lc_seq[i].islower():
            seq_mask[i] = -1
        if disorder and (disorder_seq[i] == 'O'):
            seq_mask[i] = -1
    
    #Marking regions with at least ruuning 3 instances of the same mask status
    seq_mask_immune = [False] * len_
    isl_len = 3
    for i in range(len_):
        mask = seq_mask[i]
        running = running_max = 0
        for j in range(max(0, i - isl_len + 1), min(i + isl_len, len_)):
            running += (1 if seq_mask[j] == mask else -running)
            if running > running_max:
                running_max = running
        if running_max >= isl_len:
            seq_mask_immune[i] = True
    
    #Solidifying regions with mixed mask status
    immune_prev = True
    mask_prev = -1
    for i in range(len_):
        if seq_mask_immune[i]:
            immune_prev = True
            mask_prev = seq_mask[i]
            continue
        if immune_prev == False:
            seq_mask[i] = mask_new
            continue
        sum_ = 0
        mask_next = -1
        last = i
        for j in range(i, len_):
            if seq_mask_immune[j] == True:
                mask_next = seq_mask[j]
                break
            last = j
            sum_ += seq_mask[j]
        mask_new = 1 if sum_ >= 0 else -1
        if (mask_prev == mask_next) and (mask_prev != mask_new) and\
        (last - i) < 2:
            mask_new = mask_prev
        seq_mask[i] = mask_new
        immune_prev = False
    
    #Forming exclusion string
    excl = ''
    masked_prev = False
    seq_ = ''
    for i in range(len_):
        if seq_mask[i] == 1:
            seq_ += seq[i]
            masked_prev = False
            continue
        seq_ += 'X'
        if masked_prev == True:
            continue
        last = i
        for j in range(i, len_):
            if seq_mask[j] == 1:
                break
            last = j
        if excl:
            excl += ','
        excl += '{}-{}'.format(i + 1, last + 1)
        masked_prev = True
    if len(excl) > 4096:
        raise user_error(1, prot, opt.input_files, opt.labels_file)
    
    #Saving protein info in memory
    g_prots_listed[prot]['seq'] = seq
    g_prots_listed[prot]['seq_masked'] = seq_
    g_prots_listed[prot]['excl'] = excl

    #Saving protein info to file
    datfile = '{}{:0>4}.dat'.format(cache_dir, prot)
    dump_gzip_file(datfile, g_prots_listed[prot])

def process_input_fastas(g_prots_listed, start, end, input_files, regions_file,
                         cache_dir, qid, id_, rsa_min, M, ss, lc, disorder,
                         production, netsurfp_precalc_db, hhm_precalc_db):
    """Function for producing the set of fragment HMM profiles from a list \
of input ortholog FASTA file"""
    ##
    for prot in range(start, end):
        process_input_fasta(g_prots_listed, prot, input_files, regions_file,\
        cache_dir, qid, id_, rsa_min, M, ss, lc, disorder, production,\
        netsurfp_precalc_db, hhm_precalc_db)
        ##

def gen_prot_pairs(g_prots_listed, proc_id, proc_n, homology_filter):
    """Generator of protein pairs to compare for the given process"""
    
    #Generating list of input proteins sorted by sequence length
    prots = len(g_prots_listed)
    prots_list = [(prot, len(g_prots_listed[prot]['seq'])) for prot in\
    range(prots)]
    ##
    prots_list.sort(key = lambda x: x[1])
    
    #Generating protein pairs to compare
    size_ = GV.size_eff * proc_n
    rank_ = GV.rank_eff * proc_n + proc_id
    short_end = 0
    long_end = prots * prots - 1
    take_short = True
    rank_cur = 0
    while True:
        if take_short:
            prot_a = short_end // prots
            prot_b = short_end % prots
            short_end += 1
        else:
            prot_a = long_end // prots
            prot_b = long_end % prots
            long_end -= 1
        if long_end < short_end - 1:
            break
        take_short = not take_short
        if prot_a <= prot_b:
            continue
        for filter_ in ((False, True) if homology_filter else (False, )):
            if rank_cur == rank_:
                yield (prots_list[prot_a][0], prots_list[prot_b][0], filter_)
            rank_cur = (rank_cur + 1) % size_

def compare_prot_pair(g_prots_listed, cache_dir, prot_pair, mac, nocontxt,
                      corr, vt_min, vt_max, cols_max, hits, gepf):
    """Function for performing HMM profile-profile comparison of given \
protein pair with subsequent scoring the result"""
    ##
    suffix = '_' + str(os.getpid())
    
    #Profile-profile comparison:
    prot_a, prot_b, filter_ = prot_pair
    hhmfile_a = '{}{:0>4}.hhm'.format(cache_dir, prot_a)
    hhmfile_b = '{}{:0>4}.hhm'.format(cache_dir, prot_b)
    excl_a = g_prots_listed[prot_a]['excl']
    excl_b = g_prots_listed[prot_b]['excl']
    tmp_hhr = tempfile.NamedTemporaryFile('w+', suffix = suffix)
    with open(os.devnull, 'wb') as devnull:
        check = subprocess.Popen([GV.hhalign, '-i', hhmfile_a, '-t',\
        hhmfile_b, '-o', tmp_hhr.name, '-maxres', '32767', '-ssm', '0',\
        '-smin', '0', '-alt', '100', '-corr', str(corr), '-gaph', str(gepf),\
        '-gapi', str(gepf)] + ['-norealign'] * (not mac) + ['-nocontxt'] *\
        nocontxt + ['-excl', excl_a, '-template_excl', excl_b] * (not\
        filter_), stdout = devnull, stderr = subprocess.PIPE)
        ##
    stdout, stderr = check.communicate()
    if stderr and ('ERROR' in u(stderr)):
        error(u(stderr), ' in hhalign')
    with open(tmp_hhr.name, 'r') as file:
        result_lines = file.readlines()
    tmp_hhr.close()
    
    #Result parsing
    if filter_:
        len_a = len(g_prots_listed[prot_a]['seq'])
        len_b = len(g_prots_listed[prot_b]['seq'])
        len_a_b_min = min(len_a, len_b)
        bl_a = g_prots_listed[prot_a]['black_list'][prot_b]
        bl_b = g_prots_listed[prot_b]['black_list'][prot_a]
        bl_idx = 0
    else:
        bm_a_vt = g_prots_listed[prot_a]['best_matches']['vt'][prot_b]
        bm_b_vt = g_prots_listed[prot_b]['best_matches']['vt'][prot_a]
        bm_a_ev = g_prots_listed[prot_a]['best_matches']['ev'][prot_b]
        bm_b_ev = g_prots_listed[prot_b]['best_matches']['ev'][prot_a]
        bm_idx = 0
    hit_line = False
    len_ = len(result_lines)
    for i in range(len_):
        if hit_line:
            cols = int(result_lines[i].split()[3].split('=')[1])
            vt = float(result_lines[i].split()[2].split('=')[1])
            if cols < 3:
                hit_line = False
                continue
            if filter_:
                if (cols <= len_a_b_min * GV.homology_part) and\
                (vt <= GV.vt_ts_homology):
                ##
                    hit_line = False
                    continue
                if bl_idx >= 5:
                    break
                i1 = i + 2
                while True:
                    i1 += 7
                    if (i1 >= len_) or (result_lines[i1][0] != 'Q'):
                        break
                i1 -= 7
                begin_a = int(result_lines[i + 2].split()[2])
                begin_b = int(result_lines[i + 6].split()[2])
                end_a = int(result_lines[i1].split()[4])
                end_b = int(result_lines[i1 + 4].split()[4])
                bl_a[bl_idx]['begin0'] = begin_a
                bl_a[bl_idx]['end0'] = end_a
                bl_a[bl_idx]['begin1'] = begin_b
                bl_a[bl_idx]['end1'] = end_b
                bl_b[bl_idx]['begin0'] = begin_b
                bl_b[bl_idx]['end0'] = end_b
                bl_b[bl_idx]['begin1'] = begin_a
                bl_b[bl_idx]['end1'] = end_a
                bl_idx += 1
                hit_line = False
                continue
            if (cols > cols_max) or (vt > vt_max):
                hit_line = False
                continue
            begin_a = int(result_lines[i + 2].split()[2])
            begin_b = int(result_lines[i + 6].split()[2])
            end_a = int(result_lines[i + 2].split()[4])
            end_b = int(result_lines[i + 6].split()[4])
            ev = float(result_lines[i].split()[1].split('=')[1])
            ev = -math.log10(ev) if ev > 0 else float('inf')
            ss = cols * float(result_lines[i].split()[5].split('=')[1])
            try:
                seq_a = result_lines[i + 2].split()[3]
            except IndexError:
                seq_a = ''
            if (len(seq_a) > 64) or (vt < vt_min):
                hit_line = False
                continue
            align_signs = result_lines[i + 4].strip()
            seq_b = result_lines[i + 6].split()[3]
            scores = numpy.array([(vt, ev, ss)], dtype = GV.dtype_sc)
            for ev_sc in [False, True]:
                bm_a = bm_a_ev[bm_idx] if ev_sc else bm_a_vt[bm_idx]
                bm_b = bm_b_ev[bm_idx] if ev_sc else bm_b_vt[bm_idx]
                bm_a['scores'] = scores
                bm_a['begin0'] = begin_a
                bm_a['end0'] = end_a
                bm_a['seq0'] = seq_a
                bm_a['begin1'] = begin_b
                bm_a['end1'] = end_b
                bm_a['seq1'] = seq_b
                bm_a['align_signs'] = align_signs
                bm_b['scores'] = scores
                bm_b['begin0'] = begin_b
                bm_b['end0'] = end_b
                bm_b['seq0'] = seq_b
                bm_b['begin1'] = begin_a
                bm_b['end1'] = end_a
                bm_b['seq1'] = seq_a
                bm_b['align_signs'] = align_signs
            bm_idx += 1
            if bm_idx == hits:
                break
            hit_line = False
        if result_lines[i].startswith('>'):
            hit_line = True
    if not filter_:
        bm_a_vt[: : -1].sort(order = [])

def gen_and_compare_prot_pairs(g_prots_listed, cache_dir, mac, nocontxt, corr,
                               vt_min, vt_max, cols_max, hits, gepf,
                               homology_filter, proc_id, proc_n):
    """Function for performing pairwise HMM profile-profile comparison of all \
input proteins with subsequent scoring the result"""
    ##
    for prot_pair in gen_prot_pairs(g_prots_listed, proc_id, proc_n,
                                    homology_filter):
        compare_prot_pair(g_prots_listed, cache_dir, prot_pair, mac, nocontxt,
                          corr, vt_min, vt_max, cols_max, hits, gepf)
    if proc_id == 0:
        check_memory(silent = True, proc_id = 0)

def reconstruct_g_prots_listed_0(g_prots_listed, sel_ranks = None):
    """Function for retrieval and concatenation of g_prots_listed in the \
parent process in MPI after the HHM building stage"""
    ##
    if rank != 0:
        error('reconstruct_g_prots_listed_0 cannot be called from rank != 0')
    got = got_prev = 0
    size_ = len(sel_ranks) + 1 if sel_ranks else size
    status = [None] * size_
    while got < size_ - 1:
        for i in range(1, size_):
            source = sel_ranks[i - 1] if sel_ranks else i
            if (not status[i]) and comm.Iprobe(source = source):
                g_prots_listed_ = sendrecv_array(source = source, dest = 0)
                for prot, prot_data in enumerate(g_prots_listed_):
                    if not prot_data['seq']:
                        continue
                    g_prots_listed[prot] = prot_data
                status[i] = True
                got += 1
            if got == got_prev:
                time.sleep(0.1)

def reconstruct_g_prots_listed_1(g_prots_listed, sel_ranks):
    """Function for retrieval and reconstruction of g_prots_listed in the
parent process in MPI after the protein comparison stage"""
    ##
    if rank != 0:
        error('reconstruct_g_prots_listed_1 cannot be called from rank != 0')
    scorings = ('vt', 'ev')
    prots = len(g_prots_listed)
    size_ = len(sel_ranks)
    got = 0
    status = [None] * size_
    while got < size_ - 1:
        for i in range(1, size_):
            source = sel_ranks[i]
            if status[i] or (not comm.Iprobe(source = source)):
                time.sleep(0.1)
                continue
            g_prots_listed_ = sendrecv_array(source = source)
            for prot, prot_data in enumerate(g_prots_listed):
                for scoring in scorings:
                    bm = prot_data['best_matches'][scoring]
                    bm_ = g_prots_listed_[prot]['best_matches'][scoring]
                    for j in range(prots):
                        if bm_[j][0]['begin0'] > 0:
                            bm[j] = bm_[j]
            status[i] = True
            got += 1

def calculate_comparison_stats_prot(g_prots_listed, prot, scoring, hits):
    """Function for calculating statistics of comparison results for a given 
protein"""
    ##
    prot_data = g_prots_listed[prot]
    prot_data['res_scores'][:] = 0.0
    prot_data['match_counts'][:] = 0
    for bm_prot_ in prot_data['best_matches'][scoring]:
        seq_ = [False] * 32768
        for idx_ in range(hits):
            if bm_prot_[idx_]['begin0'] <= 0:
                break
            begin_idx = bm_prot_[idx_]['begin0'] - 1
            end_res = bm_prot_[idx_]['end0']
            score = bm_prot_[idx_]['scores'][scoring]
            for res_idx in range(begin_idx, end_res):
                if seq_[res_idx]:
                    continue
                prot_data['res_scores'][res_idx] += score
                prot_data['match_counts'][res_idx] += 1
                seq_[res_idx] = True
    for res_idx in range(len(prot_data['seq'])):
        if prot_data['match_counts'][res_idx] > 0:
            prot_data['res_scores'][res_idx] /=\
            prot_data['match_counts'][res_idx]
            ##

def calculate_comparison_stats(g_prots_listed, scoring, hits, proc_n):
    """Function for calculating statistics of comparison results for each 
protein"""
    ##
    prots = len(g_prots_listed)
    g_prots_listed_ = sharedmem.empty(prots, dtype = GV.dtype)
    g_prots_listed_[: ] = g_prots_listed[: ]
    arg_list = []
    for prot in range(prots):
        arg_list.append((g_prots_listed_, prot, scoring, hits))
    pool = multiprocessing.Pool(proc_n)
    pool.starmap(calculate_comparison_stats_prot, arg_list)
    pool.close()
    pool.join()
    g_prots_listed[: ] = g_prots_listed_[: ]
    del g_prots_listed_

def calculate_region_stats(region):
    """Function for calculatig additional statistics on region matches"""
    
    #Calculating relative region borders in its gapped alignment to a match
    for match in region.matches:
        if (match.begin0 > region.end) or (match.end0 < region.begin):
            match.discard = True
            continue
        match.rbegin = max(1, region.begin - match.begin0 + 1)
        match.rend = min(len(match.seq0) - match.seq0.count('-'),
                         region.end - match.begin0 + 1)
        
        #Relative region borders in the match's seq0
        match.rbegin_ = get_align_residue_number(match.seq0, match.rbegin)
        match.rend_ = get_align_residue_number(match.seq0, match.rend)
        
        #Absolute region borders in the match's seq1
        match.begin1_reg = match.begin1 + match.rbegin_ - 1 -\
        match.seq1[0: match.rbegin_ - 1].count('-')
        ##
        match.end1_reg = match.begin1 + match.rend_ - 1 -\
        match.seq1[0: match.rend_].count('-')
        ##

def evaluate_regex(g_prots_listed, aa_freqs, region, in_prots):
    """Function for composing regex on the basis of region matches and \
calculating its p-value"""
    ##

    #Calculating regex
    regex = ''
    for col_idx, value in enumerate(region.column_scores):
        if value == 0:
            regex += '.'
            continue
        regex_col = set(region.seq[col_idx])
        for match in region.matches:
            if match.discard:
                continue
            if match.prot not in in_prots:
                continue
            res = col_idx + (region.begin - match.begin0) + 1
            if (res < 1) or (res > match.len0):
                regex_col = '.'
                break
            res_ = get_align_residue_number(match.seq0, res)
            aa = match.seq1[res_ - 1]
            if aa in '-X':
                regex_col = '.'
                break
            regex_col.add(aa)
        if regex_col != '.':
            regex_col = '[' + ''.join(sorted(regex_col)) + ']'
        regex += regex_col
    
    #Calculating background aminp acid frequencies
    aa_freqs_av = {}
    for aa in GV.aas:
        aa_freqs_av[aa] = 0.0
        aa_count = 0
        for prot in range(len(g_prots_listed)):
            aa_freqs_av[aa] += aa_freqs[prot][aa]
            aa_count += aa_freqs[prot]['n']
        aa_freqs_av[aa] /= len(g_prots_listed)
    
    #Evaluating amino acid frequency composition
    cpos = re.compile('\[([^]]+)\]')
    p = 1
    pos_n = 0
    for pos in cpos.findall(regex):
        freq = 0
        for aa in pos:
            freq += aa_freqs_av[aa]
        p *= freq
        pos_n += 1
    if pos_n < 3:##/
        p = 1
    
    #Evaluating dimers
    cdimer_gen = re.compile('\[[^]]+\]\.*\[[^]]+\]')
    n = 1
    dimer = cdimer_gen.search(regex, 0)
    while dimer:
        count = 0
        cdimer = re.compile(dimer.group(0))
        for prot in in_prots:
            local_count = sum(1 for x in cdimer.finditer(u(g_prots_listed\
            [prot]['seq_masked'])))
            ##
            count += local_count
        count = math.floor(count / len(in_prots))
        n *= count
        dimer = cdimer_gen.search(regex, dimer.start() + 1)
    pv = 1 - math.pow(1 - p, n)
    return regex, pv            

def check_region_alignment(region, prots, prots_min, match_len_min,
                           regex_score_min, match_cov_min, recheck):##/
    """Function for checking the quality of the region alignment"""
    for match_idx, match in enumerate(region.matches):
        match.discard = False
        
        #Discarding too short matches
        if match.len0 < match_len_min:
            match.discard = True
            continue
            
    #Iterative checking quality of the region columns
    final = final_final = False
    while True:
        similars = numpy.zeros((prots, len(region.seq)), dtype = 'i1')
        for match_idx, match in enumerate(region.matches):
            if match.discard:
                continue
            for rres_ in range(match.rbegin_, match.rend_ + 1):
                rres = get_seq_end_residue_number(match.seq0, rres_)
                res = match.begin0 + rres - 1
                column_idx = res - region.begin
                if match.align_signs[rres_ - 1] in '|+':
                    similars[match.prot][column_idx] = 2
                elif match.align_signs[rres_ - 1] == '.':
                    similars[match.prot][column_idx] = max(\
                    similars[match.prot][column_idx], 1)
                    ##
        columns_1 = (similars > 0).sum(0) >= prots_min
        columns_2 = (similars == 2).sum(0) >= prots_min
        region.column_scores = numpy.maximum(1 * columns_1, 2 * columns_2)
        if final:
            if final_final and (recheck >= 5):
                try:
                    region.column_scores = numpy.min(similars[numpy.sum(\
                    similars, 1) >= regex_score_min, : ], 0)
                    ##
                except ValueError:# No matches left
                    return False
            if recheck >= 3:
                trim_region(region)
                calculate_region_stats(region)
            if final_final:
                break
            for match_idx, match in enumerate(region.matches):
                if match.discard:
                    continue
                if overlap(region.begin, region.end, match.begin0,\
                match.end0) <= (region.end - region.begin + 1) * match_cov_min:
                ##
                    match.discard = True
            final_final = True
            continue
        if recheck < 2:
            break
        if recheck >= 3:
            trim_region(region)
            calculate_region_stats(region)
        if numpy.sum(region.column_scores) < regex_score_min:
            break
        discarded_count = 0
        
        #Discard matches presenting weak regexes
        scores = [[] for x in range(prots)]
        for match_idx, match in enumerate(region.matches):
            if match.discard:
                continue
            if (match.begin0 > region.end) or (match.end0 < region.begin):
                match.discard = True
                discarded_count += 1
                continue
            regex_score = 0
            for rres_ in range(match.rbegin_, match.rend_ + 1):
                rres = get_seq_end_residue_number(match.seq0, rres_)
                res = match.begin0 + rres - 1
                column_idx = res - region.begin
                res_score = 0
                if match.align_signs[rres_ - 1] in '|+':
                    res_score = 2
                elif match.align_signs[rres_ - 1] == '.':
                    res_score = 1
                regex_score += min(res_score, region.column_scores[column_idx])
            match_ef_len = min(region.end, match.end0) -\
            max(region.begin, match.begin0) + 1
            ##
            if (regex_score < regex_score_min) or (match_ef_len <
            match_len_min):
            ##
                match.discard = True
                discarded_count += 1
                continue
            if recheck >= 4:
                scores[match.prot].append(Struct(match_idx = match_idx,\
                regex_score = regex_score))
                ##
        
        #Discard suboptimal matches from the same protein
        if recheck >= 4:
            for prot in range(prots):
                if len(scores[prot]) <= 1:
                    continue
                
                #Discard ones with lower regex scores
                max_regex_score = max(map(lambda x: x.regex_score,
                                          scores[prot]))
                for el in scores[prot]:
                    if (el.regex_score < max_regex_score):
                        region.matches[el.match_idx].discard = True
                        discarded_count += 1
                match_idxs_prot = [el.match_idx for el in scores[prot] if\
                not region.matches[el.match_idx].discard]
                ##
                len_ = len(match_idxs_prot)
                if (len_ <= 1):
                    continue
                if max_regex_score == region.column_scores.sum():
                    continue
                
                #Leave only one regex combination
                matrix = numpy.zeros((len_, len(region.seq)), dtype = 'i1')
                for i, match_idx in enumerate(match_idxs_prot):
                    match = region.matches[match_idx]
                    for rres_ in range(match.rbegin_, match.rend_ + 1):
                        rres = get_seq_end_residue_number(match.seq0, rres_)
                        res = match.begin0 + rres - 1
                        column_idx = res - region.begin
                        if match.align_signs[rres_ - 1] in '|+':
                            matrix[i][column_idx] = 2
                        elif match.align_signs[rres_ - 1] == '.':
                            matrix[i][column_idx] = 1
                matrix_minima = numpy.amin(matrix, 0)
                matrix_maxima = numpy.amax(matrix, 0)
                for col_idx, value in enumerate(region.column_scores):
                    if value <= matrix_minima[col_idx]:
                        continue
                    if value <= matrix_maxima[col_idx]:
                        for match_idx in match_idxs_prot[1: ]:
                            region.matches[match_idx].discard = True
                            discarded_count += 1
                            
        if discarded_count == 0:
            final = True
    
    #Saving the check region results
    if numpy.sum(region.column_scores) >= regex_score_min:
        return True
    else:
        return False

def check_region(region, score_min, len_min, pv_max):
    """Function for checking if a region passed the postfilters"""
    if not region.accept:
        return False
    if region.score < score_min:
        return False
    if region.end - region.begin + 1 < len_min:
        return False
    if region.pv > pv_max:
        return False
    return True

def read_region_list_cur(fname):
    """Function for reading list of regions for a given protein the file"""
    if not os.path.isfile(fname):
        error('Cache is incomplete: {} is missing'.format(fname))
    region_list_cur = load_gzip_file(fname)
    return region_list_cur

def check_regions_reciprocality_prot(region_list_short, cache_dir, prot,
                                     score_min, len_min, pv_max):
    """Function for checking the reciprocality of the region matches in a \
given protein"""
    ##
    fname = '{}regions_{:0>4}.dat'.format(cache_dir, prot)
    region_list_cur = read_region_list_cur(fname)
    for region in region_list_cur:
        if not check_region(region, score_min, len_min, pv_max):
            region.accept = False
            continue
        for match in region.matches:
            overlap_total = 0
            for region_coord in region_list_short[match.prot]:
                region_begin = region_coord[0]
                if region_begin == 0:
                    break
                region_end = region_coord[1]
                overlap_ = overlap(match.begin1_reg, match.end1_reg,
                                   region_begin, region_end)
                overlap_total += overlap_
            if overlap_total <= 3:
                match.reciprocal = False
            else:
                match.reciprocal = True
            if not match.discard:
                GV.matches_totals[prot] += 1
                GV.matches_passed[prot] += 1 if match.reciprocal else 0
    dump_gzip_file(fname, region_list_cur, compresslevel = 0)

def check_regions_reciprocality(cache_dir, prots, score_min, len_min, pv_max,
                                proc_n):
    """Function for checking region reciprocality"""
    fname = cache_dir + 'regions_short.dat'
    region_list_short_numpy = load_gzip_file(fname)
    region_list_short = sharedmem.empty(prots, dtype = GV.dtype_regions)
    region_list_short[: ] = region_list_short_numpy[: ]
    del region_list_short_numpy
    arg_list = []
    for prot in range(prots):
        arg_list.append((region_list_short, cache_dir, prot, score_min,
                         len_min, pv_max))
    GV.matches_totals = sharedmem.empty(prots, 'i2')
    GV.matches_passed = sharedmem.empty(prots, 'i2')
    pool = multiprocessing.Pool(proc_n)
    pool.starmap(check_regions_reciprocality_prot, arg_list)
    pool.close()
    pool.join()
    del region_list_short
    region_list = []
    for prot in range(prots):
        fname = '{}regions_{:0>4}.dat'.format(cache_dir, prot)
        region_list_cur = read_region_list_cur(fname)
        region_list.append(region_list_cur)
    GV.stats.matches_total = sum(GV.matches_totals)
    GV.stats.matches_reciprocal = sum(GV.matches_passed)
    return region_list

def overlap(begin, end, begin_, end_):
    """function for calculating amino acid overlap of two fragments"""
    overlap = min(end, end_) - max(begin, begin_) + 1
    return max(overlap, 0)
    
def overlap_part(begin, end, begin_, end_):
    """function for calculating part overlap of two fragment"""
    overlap = min(end, end_) - max(begin, begin_) + 1
    overlap = max(overlap, 0)
    overlap_max = min(end - begin, end_ - begin_) + 1
    return overlap / overlap_max
    
def expand_regions(region_list):
    """Function for expanding matches to regions """
    region_list_ = [[] for x in range(len(region_list))]
    for region in gen_el(region_list):
        if not region.accept:
            continue
        region_list_[region.prot].append(copy.deepcopy(region))
        for match in region.matches:
            if match.discard:
                continue
            seq = match.seq1[match.rbegin_ - 1: match.rend_]
            begin = match.begin1_reg
            end = match.end1_reg
            info = '{} {} {}\n(exoanded)'.format(begin, seq, end)
            new_region = Struct(seq = seq, begin = begin, end = end, score =\
            match.score, prot = match.prot, info = info, regex = region.regex,\
            pv = region.pv, accept = True)
            ##
            region_list_[match.prot].append(new_region)
    return region_list_
    
def trim_region(region):
    """Function for region trimming on its match alignments"""
    begin_shift = numpy.argmax(region.column_scores > 0)
    end_shift = numpy.argmax(region.column_scores[:: -1] > 0)
    region.begin += begin_shift
    region.end -= end_shift
    end_trim = -end_shift if end_shift else None
    region.seq = region.seq[begin_shift: end_trim]
    region.column_scores = region.column_scores[begin_shift: end_trim]

def rescore_region(region):
    """Function for recalculating region score based on its match alignments"""
    score_sum = 0.0
    count = 0
    for match in region.matches:
        if match.discard:
            continue
        count += 1
        score_sum += match.score
    region.score = score_sum / count if count > 0 else float('nan')
    
def gen_region_prots(region, match_prots_min, take_out_n_max):
    """Generator to produce tuples of proteins, in which the region has its \
matches"""
    ##
    in_prots = set()
    in_prots.add(region.prot)
    for match in region.matches:
        if match.discard:
            continue
        in_prots.add(match.prot)
    len_ = len(in_prots)
    take_out_n_max = min(take_out_n_max, len_ - 1 - match_prots_min)
    prots_min = len_ - take_out_n_max
    for n in range(prots_min, len_ + 1):
        for in_prots_ in itertools.combinations(in_prots, n):
            if region.prot not in in_prots_:
                continue
            yield in_prots_

def discard_domain_regions(g_prots_listed, region, match_prots_min):
    """Function to discard regions that arise only because of domain and/or \
homology matches"""
    ##
    matches_n = len(region.matches)
    redundant_matches = set()
    for i in range(matches_n):
        match_a = region.matches[i]
        if match_a.discard:
            continue
        bl_prot = g_prots_listed[match_a.prot]['black_list']
        for j in range(i, matches_n):
            match_b = region.matches[j]
            if match_b.discard:
                continue
            for bl_data in bl_prot[match_b.prot]:
                if bl_data['begin0'] == 0:
                    break
                if overlap(bl_data['begin0'], bl_data['end0'], match_a.begin1,
                match_a.end1) < 3:
                ##
                    continue
                if overlap(bl_data['begin1'], bl_data['end1'], match_b.begin1,
                match_b.end1) >= 3:
                ##
                    redundant_matches.add(j)
                    break
    in_prots = set()
    for i in range(matches_n):
        if region.matches[i].discard or (i in redundant_matches):
            continue
        in_prots.add(region.matches[i].prot)
    if len(in_prots) < match_prots_min:
        region.accept = False

def calc_match_prots_min(prots_n, noisy, p_false):
    """Function to calculate minimal number of proteins with matches"""
    def combinations(k, m, p):
        k_max = min(k, m)
        sum_ = 0
        for k_ in range(k_max + 1):
            sum_ += (math.factorial(m) / (math.factorial(k_) *\
            math.factorial(m - k_))) * p ** k_
            ##
        return(sum_)
    fpr_threshold = 0.01
    prots_min = prots_n
    if prots_n <= 4:
        prots_min = 3
    else:
        for k in range(3, prots_n + 1):
            fpr_estimate = p_false ** k * combinations(k, prots_n - k, p_false)
            if fpr_estimate <= fpr_threshold:
                prots_min = k
                break
    if noisy:
        match_prots_min = max(min(prots_n // 2, prots_min), 3) - 1
    else:
        match_prots_min = prots_min - 1
    return match_prots_min

def find_motif_regions_prot(g_prots_listed, aa_freqs, region_list_short, prot,
                            prots, noisy, p_false, scoring, recheck, rescore,
                            hits, match_len_min, regex_score_min,
                            match_cov_min, fname, input_fname):
    """Function for preliminary finding motif regions in a gven protein"""
    prot_data = g_prots_listed[prot]
    match_prots_min = calc_match_prots_min(len(g_prots_listed), noisy, p_false)
    region_list_cur = []
    region_prev = False
    region_end = None
    len_ = len(prot_data['seq'])
    bm = prot_data['best_matches'][scoring]
    idx = 0
    for res_idx in range(len_):
        if not prot_data['match_counts'][res_idx] >= match_prots_min:
            region_prev = False
            continue
        if not region_prev:
            region_begin = res_idx + 1
            av_score = 0.0
        av_score += prot_data['res_scores'][res_idx]
        if (res_idx + 1 == len_) or\
        (not prot_data['match_counts'][res_idx + 1] >= match_prots_min):
        ##
            region_end = res_idx + 1
            av_score /= region_end - region_begin + 1
            region_seq = u(prot_data['seq'][region_begin - 1: region_end])
            region_matches = []
            for i, j in itertools.product(range(prots), range(hits)):
                match_begin0 = bm[i][j]['begin0']
                if match_begin0 <= 0:
                    continue
                match_end0 = bm[i][j]['end0']
                if min(region_end - match_begin0, match_end0 -\
                region_begin) < 0:
                ##
                    continue
                match_seq0 = u(bm[i][j]['seq0'])
                match_begin1 = bm[i][j]['begin1']
                match_end1 = bm[i][j]['end1']
                match_seq1 = u(bm[i][j]['seq1'])
                match_score = bm[i][j]['scores'][scoring]
                match_align_signs = u(bm[i][j]['align_signs'])
                match_len0 = match_end0 - match_begin0 + 1
                region_matches.append(Struct(begin0 = match_begin0, end0 =\
                match_end0, seq0 = match_seq0, len0 = match_len0, begin1 =\
                match_begin1, end1 = match_end1, seq1 = match_seq1, score =\
                match_score, align_signs = match_align_signs, prot = i))
                ##
            region_matches.sort(key = lambda x: x.score, reverse = True)
            region = Struct(seq = region_seq, begin = region_begin, end =\
            region_end, score = av_score, matches = region_matches, prot =\
            prot)
            ##
            calculate_region_stats(region)
            result = check_region_alignment(region, prots, match_prots_min,\
            match_len_min, regex_score_min, match_cov_min, recheck)##/
            ##
            region.accept = result if recheck > 0 else True
            trim_region(region)
            if region.accept:
                region.pv = 1.0
                in_prots_final = ()
                for in_prots in gen_region_prots(region, match_prots_min, 2):
                    regex, pv = evaluate_regex(g_prots_listed, aa_freqs,
                                               region, in_prots)
                    if pv <= region.pv:
                        region.regex = regex
                        region.pv = pv
                        in_prots_final = in_prots
                for match in region.matches:
                    if match.prot not in in_prots_final:
                        match.discard = True
                discard_domain_regions(g_prots_listed, region, match_prots_min)
            if rescore:
                rescore_region(region)
            region_list_cur.append(region)
            if check_region(region, 0.0, 1, 1.0):
                if idx >= 100:
                    user_error(2, prot, opt.input_files, opt.labels_file)
                region_list_short[prot][idx][0] = region.begin
                region_list_short[prot][idx][1] = region.end
                idx += 1
        region_prev = True
    dump_gzip_file(fname, region_list_cur, compresslevel = 0)
                
def find_motif_regions(g_prots_listed, cache_dir, noisy, p_false, scoring,
                       recheck, rescore, hits, match_len_min, regex_score_min,
                       match_cov_min, proc_n, input_files, production):
    """Function for extracting most highly scored regions in input proteins"""
    if rank > 0:
        error('find_morif_regions cannot be called from rank != 0')
    g_prots_listed_ = sharedmem.empty(len(g_prots_listed), dtype = GV.dtype)
    g_prots_listed_[: ] = g_prots_listed[: ]
    prots = len(g_prots_listed)
    region_list_short = sharedmem.empty(prots, dtype = GV.dtype_regions)
    arg_list = []
    aa_freqs = calc_aa_freqs(g_prots_listed)
#    prot_ = 1##/
#    fname = '{}regions_{:0>4}.dat'.format(cache_dir, prot_)##/
#    input_fname = input_files[prot_].split('/')[-1]##/
#    find_motif_regions_prot(g_prots_listed_, aa_freqs, region_list_short,\
#    prot_, prots, noisy, scoring, recheck, rescore, hits, match_len_min,\
#    regex_score_min, match_cov_min, fname, input_fname)##/
#    ##
#    print('~####~~~~~~~~~~~~~~~')##/
    for prot in range(prots):
        fname = '{}regions_{:0>4}.dat'.format(cache_dir, prot)
        input_fname = input_files[prot].split('/')[-1]
        if production:
            input_fname = input_fname.rsplit('.fasta', 1)[0]
        arg_list.append((g_prots_listed_, aa_freqs, region_list_short, prot,\
        prots, noisy, p_false, scoring, recheck, rescore, hits, match_len_min,\
        regex_score_min, match_cov_min, fname, input_fname))
        ##
    pool = multiprocessing.Pool(proc_n)
    pool_processes = pool._pool[:]
    result = pool.starmap_async(find_motif_regions_prot, arg_list)
    while not result.ready():
        if any(pid.exitcode for pid in pool_processes):
            sys.exit(1)
        result.wait(timeout=1)
    pool.close()
    pool.join()
    fname = cache_dir + 'regions_short.dat'
    region_list_short_numpy = numpy.zeros(prots, dtype = GV.dtype_regions)
    region_list_short_numpy[: ] =  region_list_short[: ]
    dump_gzip_file(fname, region_list_short_numpy, compresslevel = 0)
    del region_list_short_numpy
    del region_list_short
    g_prots_listed[: ] = g_prots_listed_[: ]
    del g_prots_listed_

def align_region(region, input_labels = None, production = False):
    """Function for aligning all matches of a motif region to main sequence"""
    gap_numbers_all = [0] * len(region.seq)
    calculate_region_stats(region)
    for match in region.matches:
        if match.discard:
            continue
        
        #Calculating number of gaps to insert after each residue of the region
        gap_numbers_single = [0] * len(region.seq)
        for seq0_rres_ in range(match.rbegin_, match.rend_ + 1):
            seq0_rres = get_seq_end_residue_number(match.seq0, seq0_rres_)
            region_rres = seq0_rres + (match.begin0 - region.begin)
            if match.seq0[seq0_rres_ - 1] == '-':
                gap_numbers_single[region_rres - 1] += 1
        for idx, el in enumerate(gap_numbers_single):
            gap_numbers_all[idx] = max(gap_numbers_all[idx], el)
    
    #Calculating number of padding extra spaces
    begin_min = region.begin
    digits = len(str(region.begin))
    for match in region.matches:
        if match.discard:
            continue
        begin0 = max(match.begin0, region.begin)
        match.adjust0 = region.begin - begin0
        if match.adjust0 < 0:
            match.adjust0 -= sum(gap_numbers_all[: -match.adjust0])
        match.adjust = match.adjust0 + len(str(match.begin1_reg)) - digits
        adjusted_begin = region.begin - match.adjust
        begin_min = min(begin_min, adjusted_begin)
    spaces_extra = region.begin - begin_min
    
    #Index map of region.seq, which maps original indexes to new ones after
    #the introducing extra gaps
    region_seq_imap = [None] * len(region.seq)
        
    #Inserting gaps in the region sequencce
    region_seq_list = list(region.seq)
    index_shift_end = 0
    for idx, el in enumerate(gap_numbers_all):
        region_seq_list[idx] += '_' * el
        region_seq_imap[idx] = idx + index_shift_end
        index_shift_end += el
    region_seq_gapped = ''.join(region_seq_list)
    if production:
        str0 = 'Regex: {}\n'.format(region.regex)
        str0 += 'Regex p-value: {:.3f}\n'.format(region.pv)
        str0 += 'Av. alignment score: {:.2f}\n'.format(region.score)
        str0 += '{:0>4} {} {} {} {}\n'.format(region.prot, ' ' * spaces_extra,\
        region.begin, region_seq_gapped.replace('_', '-'), region.end)
        ##
    else:
        str0 = '{} {} {} {} {} ({:.2f})\n'.format(' ' * 4, ' ' * spaces_extra,\
        region.begin, region_seq_gapped, region.end, region.score)
        ##
        str0 += 'Regex: {} ({:.3f})\n'.format(region.regex, region.pv)
    if input_labels == None:
        header = ''
    else:
        header = input_labels[region.prot][1: ]
    str_pseudo_msa = '>{}\n{}\n'.format(header, region_seq_gapped)
    
    #Inserting gaps in the sequences of the matches
    for match in region.matches:
        if match.discard:
            continue
        seq1_list_gapped = list(match.seq1)[match.rbegin_ - 1: match.rend_]
        region_rres_idx = max(0, match.begin0 - region.begin)
        first = True
        running_gaps = 0
        for idx_ in range(len(seq1_list_gapped)):
            if match.seq0[idx_ + match.rbegin_ - 1] == '-':
                running_gaps += 1
                continue
            extra_gaps = gap_numbers_all[region_rres_idx - 1] - running_gaps\
            if not first else 0
            ##
            aa = seq1_list_gapped[idx_]
            seq1_list_gapped[idx_] = '_' * extra_gaps + aa
            running_gaps = 0
            region_rres_idx += 1
            first = False
        seq1_region = ''.join(seq1_list_gapped)
        spaces_extra_ = spaces_extra - match.adjust
        str0 += '{:0>4} {} {} {} {} ({:.2f}){}\n'.format(match.prot, ' ' *\
        spaces_extra_, match.begin1_reg, seq1_region.replace('_', '-'),\
        match.end1_reg, match.score, ' nonreciprocal' if not match.reciprocal\
        else '')
        ##
        pad_right = len(region_seq_gapped) - len(seq1_region) + match.adjust0
        if input_labels != None:
            header = input_labels[match.prot][1: ]
        else:
            header = ''
        str_pseudo_msa += '>{}\n{}{}{}\n'.format(header, '-' *\
        (-match.adjust0), seq1_region.replace('_', '_'), '-' * pad_right)
        ##
    return str0, str_pseudo_msa

def align_regions(region_list, score_min, len_min, pv_max, input_labels,
                  production):
    """Function for building pseudo-alignments for found motif regions"""
    for prot, regions in enumerate(region_list):
        for idx, region in enumerate(regions):
            if hasattr(region, 'info'):
                continue
            if not region.accept:
                continue
            align_results = align_region(region, input_labels, production)
            region_list[prot][idx].info = align_results[0]
            region_list[prot][idx].align1 = align_results[1]

def calc_aa_freqs(g_prots_listed):
    """Function for calculating amino acid frequencies in input protein"""
    ##
    dtype_freqs = [('A', 'f4'), ('C', 'f4'), ('D', 'f4'), ('E', 'f4'),\
    ('F', 'f4'), ('G', 'f4'), ('H', 'f4'), ('I', 'f4'), ('K', 'f4'),\
    ('L', 'f4'), ('M', 'f4'), ('N', 'f4'), ('P', 'f4'), ('Q', 'f4'),\
    ('R', 'f4'), ('S', 'f4'), ('T', 'f4'), ('V', 'f4'), ('W', 'f4'),\
    ('Y', 'f4'), ('n', 'i2')]
    ##
    aa_freqs = numpy.zeros(len(g_prots_listed), dtype = dtype_freqs)
    for prot, prot_data in enumerate(g_prots_listed):
        res_n = len(prot_data['seq']) - prot_data['seq_masked'].count(b'X')
        for aa in GV.aas:
            aa_freqs[prot][aa] = prot_data['seq_masked'].count(aa.encode()) /\
            res_n if res_n else 0.0
            ##
        aa_freqs[prot]['n'] = res_n
    return aa_freqs

#def compare_region_profiles_pair(results, cache_dir, idx_min, idx_max):
#    """Function for comparing a pair of region HMMs"""
#    suffix = '_' + str(os.getpid())
#    for idx in range(idx_min, idx_max + 1):
#        tmp_hhr = tempfile.NamedTemporaryFile('w+', suffix = suffix)
#        fname_a = '{}region{}.hhm'.format(cache_dir, u(results[idx][0]))
#        fname_b = '{}region{}.hhm'.format(cache_dir, u(results[idx][1]))
#        with open(os.devnull, 'wb') as devnull:
#            check = subprocess.Popen([GV.hhalign, '-i', fname_a, '-t',\
#            fname_b, '-o', tmp_hhr.name, '-ssm', '0', '-norealign'], stdout =\
#            devnull, stderr = subprocess.PIPE)
#            ##
#        stdout, stderr = check.communicate()
#        if stderr and ('ERROR' in u(stderr)):
#            error(u(stderr), ' in hhalign')
#        results[idx][2] = float(linecache.getline(tmp_hhr.name, 10).split()[5])
#        tmp_hhr.close()

#def compare_region_profiles(cache_dir, proc_n):
#    """Function for pairwise comparison of HHM profiles of the found regions"""
#    profiles = glob.glob(cache_dir + 'region????_??.hhm')
#    len_ = len(profiles)
#    dtype = 'a7, a7, f4'
#    calc_n = len_ * (len_ - 1) // 2
#    results = sharedmem.empty(calc_n, dtype = dtype)
#    idx = 0
#    for i, j in itertools.product(range(len_), range(len_)):
#        if j >= i:
#            continue
#        results[idx][0] = profiles[i][-11: -4]
#        results[idx][1] = profiles[j][-11: -4]
#        idx += 1
#    arg_list = []
#    block_len = calc_n // proc_n + (0 if calc_n % proc_n == 0 else 1)
#    for proc_id in range(proc_n):
#        idx_min = proc_id * block_len
#        idx_max = min(calc_n - 1, (proc_id + 1) * block_len - 1)
#        arg_list.append((results, cache_dir, idx_min, idx_max))
#    pool = multiprocessing.Pool(proc_n)
#    pool.starmap(compare_region_profiles_pair, arg_list)
#    pool.close()
#    pool.join()
#    print('#0-1', idx_min, idx_max)##/
#    time.sleep(1)##/
#    results_numpy = numpy.zeros(calc_n, dtype = dtype)
#    results_numpy[: ] = results[: ]
#    del results
#    results_numpy.view(dtype).sort(order = ['f2'], axis = 0)

def write_region_pseudo_msas(region_list, html_dir, write_hmms):
    """Function for writing alignments for logs into files"""
    """Function for building pseudo-alignments for found motif regions"""
    for prot, regions in enumerate(region_list):
        for idx, region in enumerate(regions):
            if not hasattr(region, 'info'):
                continue
            fname = '{}region{:0>4}_{:0>2}.fasta'.format(html_dir, prot, idx)
            with open(fname, 'w') as ofile:
                ofile.write(region.align1.replace('\n', '\r\n'))
            if write_hmms:
                fname_hhm = fname[: -3] + 'hhm'
                with open(os.devnull, 'wb+') as devnull:
                    check = subprocess.Popen([GV.hhmake, '-i', fname, '-o',\
                    fname_hhm, '-M', '50', '-id', '100'], stdout = devnull,\
                    stderr = subprocess.PIPE, stdin = None)
                    ##
                stdout, stderr = check.communicate()
                if stderr and ('ERROR' in u(stderr)):
                    error(u(stderr), ' in hhmake')

def write_special_output(special_output_file, region_list, input_labels):
    """Function to write additional output file in a customized format"""
    with open(special_output_file, 'w') as ofile:
        first = True
        for prot, regions in enumerate(region_list):
            for idx, region in enumerate(regions):
                if not hasattr(region, 'info'):
                    continue
                if not first:
                    ofile.write('-' * 30 + '\n')
                first = False
                region_info_lines = region.info.split('\n')
                ofile.write(region_info_lines[0] + '\n')
                for line in region_info_lines[3: ]:
                    if not line:
                        continue
                    prot = int(line[: 4])
                    label = input_labels[prot][1: ].split()[0]
                    align = line[4: ].split('(')[0]
                    ofile.write('{:<25}{}\n'.format(label, align))

def write_output_to_file(output_file, input_files, region_list, production,
                         input_labels, html_header, job_name):
    """Function for formatting output results and writing them to file"""
    with open(output_file, 'w') as ofile:
        
        #Command
        if not production:
            ofile.write('# Command: {}\n'.format(' '.join(sys.argv)))
        
        #Job name
        if job_name:
            ofile.write('Job name: "{}"\n'.format(job_name))
        
        #Parameters
        if production and html_header:
            ofile.write('+{}+\n'.format('-' * 78))
            ofile.write('| Input parameters:{}|\n'.format(' ' * 60))
            ofile.write('+{}+\n'.format('-' * 78))
            c = re.compile(r'Proteins.*$', re.DOTALL)
            ofile.write(c.sub('', html_header.replace('<br />', '')))
        
        #Protein codes
        if production:
            ofile.write('+{}+\n'.format('-' * 78))
            ofile.write('| Proteins coding:{}|\n'.format(' ' * 61))
            ofile.write('+{}+\n'.format('-' * 78))
            for i in range(len(input_files)):
                if input_labels:
                    name = input_labels[i][1: ]
                else:
                    name = input_files[i].split('/')[-1].rsplit('.fasta', 1)[0]
                ofile.write('{:0>4}: {}\n'.format(i, name))
        else:
            for i in range(len(input_files)):
                if input_labels:
                    name = input_labels[i][1: ]
                else:
                    name = input_files[i].split('/')[-1]
                ofile.write('{:0>4}: {}\n'.format(i, name))
            
        #Short motif overview
        if production:
            ofile.write('+{}+\n'.format('-' * 78))
            ofile.write('| Short motif overview:{}|\n'.format(' ' * 56))
            ofile.write('+{}+\n'.format('-' * 78))
        for prot, regions in enumerate(region_list):
            line = ''
            for region in regions:
                if not hasattr(region, 'info'):
                    continue
                if line:
                    line += ', '
                line += '{} {} {} ({:.2f})'.format(region.begin, region.seq,
                                                   region.end, region.score)
            ofile.write('{:0>4}:\t{}\n'.format(prot, line))
        
        #Reciprocality and region concentration information
        if not production:
            ofile.write('*' * 30 + '\n')
            ofile.write('ReciproQ={:.3f}\n'.format(\
            GV.stats.matches_reciprocal / GV.stats.matches_total if\
            GV.stats.matches_total else float('nan')))
            ##
        
        #Detailed information
        if production:
            ofile.write('+{}+\n'.format('-' * 78))
            ofile.write('| Detailed information:{}|\n'.format(' ' * 56))
            ofile.write('+{}+\n'.format('-' * 78))
        else:
            ofile.write('*' * 30 + '\n')
        first = True
        for prot, regions in enumerate(region_list):
            if not first:
                ofile.write('*' * 30 + '\n')
            first = False
            ofile.write('{:0>4}:\n'.format(prot))
            first_ = True
            for idx, region in enumerate(regions):
                if not hasattr(region, 'info'):
                    continue
                if not first_:
                    ofile.write('-' * 10 + '\n')
                first_ = False
                ofile.write(region.info)

#---Main section---

#Check before start
if rank >= 1:
    set_binaries()
    comm.send(socket.gethostname(), dest = 0)
    GV.size_eff, GV.rank_eff = comm.recv(source = 0)
    opt = None
    opt = comm.bcast(opt, root = 0)
    sel_ranks = None
    sel_ranks = comm.bcast(sel_ranks, root = 0)
    prots = len(opt.input_files)
    set_dtype(prots, opt.hits)
    custom_barrier()
elif rank == 0:
    stime = time.time()
    check_parameters(opt.cache_dir, opt, opt.load_data)
    print('Configuring the environment... ', end = '')
    sys.stdout.flush()
    set_binaries()
    check_binaries([GV.mafft, GV.hhmake, GV.hhalign, GV.netsurfp, GV.weblogo])#, GV.runpsipred])
    check_hhalign_version(GV.hhalign)
    prots = len(opt.input_files)
    if prots > 10000:
        error('Number of protein to analyze must be max. 10000')
    if (opt.M < 0):
        opt.M = -1
    write_parameters_to_file(opt.cache_dir, opt)
    hostnames = [None for i in range(size)]
    hostnames[0] = socket.gethostname()
    for i in range(1, size):
        hostnames[i] = comm.recv(source = i)
    hostnames_set = set()
    sel_ranks = []
    for i in range(size):
        if hostnames[i] not in hostnames_set:
            hostnames_set.add(hostnames[i])
            sel_ranks.append(i)
    GV.size_eff = len(sel_ranks)
    GV.rank_eff = 0
    rank_eff = 1
    for i in range(1, size):
        if i in sel_ranks:
            comm.send((GV.size_eff, rank_eff), dest = i)
            rank_eff += 1
        else:
            comm.send((GV.size_eff, -1), dest = i)
    comm.bcast(opt, root = 0)
    comm.bcast(sel_ranks, root = 0)
    set_dtype(prots, opt.hits)
    g_prots_listed = numpy.zeros(prots, dtype = GV.dtype)
    custom_barrier()
    print('Done in {} sec'.format(int(time.time() - stime + 1)))

#Building/loading HMM profiles
if rank >= 1:
    if opt.load_data in ('none', 'models'):
        g_prots_listed = numpy.zeros(prots, dtype = GV.dtype)
        number = prots // (size - 1) + (prots % (size - 1) != 0)
        start = number * (rank - 1)
        end = min(start + number, prots)
        rsa_min = opt.rsa_min
        if opt.rsa_min == 90.0:
            rsa_min = GV.rsa_min_dict_90
        if opt.rsa_min == 95.0:
            rsa_min = GV.rsa_min_dict_95
            # HMM generation now
        process_input_fastas(g_prots_listed, start, end, opt.input_files,\
        opt.regions_file, opt.cache_dir, opt.qid, opt.id, rsa_min, opt.M,\
        opt.ss, opt.lc, opt.disorder, opt.production, opt.netsurfp_precalc_db,\
        opt.hhm_precalc_db)
        ##
        sendrecv_array(g_prots_listed, source = rank, dest = 0)
        del g_prots_listed
        custom_barrier()
elif rank == 0:
    stime = time.time()
    if opt.load_data == 'none':
        print('Building HMMs from {} proteins... '.format(prots), end = '')
    elif opt.load_data == 'models':
        print('Reassembling HMMs from {} proteins... '.format(prots), end = '')
    else:
        print('Loading HMMs from {} proteins... '.format(prots), end = '')
    sys.stdout.flush()
    if opt.load_data in ('none', 'models'):
        reconstruct_g_prots_listed_0(g_prots_listed)
        custom_barrier()
    else:
        for prot in range(prots):
            datfile = '{}{:0>4}.dat'.format(opt.cache_dir, prot)
            if not os.path.isfile(datfile):
                error('Cache is incomplete: {} is missing'.format(datfile))
            prot_data = load_gzip_file(datfile)
            if prot_data.dtype != g_prots_listed[prot].dtype:
                error('Cached files dtype do not match the current run')
            g_prots_listed[prot] = prot_data
    if opt.load_data != 'results':
        g_prots_listed[: ]['best_matches'] = -numpy.inf
    print('Done in {} sec'.format(int(time.time() - stime + 1)))

custom_barrier()
# killing processes, we keep one process per node (the one with non negative rank_eff)
if GV.rank_eff < 0:
    sys.exit(0)

#All-against-all protein comparison
#child (worker) processes
if rank > 0:
    if (opt.load_data in ('none', 'models', 'blanks')) and (GV.rank_eff > 0):
        # TODO see if not in cluster create a shared mem array directly, not one from numpy array
        g_prots_listed_numpy = bcast_array() # we can only broadcast numpy arrays to the cluster
        g_prots_listed = sharedmem.empty(len(g_prots_listed_numpy),
                                         dtype = GV.dtype)
        g_prots_listed[: ] = g_prots_listed_numpy[: ]
        procs = []
        # main work here
        for i in range(opt.proc_n):
            procs.append(multiprocessing.Process(target =\
            gen_and_compare_prot_pairs, args = (g_prots_listed, opt.cache_dir,\
            opt.mac, opt.nocontxt, opt.corr, opt.vt_min, opt.vt_max,\
            opt.cols_max, opt.hits, opt.gepf, opt.homology_filter, i,\
            opt.proc_n)))
            ##
            procs[i].start()
        for proc in procs:
            proc.join()
        g_prots_listed_numpy[: ] = g_prots_listed[: ]
        del g_prots_listed
        sendrecv_array(g_prots_listed_numpy, source = rank, dest = 0)
        del g_prots_listed_numpy
#main process
elif rank == 0:
    if opt.load_data in ('none', 'models', 'blanks'):
        stime = time.time()
        print('Comparing the proteins... ', end = '')
        sys.stdout.flush()
        bcast_array(g_prots_listed, sel_ranks)
        g_prots_listed_ = sharedmem.empty(len(g_prots_listed),
                                          dtype = GV.dtype)
        g_prots_listed_[: ] = g_prots_listed[: ]
        procs = []
        for i in range(opt.proc_n):
            procs.append(multiprocessing.Process(target =\
            gen_and_compare_prot_pairs, args = (g_prots_listed_,\
            opt.cache_dir, opt.mac, opt.nocontxt, opt.corr, opt.vt_min,\
            opt.vt_max, opt.cols_max, opt.hits, opt.gepf, opt.homology_filter,\
            i, opt.proc_n)))
            ##
            procs[i].start()
        for proc in procs:
            proc.join()
        g_prots_listed[: ] = g_prots_listed_[: ]
        del g_prots_listed_
        reconstruct_g_prots_listed_1(g_prots_listed, sel_ranks)
        write_prots_to_files(g_prots_listed, opt.cache_dir, opt)
        print('Done in {} sec.'.format(int(time.time() - stime + 1)))

custom_barrier(sel_ranks)

#Calculating statitics
if rank > 0:
    pass
elif rank == 0:
    if opt.load_data != 'final':
        stime = time.time()
        print('Calculating the statistics... ', end = '')
        sys.stdout.flush()
        # scoring parameter is here always viterbi (input arg -s)
        # hits is user defined, 4 generally, number of parwise hits retained per pair
        calculate_comparison_stats(g_prots_listed, opt.scoring, opt.hits,
                                   opt.proc_n)
        write_prots_to_files(g_prots_listed, opt.cache_dir, opt)
        find_motif_regions(g_prots_listed, opt.cache_dir, opt.noisy,\
        opt.p_false, opt.scoring, opt.recheck, opt.rescore, opt.hits,\
        opt.match_len_min, opt.regex_score_min, opt.match_cov_min, opt.proc_n,\
        opt.input_files, opt.production)
        ##
        print('Done in {} sec'.format(int(time.time() - stime + 1)))
    
custom_barrier(sel_ranks)

#Processing results
if rank >= 1:
    custom_barrier(sel_ranks)
    if GV.rank_eff == 0:
        check_memory(silent = True)
        print('Peak memory of child processes: {:.3f}/{:.3f} GB'.\
        format(GV.child_memory[0], GV.child_memory[1]))
        ##
    custom_barrier(sel_ranks)
elif rank == 0:
    stime = time.time()
    print('Formatting and saving results... ', end = '')
    sys.stdout.flush()
    # check if we have reciprocality in motifs, but also calls filtering function, p value camputation, 
    # TODO 
    region_list = check_regions_reciprocality(opt.cache_dir, prots,\
    opt.score_min, opt.len_min, opt.pv_max, opt.proc_n)
    ##
    if opt.ignore_filters:
        regions_n = 0
        max_score_unit = (-1, 0, 0.0)
        for prot, region_list_prot in enumerate(region_list):
            for idx, region in enumerate(region_list_prot):
                if region.accept:
                    regions_n += 1
                if max_score_unit[2] < region.score:
                    max_score_unit = (prot, idx, region.score)
        if (max_score_unit[0] != -1) and (regions_n == 0):
            prot = max_score_unit[0]
            idx = max_score_unit[1]
            region = region_list[prot][idx]
            region.accept = True
            aa_freqs = calc_aa_freqs(g_prots_listed)
            in_prots = next(gen_region_prots(region, 2, 0))
            regex, pv = evaluate_regex(g_prots_listed, aa_freqs, region,
                                       in_prots)
            region.regex = regex
            region.pv = float('nan')
            for match in region.matches:
                match.reciprocal = False
    if opt.expand_matches:
        region_list = expand_regions(region_list)
    input_labels = None
    if opt.labels_file:
        input_labels = read_input_labels(opt.input_files, opt.labels_file)
    align_regions(region_list, opt.score_min, opt.len_min, opt.pv_max,
                  input_labels, opt.production)
    if opt.html_dir:
        write_region_pseudo_msas(region_list, opt.html_dir, False)
        make_logos(opt.html_dir, GV.weblogo)
        write_results_to_html(opt.html_dir, opt.regions_file, opt.input_files,\
        g_prots_listed, region_list, opt.html_header, opt.ignore_filters,\
        input_labels = input_labels, job_name = opt.job_name)
        ##
    write_output_to_file(opt.output, opt.input_files, region_list,\
    opt.production, input_labels, opt.html_header, opt.job_name)
    ##
    if opt.special_output:
        write_special_output(opt.special_output, region_list, input_labels)
    print('Done in {} sec'.format(int(time.time() - stime + 1)))
    custom_barrier(sel_ranks)
    check_memory()
    custom_barrier(sel_ranks)
    sys.stderr.write('Normal termination\n')
    sys.stderr.flush()
