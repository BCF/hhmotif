#!/home/eda/test_env/bin/python3
"""
WSGI config for hhmotif_site project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import sys
sys.path.append('/usr/local/lib/python3.6/dist-packages/django')

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hhmotif_site.settings")

import django
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
