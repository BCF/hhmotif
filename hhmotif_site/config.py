#Server parameters
PROTS_MAX = 50
CORES = 32
MAX_TASKS_QUEUED = 5

#Paths
# change HHSUITE_ROOT when two versions of hhsuite when ran with proteom wide
URL_ROOT = ''
SERVER_URL = 'localhost' + URL_ROOT
ADMIN_EMAIL = 'edlira.nano@inserm.fr'
PYTHON_EXE = '/home/eda/hhmotifVenV/bin/python3'
HHSUITE_ROOT = ''
NETSURFP_ROOT = '/home/eda/netsurfp-1.0'

#Tool parameters
PV_MAX = 0.3
RSA_MIN = 0.16

#MySQL parameters
MYSQL_HOST = 'localhost'
MYSQL_USER = 'django'
MYSQL_PASSWORD = 'dnhhmm'
MYSQL_DB = 'django_dnhhmm'

#Mail settings
MAIL_SENDER_EMAIL = 'hhmotif.tagc@marseille.inserm.fr'
MAIL_SERVER = '195.220.67.1'
MAIL_PORT = 25
