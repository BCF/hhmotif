 """ Django settings for hhmotif_site project.

 For more information on this file, see
 https://docs.djangoproject.com/en/1.7/topics/settings/

 For the full list of settings and their values, see
 https://docs.djangoproject.com/en/1.7/ref/settings/
 """

 # Build paths inside the project like this: os.path.join(BASE_DIR, ...)
 import os
 BASE_DIR = os.path.dirname(os.path.dirname(__file__))

 from django.conf import settings

 # SECURITY WARNING: keep the secret key used in production secret!
 SECRET_KEY = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx

 # SECURITY WARNING: don't run with debug turned on in production!
 DEBUG = False

 ALLOWED_HOSTS = ['*']


 # Application definition

 INSTALLED_APPS = (
     'django.contrib.admin',
     'django.contrib.auth',
     'django.contrib.contenttypes',
     'django.contrib.sessions',
     'django.contrib.messages',
     'django.contrib.staticfiles',
     'kombu.transport.django',
     'hhmotif_app',
     'bootstrap_toolkit',
 )

 MIDDLEWARE_CLASSES = (
     'django.contrib.sessions.middleware.SessionMiddleware',
     'django.middleware.common.CommonMiddleware',
     'django.middleware.csrf.CsrfViewMiddleware',
     'django.contrib.auth.middleware.AuthenticationMiddleware',
     'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
     'django.contrib.messages.middleware.MessageMiddleware',
     'django.middleware.clickjacking.XFrameOptionsMiddleware',
 )

 ROOT_URLCONF = 'hhmotif_site.urls'

 WSGI_APPLICATION = 'hhmotif_site.wsgi.application'


 # Database
 # https://docs.djangoproject.com/en/1.7/ref/settings/#databases

 DATABASES = {
     xxxxxxxxxxxxxxx
 }

 # Internationalization
 # https://docs.djangoproject.com/en/1.7/topics/i18n/

 LANGUAGE_CODE = 'en-us'

 TIME_ZONE = 'UTC'

 USE_I18N = True

 USE_L10N = True

 USE_TZ = True


 # Static files (CSS, JavaScript, Images)
 # https://docs.djangoproject.com/en/1.7/howto/static-files/

 STATIC_ROOT = os.path.join(BASE_DIR, 'static')

 STATIC_URL = '/static/'

 TEMPLATES = [
     {
         'BACKEND': 'django.template.backends.django.DjangoTemplates',
         'DIRS': [os.path.join(BASE_DIR, 'templates')],
         'APP_DIRS': True,
         'OPTIONS': {
             'context_processors': [
                 "django.contrib.auth.context_processors.auth",
                 "django.template.context_processors.debug",
                 "django.template.context_processors.i18n",
                 "django.template.context_processors.media",
                 "django.template.context_processors.static",
                 "django.template.context_processors.tz",
                 "django.contrib.messages.context_processors.messages",
                 "django.template.context_processors.request",
             ],
         },
     },
 ]


 # Media settings for downloads

 MEDIA_ROOT = '/home/eda/hhmotif/outputs/'

 MEDIA_URL = '/output/'


 # Other settings

 SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
