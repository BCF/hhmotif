import os, sys, re, hashlib, datetime, time, smtplib, pickle
import mysql.connector
from email.mime.text import MIMEText
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from config import PROTS_MAX, MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD,\
MYSQL_DB, MAIL_SENDER_EMAIL, MAIL_SERVER, MAIL_PORT

def queue_the_task(queue, message, tag):
    """
    Function to wrap queing the tasks through MySQL
    """
    if queue not in ('prioritized', 'heavy'):
        raise RuntimeError('Unknown queue')
    submittime = time.strftime('%Y-%m-%d %H:%M:%S')
    mysql_conn = connect_status_database()
    cmd = "INSERT IGNORE INTO {}_queue(tag, submitted, args) "
    cmd += "VALUES('{}', '{}', %s)"
    cmd = cmd.format(queue, tag, submittime)
    mysql_conn.cursor().execute(cmd, (message, ))
    mysql_conn.commit()
    mysql_conn.close()
    return

def connect_status_database():
    """
    Function for connecting to the database 'motif_search_status'
    """
    config = {'user': MYSQL_USER, 'password': MYSQL_PASSWORD,\
    'host': MYSQL_HOST, 'database': MYSQL_DB, 'buffered': True}
    ##
    mysql_error = False
    while True:
        mysql_error_ = False
        try:
            connection = mysql.connector.connect(pool_name = 'pool',
                                                 pool_size = 1, **config)
        except mysql.connector.Error as err:
            if err.errno == mysql.connector.errorcode.ER_ACCESS_DENIED_ERROR:
                raise RuntimeError(\
                "User name and password combination is not recognized")
                ##
            elif err.errno == mysql.connector.errorcode.ER_BAD_DB_ERROR:
                raise RuntimeError("Database does not exist")
            else:
                raise RuntimeError(err)
        for queue in ('prioritized', 'heavy'):
            cmd = "CREATE TABLE IF NOT EXISTS {}_queue (" +\
            "tag CHAR(40) NOT NULL PRIMARY KEY, " +\
            "submitted DATETIME NOT NULL, " +\
            "args MEDIUMBLOB, " +\
            "worker SMALLINT DEFAULT -1, " +\
            "status VARCHAR(512)" +\
            ") ENGINE=InnoDB"
            ##
            cmd = cmd.format(queue)
            try:
                connection.cursor().execute(cmd)
            except mysql.connector.errors.OperationalError:
                if not mysql_error:
                    sys.stderr.write("MySQL connection is not available. ")
                    sys.stderr.write("Trying to reconnect...\n")
                    sys.stderr.flush()
                mysql_error = True
                mysql_error_ = True
                break
        if mysql_error_:
            time.sleep(15)
            continue
        if mysql_error:
            sys.stderr.write("MySQL connection is reestablished!\n")
            sys.stderr.flush()
        break
    return connection

def connect_sessions_database():
    """
    Function for connecting to the database 'motif_search_status'
    """
    config = {'user': MYSQL_USER, 'password': MYSQL_PASSWORD,\
    'host': MYSQL_HOST, 'database': MYSQL_DB, 'buffered': True}
    ##
    try:
        connection = mysql.connector.connect(pool_name = 'django_pool',
                                             pool_size = 15, **config)
    except mysql.connector.Error as err:
        if err.errno == mysql.connector.errorcode.ER_ACCESS_DENIED_ERROR:
            raise RuntimeError(\
            "User name and password combination is not recognized")
            ##
        elif err.errno == mysql.connector.errorcode.ER_BAD_DB_ERROR:
            raise RuntimeError("Database does not exist")
        else:
            raise RuntimeError(err)
    cmd = "CREATE TABLE IF NOT EXISTS sessions (" +\
    "session_key CHAR(40) NOT NULL PRIMARY KEY, " +\
    "started DATETIME NOT NULL, " +\
    "args BLOB " +\
    ") ENGINE=InnoDB"
    ##
    connection.cursor().execute(cmd)
    return connection

def start_session(session_key, args):
    """
    Function start a session
    """
    starttime = time.strftime('%Y-%m-%d %H:%M:%S')
    mysql_conn = connect_sessions_database()
    cmd = "INSERT IGNORE INTO sessions(session_key, started, args) "
    cmd += "VALUES('{}', '{}', %s)"
    cmd = cmd.format(session_key, starttime)
    mysql_conn.cursor().execute(cmd, (args, ))
    mysql_conn.commit()
    mysql_conn.close()
    return

def get_session_dict(session_key):
    """
    Functions for retreiving session data based on the session key
    """
    mysql_conn = connect_sessions_database()
    cmd = "SELECT args FROM sessions " +\
    "WHERE session_key='{}'"
    ##
    cmd = cmd.format(session_key)
    cursor = mysql_conn.cursor()
    cursor.execute(cmd)
    result = cursor.fetchall()
    mysql_conn.close()
    if result:
        args = pickle.loads(result[0][0])
    else:
        args = {}
    return args

def end_session(session_key):
    """
    Function for deleting the session record from MySQL
    """
    mysql_conn = connect_sessions_database()
    cmd = "DELETE FROM sessions WHERE session_key='{}'"
    cmd = cmd.format(session_key)
    mysql_conn.cursor().execute(cmd)
    mysql_conn.commit()
    mysql_conn.close()

def schedule_end_session(session_key, delay):
    """
    Function to run end_session() with a delay
    """
    time.sleep(delay)
    end_session(session_key)

def clear_record(tag, queue):
    """
    Function for deleting the table row corresponding to the tag
    """
    if queue not in ('prioritized', 'heavy'):
        raise RuntimeError('Unknown queue')
    mysql_conn = connect_status_database()
    cmd = "DELETE FROM {}_queue WHERE tag='{}'"
    cmd = cmd.format(queue, tag)
    mysql_conn.cursor().execute(cmd)
    mysql_conn.commit()
    mysql_conn.close()

def ask_status(tag):
    """
    Function for getting the status of user input check
    """
    mysql_conn = connect_status_database()
    cmd = "SELECT status FROM prioritized_queue WHERE tag='{}'"
    cmd = cmd.format(tag)
    cursor = mysql_conn.cursor()
    cursor.execute(cmd)
    result = cursor.fetchall()
    mysql_conn.close()
    status = result[0][0] if result else 'Connection timeout'
    return status

def get_number_of_tasks(queue):
    """
    Function for checking number of tasks in particular queues
    """
    if queue not in ('prioritized', 'heavy'):
        raise RuntimeError('Unknown queue')
    mysql_conn = connect_status_database()
    cmd = "SELECT COUNT(*) FROM {}_queue ORDER BY submitted LIMIT 1"
    cmd = cmd.format(queue)
    cursor = mysql_conn.cursor()
    cursor.execute(cmd)
    result = cursor.fetchall()
    mysql_conn.close()
    return result[0][0]

def generate_tag(user_email):
    """
    Function for generation of unique tag for each user request.
    """
    tag = hashlib.new('ripemd160')
    tag.update('just_nu_anna_kinberg'.encode())
    tag.update(str(datetime.datetime.now()).encode())
    tag.update(user_email.encode())
    tag = tag.hexdigest()
    return tag

def prepare_input_fasta(input_file, output_file, output_dir = '',
                        prots_min = 1, real_name='FASTA file'):
    """
    Function for extensive checking and converting input FASTA files to
    streamlined format
    """ 
    cheader = re.compile('^>')
    cseq = re.compile('^[A-Z\-]+\*?$')
    cblank = re.compile('^[\ \t]*$')
    fname = input_file.split('/')[-1]
    message_base = "Error while parsing line {} of the "
    message_base += real_name + ": "
    first = True
    expect_seq = False
    header = ''
    seq = ''
    count = -1
    with open(input_file, 'r') as ifile, open(output_file, 'w') as ofile:
        for idx, line in enumerate(ifile):
            if cheader.search(line):
                if expect_seq:
                        message = message_base + 'Empty sequence. '
                        raise IOError(message.format(idx + 1, fname))
                if first:
                    first = False
                else:
                    if len(seq) < 25:
                        message = message_base + 'Sequence is too short. '
                        message += 'All sequences must be at least 25 aa long'
                        raise IOError(message.format(idx + 1, fname))
                    elif len(seq) > 10000:
                        message = message_base + 'Sequence is too long. '
                        message += 'Maximal processible length is 10000 aa'
                        raise IOError(message.format(idx + 1, fname))
                    count += 1
                    if output_dir:
                        ofname = '{}{:0>4}.fasta'.format(output_dir, count)
                        with open(ofname, 'w') as ofile1:
                            ofile1.write('{}\n{}\n'.format(header, seq))
                        if count > PROTS_MAX:
                            message = 'Too many proteins are provided. '
                            message += 'Maximal number is ' + str(PROTS_MAX)
                            raise IOError(message)
                    else:
                        ofile.write('{}\n{}\n'.format(header, seq))
                header = line.strip()
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                if first:
                    message = message_base + 'The format is not FASTA'
                    raise IOError(message.format(idx + 1, fname))
                if '-' in line:
                    message = message_base + 'Illegal character: \'-\''
                    raise IOError(message.format(idx + 1, fname))
                if seq.endswith('*'):
                    message = message_base + 'Sequence continuing after \'*\''
                    raise IOError(message.format(idx + 1, fname))
                seq += line.strip()
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                message = message_base + 'The file is not FASTA'
                raise IOError(message.format(idx + 1, fname))
        if not seq:
            message = "Error while parsing '{}': Unexpected end of file"
            raise IOError(message.format(fname))
        if len(seq) < 25:
            message = message_base + 'Sequence is too short. '
            message += 'All sequences must be at least 25 aa long'
            raise IOError(message.format(idx + 1, fname))
        elif len(seq) > 10000:
            message = message_base + 'Sequence is too long. '
            message += 'Maximal processible length is 10000 aa'
            raise IOError(message.format(idx + 1, fname))
        count += 1
        if output_dir:
            ofname = '{}{:0>4}.fasta'.format(output_dir, count)
            with open(ofname, 'w') as ofile1:
                ofile1.write('{}\n{}\n'.format(header, seq))
            if count > PROTS_MAX:
                message = 'Too many proteins are provided. '
                message += 'Maximzl number is ' + str(PROTS_MAX)
                raise IOError(message)
        else:
            ofile.write('{}\n{}\n'.format(header, seq))
    if count + 1 < prots_min:
        message = 'Too few proteins are provided. '
        message += 'Minimal number is {}'.format(prots_min)
        raise IOError(message)
            
def prepare_input_align(input_file, output_file):
    """
    Function for extensive checking and converting input aligned motif template
    FASTA with to streamlined format
    """
    cheader = re.compile('^>')
    cseq = re.compile('^[A-Z\-]+\*?$')
    cblank = re.compile('^[\ \t]*$')
    fname = input_file.split('/')[-1]
    message_base = "Error while parsing line {} of the FASTA file: "
    first = True
    expect_seq = False
    header = ''
    seq = ''
    first_seq_len = 0
    with open(input_file, 'r') as ifile, open(output_file, 'w') as ofile:
        for idx, line in enumerate(ifile):
            if cheader.search(line):
                if expect_seq:
                        message = message_base + 'Empty sequence. '
                        raise IOError(message.format(idx + 1))
                if first:
                    first = False
                else:
                    if first_seq_len == 0:
                        first_seq_len = len(seq)
                        if first_seq_len > 50:
                            message = message_base + 'Alignment is too long. '
                            message += 'Maximal allowed length is 50 residues'
                            raise IOError(message.format(idx + 1))
                    elif len(seq) != first_seq_len:
                        message = message_base
                        message += 'Alignment sequences have different length'
                        raise IOError(message.format(idx + 1))
                    if len(seq) - seq.count('-') < 3:
                        message = message_base + 'Too few residues. '
                        message += 'Each record must contain at least 3'
                        raise IOError(message.format(idx + 1))
                    ofile.write('>\n{}\n'.format(seq))
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                if first:
                    message = message_base + "No \\'>\\' at the begin detected"
                    raise IOError(message.format(idx + 1))
                if seq.endswith('*'):
                    message = message_base + 'Sequence continuing after \'*\''
                    raise IOError(message.format(idx + 1))
                seq += line.strip()
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                message = message_base + 'Invalid line format'
                raise IOError(message.format(idx + 1))
        if not seq:
            message = "Error: Unexpected end of file"
            raise IOError(message)
        if first_seq_len == 0:
            message = message_base + 'Less than 2 FASTA records found'
            raise IOError(message.format(idx + 1))
        elif len(seq) != first_seq_len:
            message = message_base
            message += 'Alignment sequences have different length'
            raise IOError(message.format(idx + 1))
        if len(seq) - seq.count('-') < 3:
            message = message_base + 'Alignment is too short. '
            message += 'Each record must contain at least 3'
            raise IOError(message.format(idx + 1))
        ofile.write('>\n{}\n'.format(seq))

def strip_orthologs(input_file):
    """Function for leaving only first record in a streamlined FASTA file"""
    with open(input_file, 'r') as ifile:
        lines = ifile.readlines()[: 2]
    with open(input_file, 'w') as ofile:
        ofile.write(lines[0])
        ofile.write(lines[1])

def check_regions_file(regions_file, ortho_dir):
    """Function for checking for validity of the regions file"""
    fnames = os.listdir(ortho_dir)
    cline = re.compile('^[^:]+:([1-9]\d*-[1-9]\d*,)*[1-9]\d*-[1-9]\d*$')
    message_base = 'Error while parsing line {} in the regions file: '
    with open(regions_file, 'r') as ifile:
        for idx, line in enumerate(ifile):
            if line.isspace():
                continue
            if not cline.search(line):
                message = message_base + 'Invalid line format'
                raise IOError(message.format(idx + 1))
            fname = line.split(':')[0] + '.fasta'
            if fname not in fnames:
                message = message_base + '"{}" is not found in the input set'
                raise IOError(message.format(idx + 1, fname[: -6]))

def send_mail(password, dest, body, subject = ''):
    """Function for sending notification e-mails"""
    msg = MIMEText(body)
    msg['Subject'] = subject
    server = smtplib.SMTP(host = MAIL_SERVER, port = MAIL_PORT)
    server.ehlo()
    server.starttls()
    server.ehlo()
    user = MAIL_SENDER_EMAIL.split('@')[0]
    server.login(user, password)
    server.sendmail(MAIL_SENDER_EMAIL, dest, msg.as_string())
    server.quit()


def send_mail_wrapped(log_file, password, dest, body, subject = ''):
    """Functionfor safe sending of the e-mails"""
    try:
        send_mail(password, dest, body, subject)
    except Exception as e:
        with open(log_file, 'a') as log:
            log.write(str(e) + '\n')
        print('Failed to send an e-mail')

def generate_error_html(message):
    """Function for generating HTML with an error message"""
    html_template =\
"""<!DOCTYPE html>
<html lang="en">
<head>
    <title>Error while predicting motifs</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
</head>
<body>
    <p>The following error occured while running your task:</p>
    <br />
    <pre>{}</pre>
</body>
</html>"""
    ##
    return html_template.format(message)