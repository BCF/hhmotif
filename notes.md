HHMOTIF
=====================================================
## Command line tool (core)
### Requierements (external)
#### mpiexec (python-mpi4py debian package)
#### HH-suite version 3 (https://github.com/soedinglab/hh-suite)
hidden markov model alignment proteins (hammer is less sensitive)
#### HH-suite version 2 
we need it for proteome wide search because hh_search of version 3 does not do one vs many. 
In that case `set_binaries` is used to specify paths to `hh_search` `hh_make` and
`hh_align` version 3, this can be changed in `GV.variables HHSUITE_ROOT`
variable in `site/config.py` file and then in daemon `runscript.py`

#### MAFFT (http://mafft.cbvrc.jp/alignment/software/linux.html) 
for aligning orthologs
#### WebLogo
#### NetSurfP 
predicts surface accessibility (80 % accurate,f based on neuronal network )
#### Iupred 
`export IUPred_PATH=$HOME/iupred`
end PATH add bin
(option disorder, recommended)

HH-MOTIF is designed to be run on clusters and servers and is an MPI program.
 To run it, you must have mpiexec installed. In addition, you need to
 install HH-suite version 3 (https://github.com/soedinglab/hh-suite)
 with hhmake and hhalign being available form the command line. You
 also need MAFFT (http://mafft.cbrc.jp/alignment/software/linux.html);
 NetSurfP and WebLogo are optional. NetSuffP will be called only if
 the surface accessibility check is activated. To block the surface
 accessibility check, use-r 0 instead of -r 0.16. If you  

Maybe works from Python 3.4 and on (TODO)

#### Python libraries mpi4py, numpy, and sharedmem.
#### remove runspsipred not used anymore


### comprehensive.....enrichment.py
TODO : not used for the moment, it could be implemented. secondary
structure, we could predict ss with "psypred" for example and
implement the feature here and have better alignments (roman says not
much potenitial)

### In Main
set_binaries should be removed TODO (done but checking
bug in MPI

TODO : check command line args before running (outputs for example)
### in ortholog_searcher3.py 
This script searches for orthologs for a given protein using taxonomy
db and blast refseq_protein db and nr db. 
TODO Roman : taxonomy db used by ortho3 script to know which proteins belong
to which species are taken from NCBI database, but processed to remove
smth by Roman with lost scripts. We have to document what this
procession was.

Test the file on dummy protein file 
`http://www.uniprot.org/uniprot/P08100.fasta`
command line 
```python3 ortholog_searcher_3.py -i ../P08100.fasta -o ../outputs/P08100_orthos.fasta -d refseq_protein -t ../db/taxonomy -c 8```
Here we more or less test blast2

### Webserver

#### for the mysql_config command we need
```sudo apt-get install libmysqlclient-dev```

#### Here we do not need cgi mode 
we do not execute python file, we
leave django do the job so :

- disable cgi mod
- remove from apache2 mime.comf AddHandler cgi-script .py so that
  wsgi.py is not executed as python script
  
  
#### TODO the parsing of the output files is done by biopython NCBI
  XML parser and Roman profiled it (with cProfile) and it was
  long. Maybe reso the parser.
  

#### pdf2svg is needed by weblogo
#### use weblogo from python packages, not the seqlogo stuff

#### TODO check the mail (not working)
#### TODO there is "nice" option when calling ortholog_searcher_folder
  from runscript worker, but in ortholog_searcher_folder when we call
  ortholog_searcher3 there is no nice anymore. Check if nice is passed
  through the processes and if not, add it explicitely.


#### hhsuite problem

Talk with the dev guys :

In version 3 they removed one to many comparisons in hhm database format, now
their database is ffm database format (meybe the only pb is the
database format change. We need to know how to generate
ffm database. then we do not need two versions of hhsuite anymore.
See the manual then write to them. 
See also : https://github.com/soedinglab/hh-suite/issues/48

### catch the error messages from blast 
(not the stdout cause it is too much, but the error ones yes)


### Mail exchange with Roman about the databases 
(duplicated in hhmotif/db/scripts/Readme.md)
From me on  18/12/2017
>>>
Hello Roman,  
I need to know exactly which databases you processed/changed in HHMotif and how did you process them, so that we are able to do the same if needed in the future. I do not necessarily need the scripts if you do not have them, you can tell us with words  (I put Bianca in CC she will understand things better than me). So please for each of them can you tell me:
  1 what the dabase is used for and where does it originally come from, where did you take it from
  2 whether you changed it or not
  3 if yes what did you change, why and how precisely

Here is the list of the databases I see in HHMotif's installation on my computer :
From folder db:
- hhm_precalculated
- netsurfp_precalculated
- orthologs_precalculated
- proteomes
- taxonomy

From folder blast-db (these I remember taking them from the net as it is so just tell me wh
- nr00 to nr78
- refseq_protein00 to 37

Also folder hhmotif_tmp contains some databases (or maybe it is just for testing).

Have I forgotten any other database?
>>>
From Roman same day :
>>>
the precalculated databases are optional. They serve for two purposes:

    to speed-up the calculations if the same sequence is submitted
	multiple again (currently, this does not work, as the
	corresponding code regions are commented out for debug purposes)
	to ensure stable output of HH-MOTiF, as BLAST is not guaranteed 
	to output hits in the same order upon multiple runs (namely, 
	the order of hits with the same e-value is random due to incomplete thread-safety)

I am attach the corresponding scripts to populate the database for the
ELM proteins. The NetSurfP script is missing, but it is analogous: it
takes *.netsurfp.dat files  (which are Gzip archives) instead of
*.hhm.  These scripts accept as input the folder from the archive
/export2/prytuliak/work/scratch/elm_database_2016.zip.  Ideally, one
can integrate these scripts into HH-MOTiF, so that it updates its
databases on the fly, i.e. appending a line to the file index.tsv andls
putting the corresponding HHM/FASTA/DAT file into the folder.

The proteome FASTA files were originally extracted from the NCBI nr
database and then cleaned and converted to HHM with the corresponding
script prepare_proteome.py (attached) which requires fasta_breaker.py (also attached).

The taxonomy file node_short.bin is generated with the script
ncbi_convert_nodes_dmp.py (attached). 
The files gi_{species_group}.txt are downloaded manually form the web
interface of NCBI after listing all the proteins and applying taxonomy
filters. The files gi_{number}.txt are generated with the attached C++ program.

It is all probably still fuzzy, so do not hesitate asking further questions.

>>>

## SLALOM
### TODO : concatenation of input files 
(homogenous files that makes sense to concatenate)

- extend input to other formats : in bed format there are variations bed6, bed12, etc, 
gff is not inside, tabular files
- circular chromosomes : now we cam process comparisons between
 circular or linear, so we should implement mixed mode (input has to
 change and algo also)
- for Jeanne, option enrichment mode
- currently no gaps allowed, we could do a new algo that takes gaps in
account and then in aligned genomes we could compare them 
